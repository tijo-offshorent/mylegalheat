jQuery(document).ready(function($) {

    /* initialize the external events
    -----------------------------------------------------------------*/

    $('#external-events .fc-event').each(function() {
        // store data so the calendar knows to render an event upon drop
        $(this).data('event', {
            title: $.trim($(this).text()), // use the element's text as the event title
            course_id: $.trim($(this).data('course-id')),
            className : $.trim($(this).text()),
            stick: true // maintain when user navigates (see docs on the renderEvent method)
        });

        // make the event draggable using jQuery UI
        $(this).draggable({
            zIndex: 999,
            revert: true,      // will cause the event to go back to its
            revertDuration: 0, //  original position after the drag
            
        });
    });

    /* initialize the calendar
    -----------------------------------------------------------------*/
    var prev_date = '';
    $('#calendar').fullCalendar({
        weekMode: 'variable',
        header: {
            left: 'prev,next today',
            center: 'title',
            right: false
        },
        editable: true,
        theme: true,
        droppable: true, // this allows things to be dropped onto the calendar
        navLinks: true, // can click day/week names to navigate views
        eventLimit: true, // allow "more" link when too many events
        events: function( start, end, timezone, callback ){
                       var moment = $('#calendar').fullCalendar('getDate');
                       var currDate = moment.format();
                       currDate = currDate.split("-");
            jQuery.ajax({
                type : "post",
                url : LDCSAdminVars.ajax_url,
                data : { action: "getEvents", month : currDate[1], year: currDate[0] },
                success: function( response ) {
                    var events = [];
                    events = response;
                    callback(events);
                }
            });

        },
        eventRender: function(event, element) {
            element.append( "<span class='closeon-container'><span class='closeon left'></span><span class='closeon right'></span></span>" );
            element.find(".closeon").click(function() {
                var confirmation = confirm("Are you sure you want to delete?");
                if ( confirmation ) {
                    jQuery.ajax({
                        type : "post",
                        url : LDCSAdminVars.ajax_url,
                        data : { action: "remove_course_schedule", date : event.start.format(), course_id : event.course_id },
                        success: function(response) {
                            $('#calendar').fullCalendar('removeEvents',event._id);
                        }
                    }); 
                }
            });
        },   
        eventDragStart : function( event ){
            prev_date = event.start.format();
        },
        eventDrop: function(event, delta, revertFunc) {
            var confirmation = confirm("Are you sure about this change?");
            if ( !confirmation ) {
                revertFunc();
            }
            else {
                jQuery.ajax({
                    type : "post",
                    url : LDCSAdminVars.ajax_url,
                    data : { action: "add_course_schedule", date : event.start.format(), prev_date : prev_date, course_id : event.course_id, remove_date : true },
                    success: function(response) {
                        if( response == "false" ) {
                            $(".invalid-date").show();
                        } else {
                            $(".invalid-date").hide();
                        }
                    }
                }); 
            }
        },
        drop: function( date ) {
            var course_id = $.trim($(this).data('course-id'));
             jQuery.ajax({
                type : "post",
                url : LDCSAdminVars.ajax_url,
                data : { action: "add_course_schedule", date : date.format(), course_id : course_id },
                success: function(response) {
                    if( response == "false" ) {
                        $(".invalid-date").show();
                    } else {
                        $(".invalid-date").hide();
                    }
                }
            });
        }
    });

    $( "#setting_tabs" ).tabs().parents(".cs_settings_wrapper").show();

});
