<?php

add_filter('admin_menu', 'instuctor_menu', 2000);

/**
 * Adding Istructor commission menu inside learndash-lms menu.
 *
 *
 * @since 2.4.0
 */
function instuctor_menu()
{
    $check = wdmCheckInstructorCap();
    if ($check) {
        add_submenu_page('learndash-lms', __('Instructor', 'wdm_instructor_role'), __('Instructor', 'wdm_instructor_role'), 'instructor_page', 'instuctor', 'instuctor_page_callback');
    }
}

/**
 * Showing menus to insturctor and to hind according to setting for new disable commission system feature.
 *
 * @return boolean to show menu or not
 *
 * @since 2.4.0
 */
function wdmCheckInstructorCap()
{
    $wdmid_admin_setting = get_option('_wdmir_admin_settings', array());
    $wl8_show_email_tab = false;
    $wl8_show_com_n_ex = true;
    if (!is_super_admin() && array_key_exists('instructor_mail', $wdmid_admin_setting) && $wdmid_admin_setting['instructor_mail'] == 1) {
        $wl8_show_email_tab = true;
    }
    if (!is_super_admin() && isset($wdmid_admin_setting['instructor_commission']) && 1 == $wdmid_admin_setting['instructor_commission']) {
        $wl8_show_com_n_ex = false;
    }
    $wdm_instructor = get_role(ROLE_ID);
    if (!$wl8_show_email_tab && !$wl8_show_com_n_ex) {
        $wdm_instructor->remove_cap('instructor_page');

        return false;
    } else {
        $wdm_instructor->add_cap('instructor_page');

        return true;
    }
}

/**
 * Adding tabs inside intructor commission page.
 *
 *
 * @since  2.4.0
 */
function instuctor_page_callback()
{

    //check whether email tab should exist for instrutor or not.
    $wdmid_admin_setting = get_option('_wdmir_admin_settings', array());
    $wl8_show_email_tab = false;
    $wl8_show_com_n_ex = true; //for showing commission and export tabs
    $current_tab = wdmSetCurrentTab($wdmid_admin_setting, $wl8_show_com_n_ex);

    //If admin select instructor mail option then we need to display only three tabs.

    if (array_key_exists('instructor_mail', $wdmid_admin_setting) && $wdmid_admin_setting['instructor_mail'] == 1) {
        $wl8_show_email_tab = true;
        if (!is_super_admin() && $current_tab != 'export' && $current_tab != 'email') {
            $current_tab = 'commission_report';
        } elseif (!is_super_admin() && $current_tab != 'commission_report' && $current_tab != 'email') {
            $current_tab = 'export';
        } elseif (!is_super_admin() && $current_tab != 'commission_report' && $current_tab != 'export') {
            $current_tab = 'email';
        }
    } elseif (!is_super_admin() && $current_tab != 'export') {
        $current_tab = 'commission_report';
    }

    wl8ShowTabs($current_tab, $wl8_show_email_tab, $wl8_show_com_n_ex);

    ?>

    <?php do_action('instuctor_tab_add', $current_tab);
    ?>
    </h2>
    <?php wl8ShowCurrentTab($current_tab);
}

/**
 * Checking current tab if not set then setting it to default tab
 * @param  array $wdmid_admin_setting   array of tabs
 * @param  boolean &$wl8_show_com_n_ex  checking to show or hide tab
 * @return string $current_tab          current tab
 */
function wdmSetCurrentTab($wdmid_admin_setting, &$wl8_show_com_n_ex)
{
    $current_tab = '';
    // var_dump(!is_super_admin());
    // var_dump(isset($wdmid_admin_setting['instructor_commission']));
    // var_dump($wdmid_admin_setting['instructor_commission']);
    if (!is_super_admin() && isset($wdmid_admin_setting['instructor_commission']) && 1 == $wdmid_admin_setting['instructor_commission']) {
        $wl8_show_com_n_ex = false;
        $current_tab = 'email';
    } elseif (isset($_GET[ 'tab' ])) {
        $current_tab = $_GET[ 'tab' ];
    } else {
        $current_tab = 'instructor';
    }

    return $current_tab;
}

/**
 * Functions shows all tabs depending on conditions.
 *
 * @param string  $current_tab        current_tab
 * @param boolean $wl8_show_email_tab to check tab
 *
 * @return html show tab
 *
 * @since  2.4.0
 */
function wl8ShowTabs($current_tab, $wl8_show_email_tab, $wl8_show_com_n_ex)
{
    ?>
    <h2 class="nav-tab-wrapper">

        <?php
        if (is_super_admin()) {
            ?>
            <a class="nav-tab <?php echo ($current_tab == 'instructor') ? 'nav-tab-active' : '' ?> " href="?page=instuctor&tab=instructor"><?php echo __('Instructor', 'wdm_instructor_role');
            ?></a>
        <?php
        }
    ?>

    <?php
    wl8ShowCommissionAndExportContent($current_tab, $wl8_show_com_n_ex);
    ?>
    <?php
    wl8ShowMailTab($current_tab, $wl8_show_email_tab, $wl8_show_com_n_ex);
    ?>

        <?php if (is_super_admin()) {
    ?>
                <a class="nav-tab <?php echo ($current_tab == 'settings') ? 'nav-tab-active' : '' ?>" href="?page=instuctor&tab=settings"><?php echo __('Settings', 'wdm_instructor_role');
    ?></a>
                <a class="nav-tab <?php echo ($current_tab == 'wdm_ir_promotion') ? 'nav-tab-active' : '' ?>" href="?page=instuctor&tab=wdm_ir_promotion"><?php echo __('Other Extensions', 'wdm_instructor_role');
    ?></a>
        <?php
}
}

/**
 * Function shows commision and export content.
 *
 * @param string $current_tab current tab
 *
 * @return html to show tabs
 *
 * @since  2.4.0
 */
function wl8ShowCommissionAndExportContent($current_tab, $wl8_show_com_n_ex)
{
    if (!$wl8_show_com_n_ex) {
        return;
    }
    ?>

    <a class="nav-tab <?php echo ($current_tab == 'commission_report') ? 'nav-tab-active' : '' ?>" href="?page=instuctor&tab=commission_report"><?php echo __('Commission Report', 'wdm_instructor_role');
    ?></a>
    <a class="nav-tab <?php echo ($current_tab == 'export') ? 'nav-tab-active' : '' ?>" href="?page=instuctor&tab=export"><?php echo __('Export', 'wdm_instructor_role');
    ?></a>

    <?php

}

/**
 * Function shows mail tab.
 *
 * @param string $current_tab        current_tab
 * @param boolean $wl8_show_email_tab to check tab
 *
 * @return html to show tab
 */
function wl8ShowMailTab($current_tab, $wl8_show_email_tab, $wl8_show_com_n_ex)
{
    if (!$wl8_show_email_tab && !$wl8_show_com_n_ex) {
        return;
    } elseif (is_super_admin() || $wl8_show_email_tab) {
        ?>
        <a class="nav-tab <?php echo ($current_tab == 'email') ? 'nav-tab-active' : '' ?>" href="?page=instuctor&tab=email"><?php echo __('Email', 'wdm_instructor_role');
        ?></a>
        <?php
    }
}

/**
 * Function shows current tab.
 *
 * @param string $current_tab current tab
 *
 *
 * @since 2.4.0
 */
function wl8ShowCurrentTab($current_tab)
{
    switch ($current_tab) {
        case 'instructor':
            wdm_instructor_first_tab();
            break;
        case 'commission_report':
            wdm_instructor_second_tab();
            break;
        case 'export':
            wdm_instructor_third_tab();
            break;
        case 'email':
            if (is_super_admin()) {
                wdmir_instructor_email_settings();
            } else {
                wdmir_individual_instructor_email_setting();
            }
            break;
        case 'settings':
            wdmir_instructor_settings();
            break;
        case 'wdm_ir_promotion':
            wdmir_promotion();
            break;
    }

    do_action('instuctor_tab_checking', $current_tab);
}

/**
 * Showing other extentions link.
 */
function wdmir_promotion() {
    if (false === ($extensions = get_transient('_ir_extensions_data'))) {
        $extensions_json = wp_remote_get(
                        'https://wisdmlabs.com/products-thumbs/ld_extensions.json',
                        array(
                            'user-agent' => 'IR Extensions Page'
                        )
                    );
        if (!is_wp_error($extensions_json)) {
            $extensions = json_decode(wp_remote_retrieve_body($extensions_json));

            if ($extensions) {
                set_transient('_ir_extensions_data', $extensions, 72 * HOUR_IN_SECONDS);
            }
        }
    }
    include_once('templates/other-extensions.php');
    unset($extensions);
    $min = (defined('SCRIPT_DEBUG') && SCRIPT_DEBUG === true) ? '':'.min';

    wp_register_style('wdmir-promotion', plugins_url('css/extension' . $min . '.css', __FILE__), array(), '1.0.0');

    // Enqueue admin styles
    wp_enqueue_style('wdmir-promotion');
}

/**
 * [Displaying table for allocating instructor commission percentage].
 *
 * @return [html] [footable table for updating commission]
 *
 * @since 2.4.0
 */
function wdm_instructor_first_tab()
{
    wp_enqueue_script('wdm_instructor_report_js', plugins_url('/js/commission_report.js', __FILE__), array('jquery'));
    wp_enqueue_script('wdm_footable_pagination', plugins_url('/js/footable.paginate.js', __FILE__), array('jquery'));
    wp_enqueue_script('wdm_commission_js', plugins_url('/js/commission.js', __FILE__), array('jquery'));
    $data = array(
        'ajax_url' => admin_url('admin-ajax.php'),
        'invalid_percentage' => __('Invalid percentage', 'wdm_instructor_role'),
    );
    wp_localize_script('wdm_commission_js', 'wdm_commission_data', $data);
    // To show values in Commission column
    $instr_commissions = get_option('instructor_commissions', '');
    $instr_commissions = $instr_commissions;
    // To get user Ids of instructors
    $args = array('fields' => array('ID', 'display_name', 'user_email'), 'role' => 'wdm_instructor');
    $instructors = get_users($args);
    ?>
    <br/>
    <div id="reports_table_div" style="padding-right: 5px">
        <div class="CL"></div>
    <?php echo __('Search', 'wdm_instructor_role');
    ?>
        <input id="filter" type="text">
        <select name="change-page-size" id="change-page-size">
            <option value="5"><?php echo __('5 per page', 'wdm_instructor_role');
    ?></option>
            <option value="10"><?php echo __('10 per page', 'wdm_instructor_role');
    ?></option>
            <option value="20"><?php echo __('20 per page', 'wdm_instructor_role');
    ?></option>
            <option value="50"><?php echo __('50 per page', 'wdm_instructor_role');
    ?></option>
        </select>
        <br><br>
        <!--Table shows Name, Email, etc-->
        <table class="footable" data-filter="#filter"  id="wdm_report_tbl" data-page-size="5" >
            <thead>
                <tr>
                    <th data-sort-initial="descending" data-class="expand">
                        <?php echo __('Name', 'wdm_instructor_role');
    ?>
                    </th>
                    <th>
                        <?php echo __('User email', 'wdm_instructor_role');
    ?>
                    </th>
                    <th>
                        <?php echo __('Commission %', 'wdm_instructor_role');
    ?>
                    </th>
                    <th data-hide="phone" >
    <?php echo __('Update', 'wdm_instructor_role');
    ?>
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (!empty($instructors)) {
                    foreach ($instructors as $instructor) {
                        $commission_percent = get_user_meta($instructor->ID, 'wdm_commission_percentage', true);
                        if ('' == $commission_percent) {
                            $commission_percent = 0;
                        }
                        //echo '<pre>';print_R($instructor);echo '</pre>';
                        ?>
                        <tr>
                            <td><?php echo $instructor->display_name;
                        ?></td>
                            <td><?php echo $instructor->user_email;
                        ?></td>
                            <td><input name="commission_input" size="5" value="<?php echo $commission_percent;
                        ?>" min="0" max="100" type="number" id="input_<?php echo $instructor->ID;
                        ?>"></td>
                            <td><a name="update_<?php echo $instructor->ID;
                        ?>" class="update_commission button button-primary" href="#"><?php echo __('Update', 'wdm_instructor_role');
                        ?></a><img class="wdm_ajax_loader"src="<?php echo plugins_url('/images/ajax-loader.gif', __FILE__);
                        ?>" style="display:none;"></td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td colspan="4">
                    <?php echo __('No instructor found', 'wdm_instructor_role');
                    ?>
                        </td>
                    </tr>
    <?php
                }
    ?>
            </tbody>
            <tfoot class="hide-if-no-paging">
                <tr>
                    <td colspan="4" style="border-radius: 0 0 6px 6px;">
                        <div class="pagination pagination-centered hide-if-no-paging"></div>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
    <br/>
    <div id="update_commission_message"></div>


<?php

}

/**
 * [Commission report page].
 *
 * @return [html] [to show select tag of instructor]
 *
 * @since 2.4.0
 */
function wdm_instructor_second_tab()
{
    if (!is_super_admin()) {
        $instructor_id = get_current_user_id();
    } else {
        $args = array('fields' => array('ID', 'display_name'), 'role' => 'wdm_instructor');
        $instructors = get_users($args);
        $instructor_id = '';
        if (isset($_REQUEST['wdm_instructor_id'])) {
            $instructor_id = $_REQUEST['wdm_instructor_id'];
        }
        if (empty($instructors)) {
            echo __('No instructor found', 'wdm_instructor_role');

            return;
        }
        ?>
        <form method="post" action="?page=instuctor&tab=commission_report">
            <table>
                <tr>
                    <th><?php echo __('Select Instructor:', 'wdm_instructor_role');
        ?></th>
                    <td>
                        <select name="wdm_instructor_id">
                            <?php foreach ($instructors as $instructor) {
    ?>
                                <option value="<?php echo $instructor->ID;
    ?>" <?php echo ($instructor_id == $instructor->ID) ? 'selected' : '';
    ?>><?php echo $instructor->display_name;
    ?></option>

        <?php
}
        ?>
                        </select>
                    </td>

                    <td>
                        <input type="submit" value="<?php echo __('Submit', 'wdm_instructor_role');
        ?>" class="button-primary">
                    </td>
                </tr>
            </table>
        </form>
        <?php
    }
    if ('' != $instructor_id) {
        wdm_commission_report($instructor_id);
    }
}

/**
 * [Export tab for insturctor and admin].
 *
 * @return [html] [instructor_third_tab]
 *
 * @since 2.4.0
 */
function wdm_instructor_third_tab()
{
    if (!is_super_admin()) {
        $instructor_id = get_current_user_id();
    } else {
        $args = array('fields' => array('ID', 'display_name'), 'role' => 'wdm_instructor');
        $instructors = get_users($args);

        $instructor_id = '';
        if (isset($_REQUEST['wdm_instructor_id'])) {
            if ('-1' == $_REQUEST['wdm_instructor_id']) {
                $instructor_id = '-1';
            } else {
                $instructor_id = $_REQUEST['wdm_instructor_id'];
            }
        }
        if (empty($instructors)) {
            echo __('No instructor found', 'wdm_instructor_role');

            return;
        }
    }
    wp_enqueue_script('wdm_instructor_report_js', plugins_url('/js/commission_report.js', __FILE__), array('jquery'));
    $url = plugins_url('/js/jquery-ui.js', __FILE__);
    wp_enqueue_script('wdm-date-js', $url, array('jquery'), true);
    $url = plugins_url('/css/jquery-ui.css', __FILE__);
    wp_enqueue_style('wdm-date-css', $url);
    wp_enqueue_script('wdm-datepicker-js', plugins_url('/js/wdm_datepicker.js', __FILE__), array('jquery'));
    $start_date = wdmCheckIsSet($_POST['wdm_start_date']);//isset($_POST['wdm_start_date']) ? $_POST['wdm_start_date'] : '';
        $end_date = wdmCheckIsSet($_POST['wdm_end_date']);//isset($_POST['wdm_end_date']) ? $_POST['wdm_end_date'] : '';
        ?>
        <form method="post" action="?page=instuctor&tab=export">
            <table>
                <?php if (is_super_admin()) {
    ?>
                <tr>
                    <th style="float:left;"><?php echo __('Select Instructor:', 'wdm_instructor_role');
    ?></th>
                    <td>
                        <select name="wdm_instructor_id">
                            <option value="-1"><?php echo __('All', 'wdm_instructor_role');
    ?></option>
                            <?php foreach ($instructors as $instructor) {
    ?>
                                <option value="<?php echo $instructor->ID;
    ?>" <?php echo ($instructor_id == $instructor->ID) ? 'selected' : '';
    ?>><?php echo $instructor->display_name;
    ?></option>

        <?php
}
    ?>
                        </select>
                    </td>
                </tr>
                <?php
}
    ?>
                <tr>
                    <th style="float:left;"><?php echo __('Start Date:', 'wdm_instructor_role');
    ?></th>
                    <td>
                        <input type="text" name="wdm_start_date" id="wdm_start_date" value="<?php echo $start_date;
    ?>">
                    </td>
                </tr>
                <tr>
                    <th style="float:left;"><?php echo __('End Date:', 'wdm_instructor_role');
    ?></th>
                    <td>
                        <input type="text" name="wdm_end_date" id="wdm_end_date" value="<?php echo $end_date;
    ?>">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit" class="button-primary" value="<?php echo __('Submit', 'wdm_instructor_role');
    ?>" id="wdm_submit">
                    </td>
                </tr>
            </table>
        </form>
        <?php
        //}
        if ('' != $instructor_id) {
            wdm_export_csv_report($instructor_id, $start_date, $end_date);
        }
}

/**
 * [Report filtered by instructor, start and end date].
 *
 * @param [int]    $instructor_id [description]
 * @param [string] $start_date    [start_date]
 * @param [string] $end_date      [end_date]
 *
 * @return [html] [report in table format]
 *
 * @since 2.4.0
 */
function wdm_export_csv_report($instructor_id, $start_date, $end_date)
{
    global $wpdb;
    wp_enqueue_script('wdm_footable_pagination', plugins_url('/js/footable.paginate.js', __FILE__), array('jquery'));
    ?>
    <br><br>
    <div id="reports_table_div" style="padding-right: 5px">
        <div class="CL"></div>
    <?php echo __('Search', 'wdm_instructor_role');
    ?>
        <input id="filter" type="text">
        <select name="change-page-size" id="change-page-size">
            <option value="5"><?php echo __('5 per page', 'wdm_instructor_role');
    ?></option>
            <option value="10"><?php echo __('10 per page', 'wdm_instructor_role');
    ?></option>
            <option value="20"><?php echo __('20 per page', 'wdm_instructor_role');
    ?></option>
            <option value="50"><?php echo __('50 per page', 'wdm_instructor_role');
    ?></option>
        </select>
        <?php if (file_exists(INSTRUCTOR_ROLE_ABSPATH.'/includes/parsecsv.lib.php')) {
            $url = admin_url('admin.php?page=instuctor&tab=export&wdm_export_report=wdm_export_report&wdm_instructor_id='.$instructor_id.'&start_date='.$start_date.'&end_date='.$end_date);
    ?>
        <a href="<?php echo $url;
    ?>" class="button-primary" style="float:right"><?php echo __('Export CSV', 'wdm_instructor_role');
    ?></a>
        <?php
}

    ?>
        <!--Table shows Name, Email, etc-->
        <br><br>
        <table class="footable" data-filter="#filter" data-page-navigation=".pagination" id="wdm_report_tbl" data-page-size="5" >
            <thead>
                <tr>
                    <th data-sort-initial="descending" data-class="expand">
                        <?php echo __('Order ID', 'wdm_instructor_role');
    ?>
                    </th>
                    <th data-sort-initial="descending" data-class="expand">
                        <?php

                         echo showOwnerOrPurchaser();
    ?>
                    </th>
                    <th data-sort-initial="descending" data-class="expand">
                        <?php echo __('Product / Course Name', 'wdm_instructor_role');
    ?>
                    </th>
                    <th>
                        <?php echo __('Actual Price', 'wdm_instructor_role');
    ?>
                    </th>
                    <th>
    <?php echo __('Commission Price', 'wdm_instructor_role');
    ?>
                    </th>

                    <th>
                        <?php echo __('Product Type', 'wdm_instructor_role');
    ?>
                    </th>

                </tr>
                <?php do_action('wdm_commission_report_table_header', $instructor_id);
    ?>
            </thead>
            <tbody>
                <?php
                $sql = "SELECT * FROM {$wpdb->prefix}wdm_instructor_commission WHERE 1=1 ";
                //echo $start_date;exit;

                wdmCreateSQLQuery($instructor_id, $start_date, $end_date, $sql);

                $results = $wpdb->get_results($sql);
                $hasData = false;
                if (!empty($results)) {
                    foreach ($results as $value) {
                        if (!userIdExists($value->user_id)) {
                            continue;
                        }
                        $hasData = true;
                        $user_details = get_user_by('id', $value->user_id);

                        ?>
                        <tr>
                            <td>
                                <?php if (is_super_admin()) {
    ?>
                                    <a href="<?php echo wdmGetPostPermalink($value->order_id, $value->product_type);
    ?>" target="<?php echo needToOpenNewDocument();
    ?>"><?php echo $value->order_id;
    ?></a>

                                <?php
} else {
    echo $value->order_id;
}
            ?>
                            </td>
                            <td><?php echo wdmShowUserName($value->order_id, $user_details->display_name, $value->product_type);
                        ?></td>
                            <td><a target="_new_blank" <?php echo wdmGetPostEditLink($value->product_id);
                        ?>><?php echo wdmGetPostTitle($value->product_id);
            ?></a></td>
                            <td><?php echo $value->actual_price;
                        ?></td>
                            <td><?php echo $value->commission_price;
                        ?></td>
                            <td><?php echo $value->product_type;
                        ?></td>

                        </tr>
                        <?php
                    }
                } else {
                    $hasData = true;
                    ?>
                    <tr>
                        <td><?php echo __('No record found!', 'wdm_instructor_role');
                                ?></td>
                                </tr>
                                        <?php
                }
                if (!$hasData) {
                    ?>
                    <tr>
                        <td><?php echo __('No record found!', 'wdm_instructor_role');
                                ?></td>
                                </tr>
                                        <?php
                }
                do_action('wdm_commission_report_table', $instructor_id);
    ?>
            </tbody>
            <tfoot >

                <tr>
                    <td colspan="6" style="border-radius: 0 0 6px 6px;">
                        <div class="pagination pagination-centered hide-if-no-paging"></div>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
    <br>
    <?php
    if (file_exists(INSTRUCTOR_ROLE_ABSPATH.'/includes/parsecsv.lib.php')) {
        $url = admin_url('admin.php?page=instuctor&tab=export&wdm_export_report=wdm_export_report&wdm_instructor_id='.$instructor_id.'&start_date='.$start_date.'&end_date='.$end_date);
        ?>
        <a href="<?php echo $url;
        ?>" class="button-primary" style="float:right"><?php echo __('Export CSV', 'wdm_instructor_role');
        ?></a>
    <?php
    }
}

function wdmCreateSQLQuery($instructor_id, $start_date, $end_date, &$sql)
{
    if ('-1' != $instructor_id) {
        $sql .= "AND user_id = $instructor_id ";
    }
    if ('' != $start_date) {
        $start_date = Date('Y-m-d', strtotime($start_date));
        $sql .= "AND transaction_time >='$start_date 00:00:00'";
    }
    if ('' != $end_date) {
        $end_date = Date('Y-m-d', strtotime($end_date));
        $sql .= " AND transaction_time <='$end_date 23:59:59'";
    }
}

/**
 * [wdmShowUserName displaying owner/purchaser name according to product].
 *
 * @param [int]    $order_id     [order_id]
 * @param [string] $display_name [display_name]
 *
 * @return [string] [owner/purchaser name]
 */
function wdmShowUserName($order_id, $display_name, $product_type)
{
    $product_type_array = array(
        'WC' => '_customer_user',
        'LD' => 'LD', //v2.4.0
        );
    $product_type_array = apply_filters('wdm_product_type_array', $product_type_array);

    if (is_super_admin()) {
        return $display_name;
    }
    if (!isset($product_type_array[$product_type])) {
        $product_type_array['LD']='LD';
    }

    if ($product_type_array[$product_type]=='LD') {
        $ownerID=get_post_field('post_author', $order_id);
    } else {
        $ownerID = get_post_meta($order_id, $product_type_array[$product_type], true);
    }

    if (empty($ownerID)) {
        if (false === get_post_status($order_id)) {
            return __('Order has been deleted!', 'wdm_instructor_role');
        }
    }
    $user_info = get_userdata($ownerID);
    if ($user_info) {
        return $user_info->first_name.' '.$user_info->last_name;
    }
        return __('User not found!', 'wdm_instructor_role');


}

add_action('admin_init', 'wdm_export_csv_date_filter');

/**
 * [Export data filter wise].
 *
 * @return [file] [csv file]
 *
 * @since 2.4.0
 */
function wdm_export_csv_date_filter()
{
    if (isset($_GET['wdm_export_report']) &&  'wdm_export_report' == $_GET['wdm_export_report']) {
        global $wpdb;
        $instructor_id = $_REQUEST['wdm_instructor_id'];
        $start_date = $_GET['start_date'];
        $end_date = $_GET['end_date'];
        $sql = "SELECT * FROM {$wpdb->prefix}wdm_instructor_commission WHERE 1=1";
        if ('' != $instructor_id && '-1' != $instructor_id) {
            if (userIdExists($instructor_id)) {
                $sql .= ' AND user_id='.$instructor_id;
            }
        }
        if ('' != $start_date) {
            $start_date = Date('Y-m-d', strtotime($start_date));
            $sql .= " AND transaction_time >='$start_date 00:00:00'";
        }
        if ('' != $end_date) {
            $end_date = Date('Y-m-d', strtotime($end_date));
            $sql .= " AND transaction_time <='$end_date 23:59:59'";
        }

        $results = $wpdb->get_results($sql);

        $course_progress_data = array();

        if (empty($results)) {
            $row = array('No data' => __('No data found', 'wdm_instructor_role'));
        } else {
            foreach ($results as $value) {
                if (!userIdExists($value->user_id)) {
                    continue;
                }
                $user_data = get_user_by('id', $value->user_id);
                $row = array(
                    'Order id' => $value->order_id,
                    ''.showOwnerOrPurchaserTR() => wdmShowUserName($value->order_id, $user_data->display_name, $value->product_type),
                    'Actual price' => $value->actual_price,
                    'Commission price' => $value->commission_price,
                    'Product name' => wdmGetPostTitle($value->product_id),
                    'Transaction time' => $value->transaction_time,
                    'Product type' => $value->product_type,
                );

                $course_progress_data[] = $row;
            }
        }

        if (file_exists(INSTRUCTOR_ROLE_ABSPATH.'/includes/parsecsv.lib.php')) {
            require_once INSTRUCTOR_ROLE_ABSPATH.'/includes/parsecsv.lib.php';
            $csv = new lmsParseCSVNS\LmsParseCSV();
            if (empty($course_progress_data)) {
                $row = array();
                $row[] = array('' => __('No data found', 'wdm_instructor_role'));
                $csv->output(true, 'commission_report.csv', $row, array_keys(reset($row)));
            } else {
                $csv->output(true, 'commission_report.csv', $course_progress_data, array_keys(reset($course_progress_data)));
            }
            die();
        }
    }
}

/**
 * Function to check post is set or not.
 */
function wdmCheckIsSet($post)
{
    if (isset($post)) {
        return $post;
    }

    return '';
}

/**
 * Function to return site url to edit post, if current user is super admin.
 *
 * @param [string] $value [checking for admin]
 *
 * @return [string] [url]
 *
 * @since 2.4.0
 */
function wdmGetPostPermalink($value, $type = null)
{
    if (is_super_admin() && $type == 'EDD') {
        return site_url('wp-admin/edit.php?post_type=download&page=edd-payment-history&view=view-order-details&id='.$value);
    } elseif (is_super_admin()) {
        return site_url('wp-admin/post.php?post='.$value.'&action=edit');
    }

    return '#';
}

/**
 * Function returns string '_new_blank', if user is super admin.
 *
 * @return [string] [open in new blank tab if user is super admin.]
 *
 * @since 2.4.0
 */
function needToOpenNewDocument()
{
    if (is_super_admin()) {
        return '_new_blank';
    }

    return '';
}

/**
 * [showOwnerOrPurchaser showing heading if admin then username of ownwer if instructor then purchaser].
 *
 * @return [string] [heading]
 */
function showOwnerOrPurchaser()
{
    if (is_super_admin()) {
        return __('Instructor name', 'wdm_instructor_role');
    }

    return __('Purchaser name', 'wdm_instructor_role');
}

function showOwnerOrPurchaserTR()
{
    if (is_super_admin()) {
        return 'Instructor name';
    }

    return 'Purchaser name';
}

add_action('admin_init', 'wdm_export_commission_report');

/**
 * [Export functionality for admin as well as instructor].
 *
 * @return [nothing]
 *
 * @since 2.4.0
 */
function wdm_export_commission_report()
{
    if (isset($_GET['wdm_commission_report']) &&  'wdm_commission_report' == $_GET['wdm_commission_report']) {
        global $wpdb;
        $instructor_id = $_REQUEST['wdm_instructor_id'];
        $user_data = get_user_by('id', $instructor_id);

        $sql = "SELECT * FROM {$wpdb->prefix}wdm_instructor_commission WHERE user_id=$instructor_id";
        $results = $wpdb->get_results($sql);

        $course_progress_data = array();
        $amount_paid = 0;
        if (empty($results)) {
            $row = array('instructor name' => $user_data->display_name);
        } else {
            foreach ($results as $value) {
                $row = array(
                    'order id' => $value->order_id,
                    'instructor name' => $user_data->display_name,
                    'actual price' => $value->actual_price,
                    'commission price' => $value->commission_price,
                    'product name' => wdmGetPostTitle($value->product_id),
                    'transaction time' => $value->transaction_time,
                );
                $amount_paid = $amount_paid + $value->commission_price;
                $course_progress_data[] = $row;
            }
            $paid_total = get_user_meta($instructor_id, 'wdm_total_amount_paid', true);
            if ('' == $paid_total) {
                $paid_total = 0;
            }
            $amount_paid = round(($amount_paid - $paid_total), 2);
            $amount_paid = max($amount_paid, 0);
            $row = array(
                    'order id' => __('Paid Earnings', 'wdm_instructor_role'),
                    'instructor name' => $paid_total,
                    'actual price' => '',
                    'commission price' => '',
                    'product name' => '',
                    'transaction time' => '',
                    );
            $course_progress_data[] = $row;
            $row = array(
                    'order id' => __('Unpaid Earnings', 'wdm_instructor_role'),
                    'instructor name' => $amount_paid,
                    'actual price' => '',
                    'commission price' => '',
                    'product name' => '',
                    'transaction time' => '',
                    );
            $course_progress_data[] = $row;
        }

        if (file_exists(INSTRUCTOR_ROLE_ABSPATH.'/includes/parsecsv.lib.php')) {
            require_once INSTRUCTOR_ROLE_ABSPATH.'/includes/parsecsv.lib.php';
            $csv = new lmsParseCSVNS\LmsParseCSV();

            $csv->output(true, 'commission_report.csv', $course_progress_data, array_keys(reset($course_progress_data)));

            die();
        }
    }
}

/**
 * [Commission Report page].
 *
 * @param [int] $instructor_id [instructor_id]
 *
 * @return [html] [to show all the commission report]
 *
 * @since 2.4.0
 */
function wdm_commission_report($instructor_id)
{
    global $wpdb;
    wp_enqueue_script('wdm_footable_pagination', plugins_url('/js/footable.paginate.js', __FILE__), array('jquery'));
    wp_enqueue_script('wdm_instructor_report_js', plugins_url('/js/commission_report.js', __FILE__), array('jquery'));
    $data = array(
        'ajax_url' => admin_url('admin-ajax.php'),
        'enter_amount' => __('Please Enter amount', 'wdm_instructor_role'),
        'enter_amount_less_than' => __('Please enter amount less than amount to be paid', 'wdm_instructor_role'),
        'added_successfully' => __('Record added successfully', 'wdm_instructor_role'),
    );
    wp_localize_script('wdm_instructor_report_js', 'wdm_commission_data', $data);
    ?>
    <br><br>
    <div id="reports_table_div" style="padding-right: 5px">
        <div class="CL"></div>
    <?php echo __('Search', 'wdm_instructor_role');
    ?>
        <input id="filter" type="text">
        <select name="change-page-size" id="change-page-size">
            <option value="5"><?php echo __('5 per page', 'wdm_instructor_role');
    ?></option>
            <option value="10"><?php echo __('10 per page', 'wdm_instructor_role');
    ?></option>
            <option value="20"><?php echo __('20 per page', 'wdm_instructor_role');
    ?></option>
            <option value="50"><?php echo __('50 per page', 'wdm_instructor_role');
    ?></option>
        </select>

        <!--Table shows Name, Email, etc-->
        <br><br>
        <table class="footable" data-filter="#filter" data-page-navigation=".pagination" id="wdm_report_tbl" data-page-size="5" >
            <thead>
                <tr>
                    <th data-sort-initial="descending" data-class="expand">
                        <?php echo __('Order ID', 'wdm_instructor_role');
    ?>
                    </th>
                    <th data-sort-initial="descending" data-class="expand">
                        <?php echo __('Product / Course Name', 'wdm_instructor_role');
    ?>
                    </th>
                    <th>
                        <?php echo __('Actual Price', 'wdm_instructor_role');
    ?>
                    </th>
                    <th>
    <?php echo __('Commission Price', 'wdm_instructor_role');
    ?>
                    </th>
                    <th>
                        <?php echo __('Product Type', 'wdm_instructor_role') ?>
                    </th>

                </tr>
                <?php do_action('wdm_commission_report_table_header', $instructor_id);
    ?>
            </thead>
            <tbody>
                <?php
                $sql = "SELECT * FROM {$wpdb->prefix}wdm_instructor_commission WHERE user_id = $instructor_id";
                $results = $wpdb->get_results($sql);

                if (!empty($results)) {
                    $amount_paid = 0;
                    foreach ($results as $value) {
                        $amount_paid += $value->commission_price;

                        ?>
                        <tr>
                            <td>
                            <?php

                            wdmcheckProductType($value);
                        ?>
                            </td>
                            <td><a target="_new_blank" <?php echo wdmGetPostEditLink($value->product_id);
                        ?>><?php echo wdmGetPostTitle($value->product_id);
            ?></a></td>
                            <td><?php echo $value->actual_price;
                        ?></td>
                            <td><?php echo $value->commission_price;
                        ?></td>
                            <td><?php echo $value->product_type;
                        ?></td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td><?php echo __('No record found!', 'wdm_instructor_role');
                                ?></td>
                                </tr>
                                        <?php
                }
                do_action('wdm_commission_report_table', $instructor_id);
    ?>
            </tbody>
            <tfoot >
                <?php
                if (!empty($results)) {
                    $paid_total = get_user_meta($instructor_id, 'wdm_total_amount_paid', true);
                    if ('' == $paid_total) {
                        $paid_total = 0;
                    }

                    $amount_paid = round(($amount_paid - $paid_total), 2);
                    $amount_paid = max($amount_paid, 0);
                    ?>
                    <tr>
                        <td></td>
                        <th style="color:black;font-weight: bold;">
        <?php echo __('Paid Earnings', 'wdm_instructor_role');
                    ?>
                        </th>
                        <td><a><span id="wdm_total_amount_paid"><?php echo $paid_total;
                    ?></span></a></td>
                        <td></td><td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <th style="color:black;font-weight: bold;">
                            <?php echo __('Unpaid Earnings', 'wdm_instructor_role');
                    ?>
                        </th>
                        <td>

                            <span id="wdm_amount_paid"><?php echo $amount_paid;
                    ?></span>    <?php if (0 != $amount_paid && is_super_admin()) {
    ?>
                                <a href="#" class="button-primary" id="wdm_pay_amount"><?php echo __('Pay', 'wdm_instructor_role');
    ?></a>
                    <?php
                    }
                    ?>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>

    <?php
                }
    ?>
                <tr>
                    <td colspan="5" style="border-radius: 0 0 6px 6px;">
                        <div class="pagination pagination-centered hide-if-no-paging"></div>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
    <!-- popup div starts -->
    <div id="popUpDiv" style="display: none; top: 627px; left: 17%;">
        <div style="clear:both"></div>
        <table class="widefat" id="wdm_tbl_staff_mail">
            <thead>
                <tr>
                    <th colspan="2">
                        <strong><?php echo __('Transaction', 'wdm_instructor_role');
    ?></strong>

            <p id="wdm_close_pop" colspan="1" onclick="popup( 'popUpDiv' )"><span>X</span></p>
            </th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <strong><?php echo __('Paid Earnings', 'wdm_instructor_role');
    ?></strong>
                    </td>
                    <td>
                        <input type="number" id="wdm_total_amount_paid_price" value="" readonly="readonly">
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong><?php echo __('Unpaid Earnings', 'wdm_instructor_role');
    ?></strong>
                    </td>
                    <td>
                        <input type="number" id="wdm_amount_paid_price" value="" readonly="readonly">
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong><?php echo __('Enter amount', 'wdm_instructor_role');
    ?></strong>
                    </td>
                    <td>
                        <input type="number" id="wdm_pay_amount_price" value="" >
                    </td>
                </tr>
                <?php do_action('wdm_commisssion_report_popup_table', $instructor_id);
    ?>
                <tr>
                    <td colspan="2">
                        <input type="hidden" id="instructor_id" value="<?php echo $instructor_id;
    ?>">
                        <input class="button-primary" type="button" name="wdm_btn_send_mail" value="<?php echo __('Pay', 'wdm_instructor_role');
    ?>" id="wdm_pay_click"><img src="<?php echo plugins_url('/images/ajax-loader.gif', __FILE__);
    ?>" style="display: none" class="wdm_ajax_loader">
                    </td>
                </tr>
            </tbody>
        </table>
    </div> <!-- popup div ends -->
<?php

}

function wdmcheckProductType($value)
{
    if (is_super_admin()) {
        if ($value->product_type == 'EDD') {
            ?>
                                    <a href="<?php echo is_super_admin() ? site_url('wp-admin/edit.php?post_type=download&page=edd-payment-history&view=view-order-details&id='.$value->order_id) : '#';
            ?>" target="<?php echo is_super_admin() ? '_new_blank' : '';
            ?>"><?php echo $value->order_id;
            ?></a>
                                <?php
        } else {
            ?>
                                    <a href="<?php echo is_super_admin() ? site_url('wp-admin/post.php?post='.$value->order_id.'&action=edit') : '#';
            ?>" target="<?php echo is_super_admin() ? '_new_blank' : '';
            ?>"><?php echo $value->order_id;
            ?></a>

                                <?php
        }
    } else {
        echo $value->order_id;
    }
}

add_action('wp_ajax_wdm_update_commission', 'wdm_update_commission');

/**
 * [Updating instructor commission using ajax].
 *
 * @return [string] [status]
 *
 * @since 2.4.0
 */
function wdm_update_commission()
{
    $percentage = $_POST['commission'];
    $instructor_id = $_POST['instructor_id'];
    if (wdm_is_instructor($instructor_id)) {
        update_user_meta($instructor_id, 'wdm_commission_percentage', $percentage);
        echo __('Updated successfully', 'wdm_instructor_role');
    } else {
        echo __('Oops something went wrong', 'wdm_instructor_role');
    }
    die();
}

/**
 * [Function returns tab name if set else returns default tab 'instructor'.].
 *
 * @param [string] $tab [tab name]
 *
 * @return [string] [tab name]
 *
 * @since 2.4.0
 */
// function conditionalOperatorForInstructorPageCallback($tab)
// {
//     if (isset($tab)) {
//         return $tab;
//     }

//     return 'instructor';
// }

/**
 * [Function to returns css class nav-tab-active.].
 *
 * @param [string] $current_tab [checking current tab]
 *
 * @return [string] [active tag or nothing]
 *
 * @since 2.4.0
 */
// function conditionalOperatorInstructorPageCallback($current_tab)
// {
//     if ($current_tab == 'instructor' || $current_tab == 'commission_report'
//         || $current_tab == 'export' || $current_tab == 'email' || $current_tab == 'settings') {
//         return 'nav-tab-active';
//     }

//     return '';
// }

function userIdExists($user_id)
{
    global $wpdb;
    $count = $wpdb->get_var($wpdb->prepare("SELECT COUNT(ID) FROM $wpdb->users WHERE ID = %d", $user_id));

    return empty($count) || 1 > $count ? false : true;
}

function wdmGetPostTitle($postID)
{
    $title=get_the_title($postID);
    if (empty($title)) {
        return __('Product/Course has been deleted !', 'wdm_instructor_role');
    }
    return $title;
}


function wdmGetPostEditLink($postId = 0)
{

    if (get_post_status($postId)===false) {
        return 'style="pointer-event:none;"';
    }
    return "href=\"".site_url('wp-admin/post.php?post='.$postId.'&action=edit')."\"";

}