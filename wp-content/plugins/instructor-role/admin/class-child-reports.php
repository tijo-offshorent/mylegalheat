<?php

namespace wdmInstructorRoleReport

{
    /**
     * This class is used to show reports on dashboard part.
     */
    class ChildInstructorReports extends \wdmInstructorRoleReport\InstructorReports
    {
        /**
         * Instance of this class.
         *
         * @since    1.0.0
         *
         * @var object
         */
        protected static $instance = null;

        public function __construct()
        {
            parent::__construct();
            // To add report page in the dashboard menu
            add_action('admin_menu', array($this, 'addReportMenuPage'), 1001);

            add_action('current_screen', array($this, 'sendMailFromReportPage'));

            //Ajax call for showing report
            add_action('wp_ajax_wdm_get_report_html', array($this, 'wdmShowReportHtml'));
            add_action('wp_ajax_wdm_get_user_html', array($this, 'wdmGetUserHtml'));
            add_action('admin_footer', array($this, 'myAdminFooterFunction'));
            //Hook for ajax where it sends mail to individual user
            add_action('wp_ajax_wdm_send_mail_to_individual_user', array($this, 'wdmSendMailToIndividualUser'));
            add_action('wp_ajax_nopriv_wdm_send_mail_to_individual_user', array($this, 'wdmSendMailToIndividualUser'));

            //For exporting users data in CSV file
            add_action('admin_init', array($this, 'wdmLearndashCsvExport'));
            add_filter('wp_mail_from', array($this, 'wdmChangeMailFrom'), 1, 1);
            add_filter('wp_mail_from_name', array($this, 'wdmChangeMailFromName'), 999, 1);
            //add_filter('wp_mail_smtp_custom_options', array($this, 'wpMailSmtpCustomOptions'), 999, 1);
            add_action('phpmailer_init', array($this, 'wdmWpSmtp'), 999, 1);

        }
        /**
         * [wpMailSmtpCustomOptions description]
         * @param  [type] $phpmailer [description]
         * @return [type]            [description]
         */
        public function wpMailSmtpCustomOptions($phpmailer)
        {
            if (!is_super_admin() && wdm_is_instructor()) {
                $current_user = wp_get_current_user();
                if ($current_user) {
                     $phpmailer->Sender = esc_html($current_user->user_email);
                }
            }
            return $phpmailer;
        }
        /**
         * Making compatible with wp-smtp plugin.
         * @param object $phpmailer email configuaration
         * @since  3.0.0
         */
        public function wdmWpSmtp($phpmailer)
        {
            if (!is_super_admin() && wdm_is_instructor()) {
                $current_user = wp_get_current_user();
                if ($current_user) {
                    global $wsOptions;
                    if (!is_email($wsOptions["from"]) || empty($wsOptions["host"]) || !isset($wsOptions)) {
                        return;
                    }
                    $phpmailer->Mailer = "smtp";
                    $phpmailer->From = esc_html($current_user->user_email);
                    $wsOptions["from"]= esc_html($current_user->user_email);
                    if (!empty($current_user->user_firstname)) {
                        $phpmailer->FromName = esc_html($current_user->user_firstname);
                    }
                    $phpmailer->FromName = esc_html($current_user->user_login);
                    $phpmailer->Sender = esc_html($current_user->user_email); //Return-Path
                    $phpmailer->AddReplyTo($phpmailer->From, $phpmailer->FromName); //Reply-To
                    $phpmailer->Host = $wsOptions["host"];
                    $phpmailer->SMTPSecure = $wsOptions["smtpsecure"];
                    $phpmailer->Port = $wsOptions["port"];
                    $phpmailer->SMTPAuth = ($wsOptions["smtpauth"]=="yes") ? true : false;
                    if ($phpmailer->SMTPAuth) {
                        $phpmailer->Username = $wsOptions["username"];
                        $phpmailer->Password = $wsOptions["password"];
                    }
                }
            }

        }

        /**
         * This function will change the from_name email id if it is send by instructor
         * @param  string $from_name
         * @return string $from_name
         * @since  3.0.0
         */
        public function wdmChangeMailFromName($from_name)
        {


            if (!is_super_admin() && wdm_is_instructor()) {
                $current_user = wp_get_current_user();
                if ($current_user) {
                    if (!empty($current_user->user_firstname)) {
                        return esc_html($current_user->user_firstname);
                    }
                    return esc_html($current_user->user_login);
                }
            }
            return $from_name;
        }

        /**
         * This function will change the from email id if it is send by instructor
         * @param  string $from_email
         * @return string $from_email
         * @since  3.0.0
         */
        public function wdmChangeMailFrom($from_email)
        {

            if (!is_super_admin() && wdm_is_instructor()) {
                $current_user = wp_get_current_user();
                if ($current_user) {
                    return esc_html($current_user->user_email);
                }
            }
            return $from_email;
        }

        /**
         * Returns an instance of this class.
         *
         * @since     1.0.0
         *
         * @return object A single instance of this class.
         */
        public static function getInstance()
        {
            // If the single instance hasn't been set, set it now.
            if (null == self::$instance) {
                self::$instance = new self();
            }

            return self::$instance;
        }

        /**
         * Registers a menu for customized LMS reports.
         *
         * @since     1.0.0
         *
         * @return
         */
        public function addReportMenuPage()
        {
            add_submenu_page('learndash-lms', __('Course Reports', 'wdm_instructor_role'), __('Course Reports', 'wdm_instructor_role'), 'instructor_reports', 'instructor_lms_reports', array('\wdmInstructorRoleReport\ChildInstructorReports', 'reportPageCallback'));
        }
        /**
         * [reportPageCallback this function to display content].
         *
         * @return [html] [report page]
         */
        public function reportPageCallback()
        {
            ?>
    <div  id="learndash-instructor-reports"  class="wrap">
        <h2><?php _e('Course Reports', 'wdm_instructor_role');
            ?></h2>
        <br>
        <div class="sfwd_settings_left">
            <div class=" " id="div-instructor-courses">
                <div class="inside">
                    <?php
                    $allCoursesList = get_posts('post_type=sfwd-courses&posts_per_page=-1');
                    $current_post = 0;
                    $found = 0;

                    if (isset($_REQUEST['course_id'])) {
                        $current_post = $_REQUEST['course_id'];
                        $current_course_title = get_the_title($_REQUEST['course_id']);
                        $course_cnt = 1;
                        $found = 1;
                    } else {
                        $course_cnt = 0;
                    }

                    if (!empty($allCoursesList)) {
                        $wdm_report_str = '';
                        $wdm_report_str .= '<label class="wdm-filter-title">';
                        $wdm_report_str .= __('Select Course', 'wdm_instructor_role').': ';
                        $wdm_report_str .= '</label>';
                        $wdm_report_str .= '<select name = "sel-instructor-courses" id = "instructor-courses" onchange="wdm_change_report( this )">';

                        $currentIndex = 0;
                        foreach ($allCoursesList as $value) {
                            if (count(self::wdmGetCourseUsers($value->ID) > 0)) {
                                if ($current_post == $value->ID) {
                                    $selected = 'selected';
                                }
                                $wdm_report_str .= '<option value="'.$value->ID.'"'.$selected.'">'.$value->post_title.'</option>';
                                $selected = '';

                                if (0 == $course_cnt) {
                                    $current_post = $value->ID;
                                    $current_course_title = $value->post_title;
                                    $found = 1;
                                }
                                ++$course_cnt;
                            }
                            ++$currentIndex;
                        }

                        $wdm_report_str .= '</select>';

                        if (0 === $found) {
                            echo '<br/>';
                            echo __('No reports to display', 'wdm_instructor_role');
                        } else {
                            echo $wdm_report_str;
                        }

                                echo '<div id="wdm_main_report_div" >';

                                $report_html = self::wdmShowReportHtml($current_post);

                                $arr_report_html = json_decode($report_html, true);

                        if (isset($arr_report_html['html'])) {
                            echo $arr_report_html['html'];
                        }

                                echo '</div>';
                    } else {
                        echo __('No reports to display', 'wdm_instructor_role');
                    }
            ?>
                </div>
            </div>
        </div>
    </div>
    <?php

        }

        /**
         * [wdmLoadScripts function to load all js and css file for loading pie chart].
         */
        public function wdmLoadScripts()
        {

            // autosave dependency because of Tinymce editor was making leave page alert when publishing new post.
            wp_enqueue_script('wdm_new_reports', plugin_dir_url(__FILE__).'js/reports.js', array('jquery'), '0.0.4');
        }

        /**
         * [wdmShowReportHtml function to generate report in HTML].
         *
         * @param [int] $current_post [current course id]
         * @param int   $GroupId      [current group id]
         *
         * @return [HTML] [report in html]
         */
        public function wdmShowReportHtml($current_post = null)
        {
            $current_post = parent::checkIsSetCurrentPost($current_post, $_POST['course_id']);
            if (empty($current_post)) {
                return;
            }

            ob_start();

            $current_course_title = get_the_title($current_post);

            $course_access_users = self::wdmGetCourseUsers($current_post);

            $total_users = count($course_access_users);
            $arr_course_status = array('not_started' => 0, 'in_progress' => 0, 'completed' => 0, 'total' => $total_users);

            $wdm_course_users = array();

            $wdm_page_counter = 10;

            $wdm_page_counter = parent::wdmPageCounter($wdm_page_counter, $_POST['wdm_pagination_select']);

            $wdm_page_users = array();
            $arr_count = count($course_access_users);
            $wdm_page_count = 0;
            if (!empty($course_access_users)) {
                $cnt = 0;
                foreach ($course_access_users as $user_id) {
                    $course_status_user = learndash_course_status($current_post, $user_id);

                    $wdm_page_users[ $wdm_page_count ][] = $user_id;

                    if (0 == (($cnt + 1) % $wdm_page_counter) || ($cnt + 1) == $arr_count) {
                        $wdm_page_users[ $wdm_page_count ] = implode(',', $wdm_page_users[ $wdm_page_count ]);

                        ++$wdm_page_count;
                    }

                    if ($cnt < $wdm_page_counter) {
                        $user_meta = get_userdata($user_id);
                        $wdm_course_users[ $cnt ]['user_name'] = $user_meta->data->user_login;
                        $wdm_course_users[ $cnt ]['user_email'] = $user_meta->data->user_email;
                        //var_dump(parent::checkIsSet($course_progress['total_steps'],$current_post));
                        $course_progress = parent::wdmGetCourseProgressInPer($user_id, $current_post);
                        $wdm_course_users[ $cnt ]['completed_per'] = parent::checkIsSet($course_progress['percentage']);
                        $wdm_course_users[ $cnt ]['total_steps'] = parent::checkIsSet($course_progress['total_steps'], $current_post);
                        $wdm_course_users[ $cnt ]['completed_steps'] = parent::checkIsSet($course_progress['completed_steps']);
                        $wdm_course_users[ $cnt ]['course_completed_on'] = parent::checkCourseCompletedOn($course_progress['course_completed_on']);
                    }

                    switch ($course_status_user) {
                //case "Not Started":
                        case __('Not Started', 'wdm_instructor_role'):
                            $arr_course_status['not_started']++;
                            break;
                //case "In Progress":
                        case __('In Progress', 'wdm_instructor_role'):
                            $arr_course_status['in_progress']++;
                            break;
                //case "Completed":
                        case __('Completed', 'wdm_instructor_role'):
                            $arr_course_status['completed']++;
                            break;
                    }

                    ++$cnt;
                }
                $not_started_str = '';
                $in_progress_str = '';
                $completed_str = '';
                $not_started_per = '';
                $in_progress_per = '';
                $completed_per = '';

                parent::updateCourseStatus($arr_course_status, $not_started_per, $in_progress_per, $completed_per, $not_started_str, $in_progress_str, $completed_str);
            } // if ( ! empty( $instructor_courses ) )

            $report_js_array = array(
            'not_started_text' => __('Not Started', 'wdm_instructor_role'),
            'in_progress_text' => __('In Progress', 'wdm_instructor_role'),
            'completed_text' => __('Completed', 'wdm_instructor_role'),
            'not_started_per' => parent::checkIsSet($not_started_per),
            'in_progress_per' => parent::checkIsSet($in_progress_per),
            'completed_per' => parent::checkIsSet($completed_per),
            'graph_heading' => __('Status of', 'wdm_instructor_role').' "'.$current_course_title.'"',
            'piece_title' => __('Users', 'wdm_instructor_role'),
            'course_title' => $current_course_title,
            'admin_ajax_path' => admin_url('admin-ajax.php'),
            'paged_users' => ($wdm_page_users),
            'paged_index' => 0,
            'success_msg' => __(' Mail sent successfully!!! ', 'wdm_instructor_role'),
            'failure_msg' => __(' Mail not sent!!! ', 'wdm_instructor_role'),
            );
            self::wdmLoadScripts();
            wp_localize_script('wdm_new_reports', 'wdm_reports_obj', $report_js_array);
            ?>
    <div id="wdm-report-graph">
        <div id="wdm_left_report">
            <?php
            echo $not_started_str;
            echo $in_progress_str;
            echo $completed_str;
            ?>
        </div>
        <div id="wdm_report_div" ></div><!-- highchart div -->
        <!--    added form for mail to all the users of that particular course -->
        <div id="mail_by_instructor">
            <form method="post" id="instructor_message_form">
                <h4 class="learndash_instructor_send_message_label"><?php echo sprintf(__('Send message to all %s users', 'wdm_instructor_role'), '<i>'.$current_course_title.'</i>');
            ?></h4>
                <label><?php _e('Subject:', 'wdm_instructor_role');
            ?> </label><span id="learndash_instructor_subject_err"></span><br>
                <input type="text" size="40" id="learndash_instructor_subject" name="learndash_instructor_subject" style="margin-bottom: 15px;"><br>
                <div class="learndash_instructor_message_label"><label for="learndash_instructor_message_label"><?php _e('Body:', 'wdm_instructor_role');
            ?> </label><span id="learndash_instructor_message_err"></span></div><textarea id="learndash_instructor_message" rows="10" cols="40" id="learndash_propanel_message" name="learndash_instructor_message"></textarea><br>
                <input class="wdm-button" type="submit" name="submit_instructor_email" value="<?php _e('Send Email', 'wdm_instructor_role');
            ?>">
             <input type="hidden" name="course_id" value="<?php echo $current_post;
            ?>">
            </form>

        </div>
        <div class="CL" ></div>
    </div>
    <div id="user_info">
        <h3><?php echo __('User Information', 'wdm_instructor_role');
            ?></h3>
        <div id="reports_table_div">
            <div class="CL"></div>
            <form action="" method="post" id="wdm_pagination_frm">
            <?php echo __('Search', 'wdm_instructor_role');
            ?>
            <input id="filter" type="text"> <?php echo __('Show', 'wdm_instructor_role');
            ?>
                <input type="hidden" value="<?php echo $current_post;
            ?>" name="course_id" />
                <select name="wdm_pagination_select" onchange="jQuery('#wdm_pagination_frm').submit();">
                    <option value="10" <?php parent::paginationSelectData(10, $wdm_page_counter)

    ?>>10</option>
                    <option value="25" <?php parent::paginationSelectData(25, $wdm_page_counter);

            ?>>25</option>
                    <option value="50" <?php parent::paginationSelectData(50, $wdm_page_counter);

            ?>>50</option>
                    <option value="100" <?php parent::paginationSelectData(100, $wdm_page_counter);

            ?>>100</option>
                </select> <?php echo __('Records', 'wdm_instructor_role');
            ?>
            </form>
            <!--Table shows Name, Email, etc-->
            <table class="footable" data-page-navigation=".pagination" data-filter="#filter" id="wdm_report_tbl" >
                <thead>
                    <tr>
                        <th data-sort-initial="descending" data-class="expand">
                            Name
                        </th>
                        <th><?php
                        echo __('E-Mail ID', 'wdm_instructor_role');
            ?>
                        </th>
                        <th data-hide="phone" >
                        <?php
                        echo __('Progress %', 'wdm_instructor_role');
            ?>
                        </th>
                        <th data-hide="phone" >
                        <?php
                        echo __('Total Steps', 'wdm_instructor_role');
            ?>
                        </th>
                        <th data-hide="phone" >
                        <?php
                        echo __('Completed Steps', 'wdm_instructor_role');
            ?>
                        </th>
                        <th data-hide="phone,tablet" >

                        <?php
                        echo __('Completed On', 'wdm_instructor_role');
            ?>
                        </th>
                        <th data-hide="phone,tablet" data-sort-ignore="true">
                        <?php
                        echo __('Email', 'wdm_instructor_role');
            ?>

                        </th>
                    </tr>
                </thead>
                <tbody>
                <?php
                self::wdmGetUserHtmlFunction($wdm_page_users, $current_post);
            ?>
                </tbody>
<?php
self::writeTFooterfunction($wdm_page_count);
            ?>
            </table>
        </div>
        <div class="CL"></div>
        <form method="post" action="">
            <input type="hidden" value="<?php echo $current_post;
            ?>" name="post_id_report" id="post_id_report" />
            <input class="wdm-button" type="submit" value="Export Course Data" />
        </form>


    </div>
    <!--For popup email div for individual-->
    <div id="popUpDiv" style="display: none; top: 245.75px; left: 17%;">
        <div style="clear:both"></div>
        <table class="widefat" id="wdm_tbl_staff_mail">
            <thead>
                <tr>
                    <th colspan="2">
                        <strong>

                <?php
                        echo __('Send E-Mail To Individual Member', 'wdm_instructor_role');
            ?>

                        </strong>

            <p id="wdm_close_pop" colspan="1" onclick="popup( 'popUpDiv' )"><span>X</span></p>
            </th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <?php
                        echo __('To', 'wdm_instructor_role');
            ?>
                    </td>
                    <td>
                        <input type="text" id="wdm_staff_mail_id" value="" readonly="readonly">
                    </td>
                </tr>
                <tr>
                    <td>
                    <?php
                        echo __('Subject', 'wdm_instructor_role');
            ?>
                    </td>
                    <td>
                        <input type="text" id="wdm_staff_mail_subject" value="">
                    </td>
                </tr>
                <tr>
                    <td>
                    <?php
                        echo __('Body', 'wdm_instructor_role');
            ?>
                    </td>
                    <td>
                        <textarea id="wdm_staff_mail_body" rows="8"></textarea>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input class="button-primary" type="button" name="wdm_btn_send_mail" value="Send E-Mail" id="wdm_btn_send_mail" onclick="wdm_individual_send_email();"><span id="wdm_staff_mail_msg"></span>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <!--Email popup-->
    <?php
    $output = ob_get_contents();
            ob_end_clean();

            $data = array(
        'html' => $output,
        'not_started_per' => $not_started_per,
        'in_progress_per' => $in_progress_per,
        'completed_per' => $completed_per,
        'graph_heading' => __('Status of', 'wdm_instructor_role').' "'.$current_course_title.'"',
            'paged_users' => ($wdm_page_users),
            'paged_index' => 0,
            );

            if (isset($_POST['course_id'])  &&
            isset($_POST['request_type']) && 'ajax' == $_POST['request_type']) {
                echo json_encode($data);
                die();
            } else {
                return json_encode($data);
            }
        }//function end

        public function setGroupID(&$GroupId)
        {
            if ($GroupId == 0 && isset($_POST['group_id'])) {
                $GroupId = $_POST['group_id'];
            }
        }

        public function wdmGetUserHtmlFunction($wdm_page_users, $current_post)
        {
            if (count($wdm_page_users) > 0) {
                self::wdmGetUserHtml($wdm_page_users[0], $current_post);
            }
        }

        public function writeTFooterfunction($wdm_page_count)
        {
            if ($wdm_page_count > 1) {
                ?>
                <tfoot class="wdm-pagination">
                <tr>
                <td colspan="10" id="wdm_paged_td">
            <?php
            echo parent::wdmGetPagedHtml($wdm_page_count);
                ?>
                </td>
                </tr>
            </tfoot>
            <?php
            }
        }

        /**
         * get all users ids of a course.
         *
         * @param int $post_id post id of a course
         *
         * @return array array of users id
         */
        public function wdmGetCourseUsers($post_id)
        {
            $course_access_users = array();

            if (empty($post_id)) {
                return $course_access_users;
            }

            $course_progress_data = array();
            $courses = array(get_post($post_id));
            $users = get_users();
            if (!empty($users)) {
                self::wdmGetCourseUserData($users, $courses, $course_progress_data);
            }
            foreach ($course_progress_data as $value) {
                array_push($course_access_users, $value['user_id']);
            }

            return $course_access_users;
        }
        /**
         * [wdmGetCourseUserData returns course progress of user].
         *
         * @param [array] $users                 [users]
         * @param [array] $courses               [courses]
         * @param [array] &$course_progress_data [course_progress_data]
         */
        public function wdmGetCourseUserData($users, $courses, &$course_progress_data)
        {
            foreach ($users as $eachUser) {
                $user_id = $eachUser->ID;
                $usermeta = get_user_meta($user_id, '_sfwd-course_progress', true);
                $usermeta = parent::wdmCheckAndUnserialize($usermeta);
                if (!empty($usermeta)) {
                    $usermeta = maybe_unserialize($usermeta);
                }

                if (!empty($courses[0])) {
                    foreach ($courses as $course) {
                        $c = $course->ID;

                        if (empty($course->post_title) || !sfwd_lms_has_access($c, $user_id)) {
                            continue;
                        }

                        $cv = parent::wdmCheckCourseProgress($usermeta[ $c ]);

                        $cours_completed_meta = get_user_meta($user_id, 'course_completed_'.$course->ID, true);
                        $cours_completed_date = parent::getCourseCompleteDate($cours_completed_meta);

                        $row = array('user_id' => $user_id, 'name' => $eachUser->display_name, 'email' => $eachUser->user_email, 'course_id' => $c, 'course_title' => $course->post_title, 'total_steps' => $cv['total'], 'completed_steps' => $cv['completed'], 'course_completed' => (!empty($cv['total']) && $cv['completed'] >= $cv['total']) ? 'YES' : 'NO', 'course_completed_on' => $cours_completed_date);
                        $i = 1;
                        if (!empty($cv['lessons'])) {
                            foreach ($cv['lessons'] as $lesson_id => $completed) {
                                if (!empty($completed)) {
                                    if (empty($lessons[ $lesson_id ])) {
                                        $lesson = $lessons[ $lesson_id ] = get_post($lesson_id);
                                    } else {
                                        $lesson = $lessons[ $lesson_id ];
                                    }

                                    $row[ 'lesson_completed_'.$i ] = $lesson->post_title;
                                    ++$i;
                                }
                            }
                        }

                        $course_progress_data[] = $row;
                        //return $row;
                    }
                }
            }
        }

        /**
         * [wdmGetUserHtml This function is used to create HTML table rows in report table to given user ids and course id].
         *
         * @param string $users        [users]
         * @param string $current_post [current_post]
         *
         * @return [string] [html content]
         */
        public function wdmGetUserHtml($users = '', $current_post = '')
        {
            $return_str = '';

            $user_ids = parent::wdmGetUsersInGetUserHtml($_POST['users'], $users);
            $user_ids = explode(',', $user_ids);
            $user_ids = array_filter($user_ids);
            $current_post = isset($_POST['current_post']) ? $_POST['current_post'] : $current_post;

            if (!empty($user_ids) && !empty($current_post)) {
                $wdm_course_users = array();
                $cnt = 0;
                foreach ($user_ids as $user_id) {
                    $user_meta = get_userdata($user_id);
                    $wdm_course_users[ $cnt ]['user_name'] = $user_meta->data->user_login;
                    $wdm_course_users[ $cnt ]['user_email'] = $user_meta->data->user_email;

                    $course_progress = parent::wdmGetCourseProgressInPer($user_id, $current_post);

                    $wdm_course_users[ $cnt ]['completed_per'] = parent::checkIsSet($course_progress['percentage']);
                    $wdm_course_users[ $cnt ]['total_steps'] = parent::checkIsSet($course_progress['total_steps'], $current_post);
                    $wdm_course_users[ $cnt ]['completed_steps'] = parent::checkIsSet($course_progress['completed_steps']);
                    $wdm_course_users[ $cnt ]['course_completed_on'] = parent::checkCourseCompletedOn($course_progress['course_completed_on']);
                    ++$cnt;
                }

                foreach ($wdm_course_users as $user_val) {
                    $return_str .= '<tr>
                <td>'.$user_val['user_name'].'</td>
                <td>'.$user_val['user_email'].'</td>
                <td>'.$user_val['completed_per'].'</td>
                <td>'.$user_val['total_steps'].'</td>
                <td>'.$user_val['completed_steps'].'</td>
                <td>'.$user_val['course_completed_on'].'</td>
                <td><a href="javascript:wdm_show_email_form(\''.$user_val['user_email'].'\');" title="E-Mail to '.$user_val['user_email'].'">'.__('E-Mail', 'wdm_instructor_role').'</a></td>
            </tr>';
                }
                echo $return_str;
            } else {
                return;
            }
            if (isset($_POST['users'])) {
                // if ajax call
                die();
            }
        }

        /**
         * [wdmSendMailToIndividualUser sends mail to individual a user].
         *
         * @return [boolean] [true or false]
         */
        public function wdmSendMailToIndividualUser()
        {
            $email = '';
            if (isset($_POST['email'])) {
                $email = $_POST['email'];
            }

            if ($email) {
                if (isset($_POST['subject'])) {
                    $subject = strip_tags($_POST['subject']);
                }

                if (isset($_POST['body'])) {
                    $body = $_POST['body'];
                }
                $headers = array('Content-Type: text/html; charset=UTF-8');
                if (wp_mail($email, $subject, $body, $headers)) {
                    echo 1;
                } //On successful message sent
                else {
                    echo 0;
                }
            }

            die();
        }

        /**
         * [sendMailFromReportPage sends mail to multiple users of that group].
         */
        public function sendMailFromReportPage()
        {
            if (empty($_POST['submit_instructor_email']) || empty($_POST['learndash_instructor_message']) || empty($_POST['learndash_instructor_subject']) || !isset($_POST['course_id'])) {
                return;
            }

            $course_id = $_POST['course_id'];
        // To get email ids of users
        //     $course_progress_data = self::wdmCourseProgressData($group_id, $_POST['course_id']);
        //     // var_dump($course_progress_data);
        //     if (empty($course_progress_data)) {
        //         return;
        //     }
        // //To get email ids we will get course progress data
        //     $users    = $course_progress_data;

            $users = self::wdmGetCourseUsers($course_id);
            if (empty($users)) {
                return;
            }

        // Check if the "from" input field is filled out
            $message = stripslashes($_POST['learndash_instructor_message']);
            $subject = stripslashes($_POST['learndash_instructor_subject']);
            $subject = strip_tags($subject);
        // message lines should not exceed 70 characters (PHP rule), so wrap it
            $message = wordwrap($message, 70);
        // send mail
            $headers = array('Content-Type: text/html; charset=UTF-8');

            foreach ($users as $user_id) {
                $user = get_user_by('ID', $user_id);
                wp_mail($user->data->user_email, $subject, $message, $headers);
            }
        // To redirect to the page after sending email
            $server_req_uri = $_SERVER['REQUEST_URI'];
            $url = parse_url($server_req_uri, PHP_URL_QUERY);
            parse_str($url, $url_params);
            $url_params_string = '?page='.$url_params['page'].'&course_id='.$_POST['course_id'];
            $url = explode('?', $server_req_uri);
            wp_redirect($url[0].$url_params_string);
            exit;
        }

        public function wdmGetCourseList($course_id, &$courses)
        {
            if (!empty($course_id)) {
                $courses = array(get_post($course_id));
            } else {
                $courses = ld_course_list(array('array' => true));
            }
        }

        /**
         * [wdmCourseProgressData gets course progress data of that group].
         *
         * @param [int] $group_id [group id]
         * @param [int] $courseID [courseID]
         *
         * @return [array] [course progress]
         */
        public function wdmCourseProgressData($courseID = null)
        {
            $current_user = wp_get_current_user();
            if (empty($current_user) || !current_user_can('instructor_reports')) {
                return;
            }

            $users = get_users();
            //$users=get_users(learndash_get_groups_user_ids($group_id));
            //$users = learndash_get_groups_users($group_id);
            $course_progress_data = array();

            $lessons = array();
            $courses = array();

            self::wdmGetCourseList($courseID, $courses);

            //print_r($courses);
            if (!empty($users)) {
                // echo "IN IF first";
                foreach ($users as $user) {
                    // echo "<br>user";
                    $user_id = $user->ID;
                    $usermeta = get_user_meta($user_id, '_sfwd-course_progress', true);

                    $usermeta = parent::wdmCheckAndUnserialize($usermeta);
                    if (!empty($courses[0])) {
                        foreach ($courses as $course) {
                            if (empty($course->post_title) || !sfwd_lms_has_access($course->ID, $user_id)) {
                                continue;
                            }

                            if (!isset($usermeta[$course->ID])) {
                                continue;
                            }
                            $cv = parent::wdmCheckCourseProgress($usermeta[$course->ID]);
                            $cours_completed_meta = get_user_meta($user_id, 'course_completed_'.$course->ID, true);
                            $cours_completed_date = parent::getCourseCompleteDate($cours_completed_meta);
                            $row = array('user_id' => $user_id, 'name' => $user->display_name, 'email' => $user->user_email, 'course_id' => $course->ID, 'course_title' => $course->post_title, 'total_steps' => $cv['total'], 'completed_steps' => $cv['completed'], 'course_completed' => parent::checkCompletedSteps($cv['total'], $cv['completed']), 'course_completed_on' => $cours_completed_date);
                            $tempI = 1;
                            if (!empty($cv['lessons'])) {
                                foreach ($cv['lessons'] as $lesson_id => $completed) {
                                    if (!empty($completed)) {
                                        if (empty($lessons[ $lesson_id ])) {
                                            $lesson = $lessons[ $lesson_id ] = get_post($lesson_id);
                                        } else {
                                            $lesson = $lessons[ $lesson_id ];
                                        }

                                        $row[ 'lesson_completed_'.$tempI ] = $lesson->post_title;
                                        ++$tempI;
                                    }
                                }
                            }

                            $course_progress_data[] = $row;
                        }
                    }
                }
                // echo "IN IF Last";
            } else {
                $course_progress_data[] = array('user_id' => $user_id, 'name' => $user->display_name, 'email' => $user->user_email, 'status' => __('No attempts', 'wdm_instructor_role'));
            }

            return $course_progress_data;
        }

        /**
         * [wdmLearndashCsvExport export functionality of to csv file].
         */
        public function wdmLearndashCsvExport()
        {
            if (empty($_REQUEST['post_id_report']) || empty($_POST['post_id_report'])) {
                return;
            }

            $content = self::wdmCourseProgressData($_POST['post_id_report']);

            $file_name = sanitize_file_name(get_the_title($_POST['post_id_report']).'-'.date('Y-m-d')); // file name to export

            if (empty($content)) {
                $content[] = array('status' => __('No attempts', 'wdm_instructor_role'));
            }
            require_once INSTRUCTOR_ROLE_ABSPATH.'includes/parsecsv.lib.php';
            $csv = new \lmsParseCSVNS\LmsParseCSV();
            $csv->output(true, $file_name.'.csv', $content, array_keys(reset($content)));
            die();
        }
        /**
         * [myAdminFooterFunction display black div to display message on some event].
         *
         * @return [string] [HTML]
         */
        public function myAdminFooterFunction()
        {
            echo '<div id="blanket" style="display:none;"></div>';
        }
    }

    ChildInstructorReports::getInstance();

}
