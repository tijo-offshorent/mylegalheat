<?php

namespace wdmInstructorRoleReport

{
    /**
     * This class is used to show reports on dashboard part.
     */
    class InstructorReports
    {
        public function __construct()
        {
        }

        public function checkIsSetCurrentPost($current_post, $post_course_id)
        {
            if (empty($current_post) && isset($post_course_id)) {
                return $post_course_id;
            }

            return $current_post;
        }

        public function wdmPageCounter($wdm_page_counter, $wdm_pgination_select)
        {
            if (isset($wdm_pgination_select) && $wdm_pgination_select < 101) {
                return $wdm_pgination_select;
            }

            return $wdm_page_counter;
        }

        public function checkIsSet($course_progress_data, $course_id = null)
        {
            if (isset($course_progress_data)) {
                return $course_progress_data;
            } elseif ($course_id != null) {
                $total_steps = 0;
                $total_quizs = learndash_get_global_quiz_list($course_id);
                $total_lessons = learndash_get_lesson_list($course_id);
                if (!empty($total_quizs)) {
                    $total_steps = 1;
                }
                if (!empty($total_lessons)) {
                    $total_steps += count($total_lessons);
                }

                return $total_steps;
            }

            return 0;
        }
        public function checkCourseCompletedOn($course_progress_data)
        {
            if (isset($course_progress_data)) {
                return $course_progress_data;
            }

            return '-';
        }

        public function paginationSelectData($val, $wdm_page_counter)
        {
            if ($val == $wdm_page_counter) {
                echo 'selected';
            }
        }

        public function updateCourseStatus($arr_course_status, &$not_started_per, &$in_progress_per, &$completed_per, &$not_started_str, &$in_progress_str, &$completed_str)
        {
            if ($arr_course_status['total'] > 0) {
                $not_started_per = round(($arr_course_status['not_started'] / $arr_course_status['total']) * 100, 2);
                $in_progress_per = round(($arr_course_status['in_progress'] / $arr_course_status['total']) * 100, 2);
                $completed_per = 100 - $not_started_per - $in_progress_per;

                $not_started_str = '<div id="not_started">
                                <span class="color-code"></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                '.__('Not Started', 'wdm_instructor_role').': '.($not_started_per).'% ( '.$arr_course_status['not_started'].'/'.$arr_course_status['total'].' )
                                </div>';

                $in_progress_str = '<div id="in_progress">
                                <span class="color-code"></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                '.__('In Progress', 'wdm_instructor_role').': '.($in_progress_per).'% ( '.$arr_course_status['in_progress'].'/'.$arr_course_status['total'].' )
                                </div>';

                $completed_str = '<div id="completed">
                                <span class="color-code"></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                '.__('Completed', 'wdm_instructor_role').': '.($completed_per).'% ( '.$arr_course_status['completed'].'/'.$arr_course_status['total'].' )
                                </div>';
            }
        }
        /**
         * To create HTML for pagination.
         *
         * @param int $last_page_number last number of the pagination
         *
         * @return string $str string of pagination
         */
        public function wdmGetPagedHtml($last_page_number)
        {
            $str = '';
            if ($last_page_number <= 1) {
                return;
            }

            $str = '<div class="tablenav-pages">
                <span class="pagination-links">
                    <a class="first-page wdm-paged" id="wdm_first_page" title="Go to the first page" href="javascript:wdm_js_ajax_pagination(0);">«</a>
                    <a class="prev-page wdm-paged" id="wdm_prev_page" title="Go to the previous page" href="javascript:wdm_js_ajax_pagination(0);">‹</a>
                    <span class="paging-input"><span id="wdm_paged_start_num">1</span> of <span class="total-pages">'.$last_page_number.'</span></span>
                    <a class="next-page wdm-paged" id="wdm_next_page" title="Go to the next page" href="javascript:wdm_js_ajax_pagination(1);">›</a>
                    <a class="last-page wdm-paged" id="wdm_last_page" title="Go to the last page" href="javascript:wdm_js_ajax_pagination(\''.($last_page_number - 1).'\');">»</a>
                </span>
            </div>';

            return $str;
        }

        public function wdmCheckAndUnserialize($usermeta)
        {
            if (!empty($usermeta)) {
                return maybe_unserialize($usermeta);
            }

            return '';
        }

        public function getCourseCompleteDate($cours_completed_meta)
        {
            if (empty($cours_completed_meta)) {
                $cours_completed_date = '';
            } else {
                $cours_completed_date = date('F j, Y H:i:s', $cours_completed_meta);
            }

            return $cours_completed_date;
        }

        public function wdmCheckCourseProgress($detail)
        {
            if (!empty($detail)) {
                return $detail;
            }

            return array('completed' => '', 'total' => '');
        }

        /**
         * to get course progress of a user.
         *
         * @param int $user_id   user id of a user
         * @param int $course_id course id of a course
         *
         * @return array course progress data
         */
        public function wdmGetCourseProgressInPer($user_id, $course_id)
        {
            if (empty($user_id) || empty($course_id)) {
                return;
            }
            $percentage = 0;
            $cours_completed_date = '-';
            $user_meta = get_user_meta($user_id, '_sfwd-course_progress', true);
            if (!empty($user_meta)) {
                if (isset($user_meta[ $course_id ])) {
                    $percentage = floor(($user_meta[ $course_id ]['completed'] / $user_meta[ $course_id ]['total']) * 100);

                    $cours_completed_meta = get_user_meta($user_id, 'course_completed_'.$course_id, true);
                    $cours_completed_date = (!empty($cours_completed_meta)) ? date('F j, Y H:i:s', $cours_completed_meta) : '';
                }

                $course_arr = array(
                'total_steps' => $user_meta[ $course_id ]['total'],
                'completed_steps' => $user_meta[ $course_id ]['completed'],
                'percentage' => $percentage,
                'course_completed_on' => $cours_completed_date,
                );
            }

            return $course_arr;
        }

        public function wdmGetUsersInGetUserHtml($post_users, $users)
        {
            if (isset($post_users)) {
                return $post_users;
            }

            return $users;
        }

        public function checkCompletedSteps($cv_total, $cv_completed)
        {
            if (!empty($cv_total) && $cv_completed >= $cv_total) {
                return 'YES';
            }

            return 'NO';
        }
    }

}
