<?php

/*
 * Create a custom function to display export EDD product details according to applied filter.
 * @param int $instructor_id
 * @param string $start_date
 * @param string $end_date
 * @return nothing
 * @since 2.4.0
 */
function wdmExportEddCsvReport($instructor_id, $start_date, $end_date)
{
    $order_list_to_disp = getEddOrderDetailsBasedOnFilters($instructor_id, $start_date, $end_date);
    //if(!empty($order_list_to_disp))
    //{
        //display table of EDD payment/order details.
        wdmDisplayEddOrderDetailsForExport($order_list_to_disp, $instructor_id, $start_date, $end_date);
    //}
}

/*
 * function to get filtered orders comes under particular instructor.
 * @param int $instructor_id
 * @param string $start_date
 * @param string $end_date
 * @return array having all orders after apply different filters like start_date , end_date
 * @since 2.4.0
 */
function getEddOrderDetailsBasedOnFilters($instructor_id, $start_date, $end_date)
{
    if (!isset($instructor_id)) {
        return false;
    }
    $order_list_to_disp = array();
    $start_date_timestamp = '';
    $end_date_timestamp = '';
    if (!empty($start_date)) {
        $start_date_timestamp = strtotime($start_date);
    }
    if (!empty($end_date)) {
        $end_date_timestamp = strtotime($end_date);
    }

//  {
        $instruc_order_list = getOrdersComesUnderInstructor($instructor_id);
    if (!empty($instruc_order_list)) {
        foreach ($instruc_order_list as $order => $products) {
            $to_display_order = false;
            $order_date_time = new DateTime(getEddOrderCompletionDate($order));
            $order_date_format = $order_date_time->format('Y-m-d');
            $order_date_timestamp = strtotime($order_date_format);
            if (!empty($start_date_timestamp) && !empty($end_date_timestamp)) {
                //if order completion date is greater than start date filter but less than end-date filter.
                if (($order_date_timestamp >= $start_date_timestamp) && ($order_date_timestamp <= $end_date_timestamp)) {
                    $to_display_order = true;
                }
            } elseif (empty($start_date_timestamp) && !empty($end_date_timestamp)) {
                //if startdate filter set then check whether order completion date is greater than start date of filter.
                if ($order_date_timestamp <= $end_date_timestamp) {
                    $to_display_order = true;
                }
            } elseif (!empty($start_date_timestamp) && empty($end_date_timestamp)) {
                //if enddate filter set then order will display if order completion date is less than end date.
                if ($order_date_timestamp >= $start_date_timestamp) {
                    $to_display_order = true;
                }
            } else {
                $to_display_order = true;
            }

            if ($to_display_order) {
                $order_list_to_disp[$order] = $products;
            }
        }
    }
    //}
    return $order_list_to_disp;
}

/**
 * function to get all order details comes under particular instructor.
 *
 * @param int $instructor_id
 *
 * @return key=>value here $key will indicate to order/payment id where $value will be an array to indicate all product to that orders.
 *
 * @since 2.4.0
 */
function getOrdersComesUnderInstructor($instructor_id)
{
    if (!isset($instructor_id)) {
        return array();
    }
    $ordrs_of_i_prodcts = array();
    $args = array('post_type' => 'edd_payment', 'post_status' => 'publish', 'orderby' => 'ID', 'order' => 'ASC', 'posts_per_page' => -1);
    $payment_details = new WP_Query($args);
    //echo '<pre>';print_r($payment_details);echo '</pre>';
    if ($payment_details->have_posts()) {
        while ($payment_details->have_posts()) {
            $payment_details->the_post();
            //check whether payment/order status is complete or not.
            $is_order_completed = edd_is_payment_complete(get_the_ID());
            if ($is_order_completed) {
                $odr_asociated_prdcts = edd_get_payment_meta_downloads(get_the_ID());
                if (!empty($odr_asociated_prdcts)) {
                    $product_array = array();
                    foreach ($odr_asociated_prdcts as $product) {
                        if ($instructor_id == -1) {
                            $product_array[] = $product['id'];
                        } else {
                            $product_author_id = getEddProductAuthor($product['id']);
                            if ($product_author_id == $instructor_id) {
                                $product_array[] = $product['id'];
                            }
                        }
                    }
                    if (!empty($product_array)) {
                        $ordrs_of_i_prodcts[get_the_ID()] = $product_array;
                    }
                }
            }
        }
    }
    wp_reset_query();

    return $ordrs_of_i_prodcts;
}

/**
 * create a function to get order/payment completion days for particular order.
 *
 * @param int $payment_id
 *
 * @return string order completion date if complete otherwise false.
 *
 * @since 2.4.0
 */
function getEddOrderCompletionDate($payment_id)
{
    $payment_details = get_post($payment_id);

    return $payment_details->post_date;
}

/**
 * [Function to show page setting].
 *
 * @param [int]    $instructor_id [description]
 * @param [string] $start_date    [start date]
 * @param [string] $end_date      [end date]
 *
 * @return [html] [page setting]
 *
 * @since 2.4.0
 */
function enqueueJsFileAndCheckFile($instructor_id, $start_date, $end_date)
{
    //if woocommerce plugin isn't active then display table otherwise not.
    if (!is_plugin_active('woocommerce/woocommerce.php')) {
        wp_enqueue_script('wdm_footable_pagination', plugins_url('/js/footable.paginate.js', __FILE__), array('jquery'));
    }
    ?>
    <br/><br/><br/>
    <h3>Easy Digital Download Commission Details:</h3>

    Search:<input id="edd_filters" type="text">
        <select name="change-page-size" id="change-edd-page-size" >
            <option value="5"><?php echo __('5 per page', 'wdm_instructor_role');
    ?></option>
            <option value="10"><?php echo __('10 per page', 'wdm_instructor_role');
    ?></option>
            <option value="20"><?php echo __('20 per page', 'wdm_instructor_role');
    ?></option>
            <option value="50"><?php echo __('50 per page', 'wdm_instructor_role');
    ?></option>
        </select>
    <br/>
    <?php
    if (file_exists(dirname(__FILE__).'/includes/parsecsv.lib.php')) {
        $url = admin_url('admin.php?page=instuctor&tab=export&wdm_export_edd_report=wdm_export_edd_report&wdm_instructor_id='.$instructor_id.'&edd_order_start_date='.$start_date.'&edd_order_end_date='.$end_date);
        ?>
    <a href="<?php echo $url;
        ?>" class="button-primary export_edd_order_details" id="edd_top_export_button" style="float:right"><?php echo __('Export CSV', 'wdm_instructor_role');
        ?></a> <?php
    }
}

/**
 * function to display EDD order details table in Export tab.
 *
 * @param array  $order_list
 * @param int    $instructor_id
 * @param string $start_date
 * @param string $end_date
 *
 * @since 2.4.0
 */
function wdmDisplayEddOrderDetailsForExport($order_list, $instructor_id, $start_date, $end_date)
{
    if (!isset($order_list) &&  !is_array($order_list)) {
        return;
    }
    enqueueJsFileAndCheckFile($instructor_id, $start_date, $end_date);
    //foreach($order_list as $order=>$products);

    ?>
    <br/><br/><br/>
    <div id="wdm_edd_order_in_export_tab">
    <table class="footable" data-filter="#edd_filters" data-page-navigation=".pagination" id="wdm_edd_report_tbl" data-page-size="5" >
            <thead>
                    <tr>
                        <th data-sort-initial="descending" data-class="expand">
                            <?php echo __('Payment ID', 'wdm_instructor_role');
    ?>
                        </th>
                        <th data-sort-initial="descending" data-class="expand">
                            <?php echo __('Instructor Name', 'wdm_instructor_role');
    ?>
                        </th>
                        <th data-sort-initial="descending" data-class="expand">
                            <?php echo __('Product / Course Name', 'wdm_instructor_role');
    ?>
                        </th>
                        <th>
                            <?php echo __('Actual Price', 'wdm_instructor_role');
    ?>
                        </th>
                        <th>
        <?php echo __('Commission Price', 'wdm_instructor_role');
    ?>
                        </th>


                    </tr>
                    <?php //do_action('wdm_commission_report_table_header', $instructor_id);
        ?>
                </thead>
                <tbody>
                    <?php

                    if (!empty($order_list)) {
                        foreach ($order_list as $payment => $product) {
                            foreach ($product as $p) {
                                ?>
                                <tr>
                                <td>
                                    <a href="<?php echo is_super_admin() ? site_url('wp-admin/edit.php?post_type=download&page=edd-payment-history&view=view-order-details&id='.$payment) : '#';
                                ?>" target="<?php echo is_super_admin() ? '_new_blank' : '';
                                ?>"><?php echo $payment;
                                ?></a>
                                </td>
                                <td>
                                <?php
                                $instructor = getEddProductAuthor($p);
                                $instructor_details = get_userdata($instructor);
                                echo $instructor_details->display_name;
                                ?> 
                                </td>
                                <td><a target="_new_blank"href="<?php echo site_url('wp-admin/post.php?post='.$p.'&action=edit');
                                ?>"><?php echo get_the_title($p);
                                ?> </a> </td>
                                <td>
                                    <?php
                                       $download = new EDD_Download($p);
                                    echo $download->get_price();
                                ?>
                                    </td>
                                    <td>
                                    <?php
                                    $order_commission = get_post_meta($payment, '_wdm_edd_instructor_commission', true);
                                    echo $order_commission;
                                ?>
                                    </td>
                                    </tr>

                                    <?php
                            }
                        }
                    } else {
                        ?>
                                <tr>
                                    <td><?php echo __('No record found!', 'wdm_instructor_role');
                        ?></td>
                                </tr>
                            <?php
                    }

    ?>

                </tbody>
                <tfoot >
                <tr>
                    <td colspan="5" style="border-radius: 0 0 6px 6px;">
                        <div class="pagination pagination-centered hide-if-no-paging"></div>
                    </td>
                </tr>
                </tfoot>

        </table>
    
    </div><br/><br/><?php
    if (file_exists(dirname(__FILE__).'/includes/parsecsv.lib.php')) {
        $url = admin_url('admin.php?page=instuctor&tab=export&wdm_export_edd_report=wdm_export_edd_report&wdm_instructor_id='.$instructor_id.'&edd_order_start_date='.$start_date.'&edd_order_end_date='.$end_date);
        ?>
    <a href="<?php echo $url;
        ?>" class="button-primary export_edd_order_details" id="edd_bottom_export_button" style="float:right"><?php echo __('Export CSV', 'wdm_instructor_role');
        ?></a>
    
    <?php
    }
}

/*
 * Extended functionality
 */
add_action('edd_complete_download_purchase', 'addCommissionToEddCourseInstructor', 10, 2);

/**
 * [function to add commission to the instructor after successfuly payment of product.].
 *
 * @param [int] $download_id [download_id]
 * @param [int] $payment_id  [payment_id]
 *
 * @return [nothing]
 *
 * @since 2.4.0
 */
function addCommissionToEddCourseInstructor($download_id, $payment_id)
{
    global $wpdb;
    //update_user_meta(1,'this_is_download_id',$download_id);
    //$user_id = edd_get_payment_user_id($payment_id);
    //get edd product details to get associated author to that product.
    $edd_product_meta = new EDD_Download($download_id);
    $auth_of_edd_product = $edd_product_meta->post_author;
    $edd_product_price = $edd_product_meta->get_price();
    $is_edd_type_course = get_post_meta($download_id, '_edd_learndash_is_course', true);
    if ($is_edd_type_course && !empty($edd_product_price)) {
        $is_instructor = wdm_is_instructor($auth_of_edd_product);
        if ($is_instructor) {
            $unpaid_earning = getUpdateUnpaidEarningsOfInstructor($auth_of_edd_product, $edd_product_price);
            if (!empty($unpaid_earning)) {
                update_user_meta($auth_of_edd_product, '_wdm_edd_unpaid_earning', $unpaid_earning);
                $commission_in_per = get_user_meta($auth_of_edd_product, 'wdm_commission_percentage', true);
                if (!empty($commission_in_per)) {
                    $added_comisin_price = ($edd_product_price * $commission_in_per) / 100;
//                  update_post_meta($payment_id,'_wdm_edd_instructor_commission',$added_comisin_price);
                     $data = array(
                                    'user_id' => $auth_of_edd_product,
                                    'order_id' => $payment_id,
                                    'product_id' => $download_id,
                                    'actual_price' => $edd_product_price,
                                    'commission_price' => $added_comisin_price,
                                    'product_type' => 'EDD',
                                  );
                     $wpdb->insert($wpdb->prefix.'wdm_instructor_commission', $data);
                }
            }
        }
    }
}

/**
 * function to get updated unpaid earning of instructor.
 *
 * @param int $instructor_id
 * @param int $edd_product_price
 *
 * @return updated unpaid earning for particular instructor otherwise false.
 *
 * @since 2.4.0
 */
function getUpdateUnpaidEarningsOfInstructor($instructor_id, $edd_product_price)
{
    if (!isset($instructor_id) || !isset($edd_product_price)) {
        return false;
    }
    $commission_in_per = get_user_meta($instructor_id, 'wdm_commission_percentage', true);
    if (!empty($commission_in_per)) {
        $ttl_upid_earni_of_in = '';
        $edd_unpaid_earning = get_user_meta($instructor_id, '_wdm_edd_unpaid_earning', true);
        if (!empty($edd_unpaid_earning)) {
            $ttl_upid_earni_of_in = $edd_unpaid_earning;
            $ttl_upid_earni_of_in += ($edd_product_price * $commission_in_per) / 100;
        } else {
            $ttl_upid_earni_of_in = ($edd_product_price * $commission_in_per) / 100;
        }
    }

    return $ttl_upid_earni_of_in;
}

/**
 * function to get author id of $edd_product_id.
 *
 * @param int $edd_product_id
 *
 * @return int id of the author/instructor id of passed product id.If error return false.
 *
 * @since 2.4.0
 */
function getEddProductAuthor($edd_product_id)
{
    //check whether passed product is of type download.
    if (!isset($edd_product_id) || get_post_type($edd_product_id) != 'download') {
        echo get_post_type($edd_product_id);

        return false;
    }
    $author_id = '';
    $edd_product_meta = get_post($edd_product_id);
    if (!empty($edd_product_meta)) {
        $author_id = $edd_product_meta->post_author;
    }

    return $author_id;
}

add_filter('wdmir_set_post_types', 'wdmAddEDDPostType');
/**
 * wdmAddEDDPostType adding edd post type.
 * @param  array $wdm_ar_post_types contains list of post type which instructor can access
 */
function wdmAddEDDPostType($wdm_ar_post_types)
{
    if (wdmCheckEDDDependency()) {
        array_push($wdm_ar_post_types, "download");
    }

    return $wdm_ar_post_types;
}

add_action('init', 'wdmEddSkipCommission');

/**
 * [wdmEddcapabilities for skipping commission on EED product]
 */
function wdmEddSkipCommission()
{
    $wdmir_admin_settings = get_option('_wdmir_admin_settings', array());
    if (isset($wdmir_admin_settings['instructor_commission']) && '1' == $wdmir_admin_settings['instructor_commission']) {
        remove_action('edd_complete_download_purchase', 'addCommissionToEddCourseInstructor', 10);
    }
}


add_filter('wdmir_add_dash_tabs', 'wdmAddEEDMenu');
if (!function_exists('wdmAddEEDMenu')) {
    /**
     * wdmAddEEDMenu to add
     * @param  array $allowed_tabs list of menus to be shown on dashboard
     */
    function wdmAddEEDMenu($allowed_tabs)
    {
        if (wdmCheckEDDDependency() && !in_array('edit.php?post_type=download', $allowed_tabs)) {
            array_push($allowed_tabs, "edit.php?post_type=download");
        } elseif (!wdmCheckEDDDependency()&& in_array('edit.php?post_type=download', $allowed_tabs)) {
            unset($allowed_tabs['edit.php?post_type=download']);
        }
        return $allowed_tabs;
    }
}

add_filter('wdm_product_type_array', 'wdmAddEEDPostTypeReport');
if (!function_exists('wdmAddEEDPostTypeReport')) {
    /**
     * wdmAddEEDPostTypeReport to add post type to show in reports
     * @param  array $product_type_array list of post type to show on report page
     */
    function wdmAddEEDPostTypeReport($product_type_array)
    {
        if (wdmCheckEDDDependency()) {
            $product_type_array['EDD']='_edd_payment_user_id';
        }

        return $product_type_array;
    }
}


add_action('admin_menu', 'wdmRemoveEddCustomerMenu');
/**
 * wdmRemoveEddCustomerMenu to remove customers sub-menu from dashboard
 */
function wdmRemoveEddCustomerMenu()
{
    if (wdm_is_instructor()) {
        remove_submenu_page('edit.php?post_type=download', 'edd-customers');
    }

}


add_filter('edd_report_views', 'wdmFilterEddReportView');
/**
 * Function to show only downloads option to instructor
 * @param  array $views list of options to show different reports
 * @return array $views list of options to show different reports
 */
function wdmFilterEddReportView($views)
{
    if (wdm_is_instructor()) {
        $views = array(
        'downloads'  => edd_get_label_plural(),
        );
    }
    return $views;
}

add_filter('wdmir_exclude_post_types', 'wdmExcludeEDDPostTypes');
/**
 * Function to exclude post types of EDD to show report
 * @param  array $wdmir_exclude_posts list of excluding post type
 * @return array $wdmir_exclude_posts list of excluding post type
 * @since 3.0.0
 */
function wdmExcludeEDDPostTypes($wdmir_exclude_posts)
{
    if (wdm_is_instructor()) {
        array_push($wdmir_exclude_posts, "edd_payment");
        array_push($wdmir_exclude_posts, "edd_log");
    }
    return $wdmir_exclude_posts;
}


add_action('admin_init', 'wdmEddCapabilities');

/**
 * wdmEddCapabilities giving capability to instructor for accessing report page of EDD.
 * @since 3.0.0
 */
function wdmEddCapabilities()
{
    if (wdm_is_instructor()) {
        if (!current_user_can("view_shop_reports")) {
            $wdm_instructor = get_role(ROLE_ID);
            $wdm_instructor->add_cap('view_shop_reports');
        }
        if (!current_user_can("export_shop_reports")) {
            $wdm_instructor = get_role(ROLE_ID);
            $wdm_instructor->add_cap('export_shop_reports');
        }
    }
}



add_action('admin_enqueue_scripts', 'wdmEDDAssetsFiles');
/**
 * wdmEDDAssetsFiles including css and js files
 */
function wdmEDDAssetsFiles()
{
    if (wdm_is_instructor() && isset($_GET['page']) && $_GET['page']=="edd-reports") {
        wp_enqueue_script('wdm_edd_report_js', plugin_dir_url(dirname(__FILE__)).'js/wdm_edd_report.js', array('jquery'));
        wp_enqueue_style('wdm_edd_report_css', plugin_dir_url(dirname(__FILE__)).'css/wdm_edd_report.css');
    }
}



add_action('wdm_ir_restrict_page', 'wdmRestrictLogTab', 10, 1);

/**
 * wdmRestrictLogTab to restrict instructor to access log tab
 * @param  String $current_scr_base base page
 * @since 3.0.0
 */
function wdmRestrictLogTab($current_scr_base)
{
    if (wdm_is_instructor() && $current_scr_base=="download_page_edd-reports") {
        if (isset($_GET['tab']) && $_GET['tab']=='logs') {
             wp_die(__('You do not have sufficient permissions to access this page...', 'wdm_instructor_role'));
        }
    }
}



/**
 * Fire a callback only when download posts are transitioned to 'publish'.
 *
 * @param string  $new_status New post status.
 * @param string  $old_status Old post status.
 * @param WP_Post $post       Post object.
 * @since 3.0.0
 */
function wdmLearndashEddMetaboxSave($new_status, $old_status, $post)
{
    if (( 'publish' == $new_status )
        && 'download' == $post->post_type && wdm_is_instructor()
    ) {
        if (isset($_POST['_edd_learndash_is_course']) && (isset($_POST['_edd_learndash_course']) && count($_POST['_edd_learndash_course'])>0)) {
            return;
        }
        global $wpdb;
        $wpdb->update($wpdb->posts, array('post_status' => 'draft'), array('ID' => $post->ID));
        clean_post_cache($post->ID);
    }
}
// add_action('transition_post_status', 'wdmLearndashEddMetaboxSave', 10, 3);


add_filter('post_updated_messages', 'wdmir_edd_message', 12, 1);

function wdmir_edd_message($message)
{

    if (!wdm_is_instructor(get_current_user_id())) {
        return $message;
    }
    global $post;

    $post_name = '';

    if (WDMIR_REVIEW_DOWNLOAD && $post->post_type == 'download') {
        $post_name = __('Download', 'wdm_instructor_role');

        $update_msg = __('This', 'wdm_instructor_role').' '
                      .$post_name.' '.__('will be reviewed and published by the admin upon approval.', 'wdm_instructor_role');

        $update_msg = apply_filters('wdmir_updated_message_download', $update_msg, $post->post_type, $message);

        $message['download'][1] = $update_msg; // saved
        $message['download'][4] = $update_msg; // updated
        $message['download'][6] = $update_msg; // published
        $message['download'][7] = $update_msg; // saved
        $message['download'][8] = $update_msg; // submitted
        $message['download'][10] = $update_msg; // saved in draft
    }
    return $message;

}


/**
 * @since 2.1
 * To validate user and to change default EDD product status accordingly.
 */
function wdmir_download_approval_validate($post_id, $post)
{
    if ((defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || ('auto-draft' == $post->post_status)
           || ('download' != $post->post_type) || (!wdm_is_instructor(get_current_user_id()))) {
        return;
    }

    // If Review Product setting is enabled. Then remove "publish product" capability of instructors.
    if (WDMIR_REVIEW_DOWNLOAD) {
        $status = 'draft';

        $product = array(
            'ID' => $post->ID,
            'post_status' => apply_filters('wdmtv_download_approval_status', $status),
        );

        //If calling wp_update_post, unhook this function so it doesn't loop infinitely
        remove_action('save_post', 'wdmir_download_approval_validate', 999, 2);

        wp_update_post($product);

        add_action('save_post', 'wdmir_download_approval_validate', 999, 2);

        // ------- to send an email - starts --------- //

        $send_mail = true;
        $send_mail = apply_filters('wdmir_dra_send_mail', $send_mail);

 //If you don't want to send en email on every update,you can apply your own logic and send an email whenever necessary.
        if ($send_mail) {
            $email_settings = get_option('_wdmir_email_settings');

            $pra_emails = isset($email_settings['dra_emails']) ? explode(',', $email_settings['dra_emails']) : '';

            $pra_emails = apply_filters('wdmir_dra_emails', $pra_emails);

            // If any E-Mail ID is not set, then send to admin email
            if (empty($pra_emails)) {
                $pra_emails = get_option('admin_email');
            } else {
                if (is_array($pra_emails)) {
                    $pra_emails = array_filter($pra_emails);
                }

                $subject = conditionalOperator($email_settings['dra_subject']);
                $body = conditionalOperator($email_settings['dra_mail_content']);

                // replacing shortcodes
                $subject = wdmir_post_shortcodes($post_id, $subject, false);
                $subject = wdmir_user_shortcodes(get_current_user_id(), $subject);

                $body = wdmir_post_shortcodes($post_id, $body, false);
                $body = wdmir_user_shortcodes(get_current_user_id(), $body);

                add_filter('wp_mail_content_type', 'wdmir_html_mail');

                $subject = apply_filters('wdmir_dra_subject', $subject);
                $body = apply_filters('wdmir_dra_body', $body);

                wdmir_wp_mail($pra_emails, $subject, $body);

                remove_filter('wp_mail_content_type', 'wdmir_html_mail');
            }
        } // if( $send_mail )

        // ------- to send an email - ends --------- //
    }
}
add_action('save_post', 'wdmir_download_approval_validate', 999, 2);

/**
 * @since 2.1
 * To Hide and show "Publish" button to instructor to products.
 */
function wdmir_hide_publish_edd_product()
{
    global $post;

    if (!isset($post->post_type) || 'download' != $post->post_type) {
        return;
    }

    if (!wdm_is_instructor(get_current_user_id())) {
        return;
    }

    // If Review Product setting is enabled.
    if (WDMIR_REVIEW_DOWNLOAD) {
        if ($post->post_status == 'publish') {
            echo '<script>';
            echo 'jQuery("#publishing-action #publish").attr("value","'.__('Save Draft', 'wdm_instructor_role').'");';
            echo '</script>';
        } else {
            // To hide "Publish" button of publish meta box.
            echo '<style>';
            echo '#publishing-action {
                display: none;
            }';
            echo '</style>';
        }

        // To remove "Publish" option from dropdown of quick edit.
    }
}
add_action('admin_footer', 'wdmir_hide_publish_edd_product', 999);




/**
 * @since 3.0.0
 * To Send an email notification after a product has been published of an instructor.
 */
function wdmir_download_published_notification($PID, $post)
{
    // check if we should send notification or not.
    if (wdmir_download_published_notification_cond($post)) {
        return;
    }

    // ------- to send an email - starts --------- //

    $send_mail = true;
    $send_mail = apply_filters('wdmir_dri_send_mail', $send_mail);

 //If you don't want to send en email on every update,you can apply your own logic and send an email whenever necessary.
    if ($send_mail) {
        $post_id = $PID;

        $email_settings = get_option('_wdmir_email_settings');

        $pri_emails = get_the_author_meta('user_email', $post->post_author);

        $pri_emails = apply_filters('wdmir_dri_emails', $pri_emails);

        if (!empty($pri_emails)) {
            if (is_array($pri_emails)) {
                $pri_emails = array_filter($pri_emails);
            }

            $subject = isset($email_settings['dri_subject']) ?
            wp_unslash($email_settings['dri_subject']) : 'Download is Published by an Admin';
            $body = isset($email_settings['dri_mail_content']) ?
            wp_unslash($email_settings['dri_mail_content']) : 'Download is Published by an Admin';

            // replacing shortcodes
            $subject = wdmir_post_shortcodes($post_id, $subject, false);
            $subject = wdmir_post_shortcodes($parent_course_id, $subject, false);
            $subject = wdmir_user_shortcodes($post->post_author, $subject);

            $body = wdmir_post_shortcodes($post_id, $body, false);
            $body = wdmir_post_shortcodes($parent_course_id, $body, false);
            $body = wdmir_user_shortcodes($post->post_author, $body);

            add_filter('wp_mail_content_type', 'wdmir_html_mail');

            $subject = apply_filters('wdmir_dri_subject', $subject);
            $body = apply_filters('wdmir_dri_body', $body);

            wdmir_wp_mail($pri_emails, $subject, $body);

            remove_filter('wp_mail_content_type', 'wdmir_html_mail');
        }
    } // if( $send_mail )

    // ------- to send an email - ends --------- //
}
add_action('publish_download', 'wdmir_download_published_notification', 10, 2);


/**
 * @since 3.0.0
 * To check if we should send notification or not after a product has been published of an instructor.
 */
function wdmir_download_published_notification_cond($post)
{
    if (empty($post)) {
        return true;
    }
    // If current post is NOT product OR product author is not an instructor OR current user is an Instructor
    if (($post->post_type != 'download') || (!wdm_is_instructor($post->post_author)) || (wdm_is_instructor(get_current_user_id()))) {
        return true;
    }

    return false;
}