<?php
/**
 * On woocommerce order complete, adding commission percentage in custom table.
 *
 * @param int $order_id order_id
 *
 * @since 2.4.0
 */
function wdm_add_record_to_db($order_id)
{
    $order = new WC_Order($order_id);
    global $wpdb;

    $items = $order->get_items();
    foreach ($items as $item) {
        $product_id = $item['product_id'];
        $total = $item['line_total'];
        $product_post = get_post($product_id);
        $author_id = $product_post->post_author;
        if (wdm_is_instructor($author_id)) {
            $commission_percent = get_user_meta($author_id, 'wdm_commission_percentage', true);
            if ('' == $commission_percent) {
                $commission_percent = 0;
            }
            $commission_price = ($total * $commission_percent) / 100;
            $sql = "SELECT id FROM {$wpdb->prefix}wdm_instructor_commission WHERE user_id = $author_id AND order_id = $order_id AND product_id = $product_id";
            $id = $wpdb->get_var($sql);
            $data = array(
                'user_id' => $author_id,
                'order_id' => $order_id,
                'product_id' => $product_id,
                'actual_price' => $total,
                'commission_price' => $commission_price,
                 'product_type' => 'WC',
            );
            if ('' == $id) {
                $wpdb->insert($wpdb->prefix.'wdm_instructor_commission', $data);
            } else {
                $wpdb->update($wpdb->prefix.'wdm_instructor_commission', $data, array('id' => $id));
            }
        }
    }
}

add_action('woocommerce_order_status_completed', 'wdm_add_record_to_db');

add_action('added_post_meta', 'wdm_instructor_updated_postmeta', 10, 4);

/**
 * Adding transaction details after LD transaction.
 *
 * @param int    $meta_id    meta id
 * @param int    $object_id  object_id
 * @param string $meta_key   meta key
 * @param string $meta_value meta value
 *
 *
 * @since 2.4.0
 */
function wdm_instructor_updated_postmeta($meta_id, $object_id, $meta_key, $meta_value)
{
    global $wpdb;
    $post_type = get_post_type($object_id);
    $meta_id = $meta_id;
    if ('sfwd-transactions' == $post_type && 'course_id' == $meta_key) {
        $course_id = $meta_value;
        $course_post = get_post($course_id);
        $author_id = $course_post->post_author;
        if (wdm_is_instructor($author_id)) {
            $commission_percent = get_user_meta($author_id, 'wdm_commission_percentage', true);
            if ('' == $commission_percent) {
                $commission_percent = 0;
            }
            $total = get_post_meta($object_id, 'payment_gross', true);
            if ('' == $total) {
                $total = 0;
            }
            $commission_price = ($total * $commission_percent) / 100;
            $data = array(
                'user_id' => $author_id,
                'order_id' => $object_id,
                'product_id' => $course_id,
                'actual_price' => $total,
                'commission_price' => $commission_price,
            );
            $wpdb->insert($wpdb->prefix.'wdm_instructor_commission', $data);
            //v2.4.0
            // update_post_meta($object_id, '_ldpurchaser_id', get_current_user_id());
        }
    }
}


add_filter('woocommerce_prevent_admin_access', 'wdmAllowDashboardAccess');

/**
 * To allow instructor to access dashboard
 * @param  boolean $prevent_access prevent_access
 * @return boolean $prevent_access prevent_access
 */
function wdmAllowDashboardAccess($prevent_access)
{

    if (wdm_is_instructor()) {
        return false;
    }
    return $prevent_access;

}


add_filter('wdmir_set_post_types', 'wdmAddWoocommercePostType');
/**
 * wdmAddWoocommercePostType adding woocommerce product post type.
 * @param  array $wdm_ar_post_types contains list of post type which instructor can access
 */
function wdmAddWoocommercePostType($wdm_ar_post_types)
{
    if (wdmCheckWooDependency() && !in_array('product', $wdm_ar_post_types)) {
        array_push($wdm_ar_post_types, "product");
    }

    return $wdm_ar_post_types;
}




add_filter('wdmir_add_dash_tabs', 'wdmAddWoocommerceMenu');
if (!function_exists('wdmAddWoocommerceMenu')) {
    /**
     * wdmAddWoocommerceMenu to add menu
     * @param  array $allowed_tabs list of menus to be shown on dashboard
     */
    function wdmAddWoocommerceMenu($allowed_tabs)
    {
        if (wdmCheckWooDependency() && !in_array('edit.php?post_type=product', $allowed_tabs)) {
            array_push($allowed_tabs, "edit.php?post_type=product");
        } elseif (!wdmCheckWooDependency()&& in_array('edit.php?post_type=product', $allowed_tabs)) {
            unset($allowed_tabs['edit.php?post_type=product']);
        }
        return $allowed_tabs;
    }
}
