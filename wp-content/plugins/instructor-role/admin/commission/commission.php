<?php

require_once 'commission-woocommerce.php';
require_once 'commission-edd.php';
add_action('admin_head', 'wdm_instructor_table_setup');

/**
 * Creating wdm_instructor_commission table.
 *
 * @since 2.4.0
 */
function wdm_instructor_table_setup()
{
    global $wpdb;
    $table_name = $wpdb->prefix.'wdm_instructor_commission';
    //if table doesn't exist then create a new table.
    if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
        $sql = 'CREATE TABLE '.$table_name.' (
        id INT NOT NULL AUTO_INCREMENT,
        user_id int,
        order_id int,
        product_id int,
        actual_price float,
        commission_price float,
        transaction_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        product_type varchar(5) DEFAULT NULL,
        PRIMARY KEY  (id)
                );';
        require_once ABSPATH.'wp-admin/includes/upgrade.php';
        dbDelta($sql);
    } else { //if table already exist then check that product_type coloumn exist or not.
        $fields = $wpdb->get_var("SHOW fields FROM {$table_name} LIKE 'product_type'");
        //if column 'product_type' isn't exist
        if ($fields != 'product_type') {
            $wpdb->query('ALTER TABLE '.$table_name.' ADD product_type VARCHAR(5) DEFAULT NULL');
        }
        wdmAddProductTypeToAlteredAttribute($table_name);
    }
}

/**
 * function to update product_type field if it's set to NULL.
 *
 * @param string $table_name table name
 *
 * @since 2.4.0
 */
function wdmAddProductTypeToAlteredAttribute($table_name)
{
    global $wpdb;
    $undef_product_type = $wpdb->get_results("SELECT * FROM $table_name  WHERE product_type IS NULL", ARRAY_A);
    if (!empty($undef_product_type)) {
        foreach ($undef_product_type as $row) {
            $to_add_product_type = '';
            $row_product_id = $row['product_id'];
            $row_unique_id = $row['id'];
            if (get_post_type($row_product_id) == 'product') {
                $to_add_product_type = 'WC';
            } elseif (get_post_type($row_product_id) == 'download') {
                $to_add_product_type = 'EDD';
            } elseif (get_post_type($row_product_id) == 'sfwd-courses') {
                $to_add_product_type = 'LD';
            }
            if (!empty($to_add_product_type)) {
                $wpdb->update($table_name, array('product_type' => $to_add_product_type), array('id' => $row_unique_id), array('%s'), array('%d'));
            }
        }
    }
}

add_action('wp_ajax_wdm_amount_paid_instructor', 'wdm_amount_paid_instructor');

/**
 * Update user meta of instructor for amount paid.
 *
 * @return json_encode status of operation
 *
 * @since 2.4.0
 */
function wdm_amount_paid_instructor()
{
    if (!is_super_admin()) {
        die();
    }
    $instructor_id = filter_input(INPUT_POST, 'instructor_id', FILTER_SANITIZE_NUMBER_INT);
    if (('' == $instructor_id) || (!wdm_is_instructor($instructor_id))) {
        echo json_encode(array('error' => __('Oops something went wrong', 'wdm_instructor_role')));
        die();
    }

    $total_paid = filter_input(INPUT_POST, 'total_paid', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    $amount_tobe_paid = filter_input(INPUT_POST, 'amount_tobe_paid', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    $enter_amount = filter_input(INPUT_POST, 'enter_amount', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    $usr_instructor_total = get_user_meta($instructor_id, 'wdm_total_amount_paid', true);

    $usr_instructor_total = getUsrInstructorTotal($usr_instructor_total);
    if (('' == $amount_tobe_paid || '' == $enter_amount) || $total_paid != $usr_instructor_total
        || $enter_amount > $amount_tobe_paid) {
        echo json_encode(array('error' => __('Oops something went wrong', 'wdm_instructor_role')));
        die();
    }

    global $wpdb;
    $sql = "SELECT commission_price FROM {$wpdb->prefix}wdm_instructor_commission WHERE user_id = $instructor_id";
    $results = $wpdb->get_col($sql);
    if (empty($results)) {
        echo json_encode(array('error' => __('Oops something went wrong', 'wdm_instructor_role')));
        die();
    } else {
        $vald_amnt_tobe_paid = 0;
        foreach ($results as $value) {
            $vald_amnt_tobe_paid += $value;
        }
        $vald_amnt_tobe_paid = round(($vald_amnt_tobe_paid - $total_paid), 2);
        if ($vald_amnt_tobe_paid != $amount_tobe_paid) {
            echo json_encode(array('error' => __('Oops something went wrong', 'wdm_instructor_role')));
            die();
        }
    }

    $new_paid_amount = round(($total_paid + $enter_amount), 2);
    update_user_meta($instructor_id, 'wdm_total_amount_paid', $new_paid_amount);

    /*
     * instructor_id is id of the instructor
     * enter_amount is amount entered by admin to pay
     * total_paid is the total amount paid by admin to insturctor before current transaction
     * amount_tobe_paid is the amount required to be paid by admin
     * new_paid_amount is the total amount paid to instructor after current transaction
     */
    do_action('wdm_commission_amount_paid', $instructor_id, $enter_amount, $total_paid, $amount_tobe_paid, $new_paid_amount);
    $new_amount_tobe_paid = round(($amount_tobe_paid - $enter_amount), 2);

    $data = array(
        'amount_tobe_paid' => $new_amount_tobe_paid,
        'total_paid' => $new_paid_amount,
    );
    echo json_encode(array('success' => $data));
    die();
}

/**
 * Function returns user instructor total.
 *
 * @param int $usr_instructor_total usr_instructor_total
 *
 * @return int usr_instructor_total
 *
 * @since 2.4.0
 */
function getUsrInstructorTotal($usr_instructor_total)
{
    if ('' == $usr_instructor_total) {
        return 0;
    }

    return $usr_instructor_total;
}
