<?php

add_filter('learndash_current_admin_tabs_on_page', 'wdm_remove_tabs_course', 10, 4);

/**
 * To remove "Lesson options" tab from admin lessons' page to instructor.
 */
function wdm_remove_tabs_course($current_page_id_data, $admin_tabs, $admin_tabs_on_page, $current_page_id)
{
    $admin_tabs = $admin_tabs;
    $admin_tabs_on_page = $admin_tabs_on_page;
    if (wdm_is_instructor()) {
        $admin_tabs = $admin_tabs;
        $admin_tabs_on_page = $admin_tabs_on_page;
        $course_pages = array('edit-sfwd-courses', 'sfwd-courses', 'admin_page_learndash-lms-course_shortcodes'); // lesson page IDs

        if (in_array($current_page_id, $course_pages)) { // if admin lessons page
            foreach ($current_page_id_data as $key => $value) {
                if (24 == $value) { // Categories tab
                    unset($current_page_id_data[ $key ]);
                } elseif (26 == $value) { // Tags tab
                    unset($current_page_id_data[ $key ]);
                } elseif (28 == $value) { // Course Shortcodes
                    unset($current_page_id_data[ $key ]);
                }
            }
        }
    }

    return $current_page_id_data;
}

/**
 * To load posts of current user (instructor) only in the backend.
 */
function wdm_load_my_courses($options)
{
    if (is_admin()) {
        $wdm_user_id = get_current_user_id();

        if (wdm_is_instructor($wdm_user_id)) {
            $options['author__in'] = $wdm_user_id;
        }
    }

    return $options;
}
add_filter('learndash_select_a_course', 'wdm_load_my_courses');



/**
 * This function will enroll the intructor to his course
 * @param  integer $post_id [course id]
 * @param  object $post    [post object]
 * @since 3.0.2
 */
function wdmEnrollAuthorToCourse($post_id, $post)
{
    if ((defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || ('auto-draft' == $post->post_status)) {
        return;
    }
    $user_id=get_current_user_id();
    if ($post->post_type!="sfwd-courses" || (!wdm_is_instructor($user_id))) {
        return;
    }
    ld_update_course_access($user_id, $post_id, $remove = false);
}
add_action('save_post', 'wdmEnrollAuthorToCourse', 11, 2);
