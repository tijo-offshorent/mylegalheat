<?php

/**
 *   @since version 2.1
 *   Display of HTML content on Instructor Settings page.
 *   This function is called from file "commission.php" in function instuctor_page_callback()
 */
function wdmir_instructor_settings()
{
    ?><div class="wrap">
    <h2><?php echo __('Instructor Settings', 'wdm_instructor_role') ?></h2>
    <form method="post" action="">
    <?php
    wp_nonce_field('instructor_setting_nonce_action', 'instructor_setting_nonce', true, true);
    do_action('wdmir_settings_before_table');
    ?>
        <table class="form-table wdmir-form-table">
            <tbody>
                <?php
                do_action('wdmir_settings_before');
                $wdmir_admin_settings = get_option('_wdmir_admin_settings', array());

                // Product Review
                $review_product = '';
                wdmSetSettingVariable($wdmir_admin_settings, 'review_product', $review_product);

                // Course Review
                $review_course = '';
                wdmSetSettingVariable($wdmir_admin_settings, 'review_course', $review_course);
             // Download Review
                $review_download = '';
                wdmSetSettingVariable($wdmir_admin_settings, 'review_download', $review_download);

                //instructor mail
                $wl8_en_inst_mail = '';
                wdmSetSettingVariable($wdmir_admin_settings, 'instructor_mail', $wl8_en_inst_mail);

                //added in 2.4.0 v instructor commission
                $wl8_en_inst_commi = '';
                wdmSetSettingVariable($wdmir_admin_settings, 'instructor_commission', $wl8_en_inst_commi);

                // if (isset($wdmir_admin_settings['instructor_commission']) && $wdmir_admin_settings['instructor_commission'] == '1') {
                //     $wl8_en_inst_commi = 'checked';
                // }
    //if EDD-LD integration plugin is deactivated then it will not show this setting v3.0.0
                if (wdmCheckEDDDependency()) {
                            ?>
                                <tr>
                                <th scope="row"><label for="wdmir_review_download"><?php echo __('Review Download', 'wdm_instructor_role');
                            ?></label></th>
                                <td><input name="wdmir_review_download" type="checkbox" id="wdmir_review_download" <?php echo $review_download;
                            ?>>
                                <?php echo __('Enable admin approval for EDD product updates.', 'wdm_instructor_role');
                            ?>
                                </td>
                            </tr>
                            <?php
                }
                //if woocommerce-ld integration plugin is deactivated then it will not show this setting v3.0.0
                if (wdmCheckWooDependency()) {
                            ?>
                                  <tr>
                             <th scope="row"><label for="wdmir_review_product"><?php echo __('Review Product', 'wdm_instructor_role');
                        ?></label></th>
                             <td><input name="wdmir_review_product" type="checkbox" id="wdmir_review_product" <?php echo $review_product;
                        ?>>
                                <?php echo __('Enable admin approval for WooCommerce product updates.', 'wdm_instructor_role');
                        ?>
                             </td>
                         </tr>
                            <?php
                } ?>
                <tr>
                    <th scope="row"><label for="wdmir_review_course"><?php echo __('Review Course', 'wdm_instructor_role');
    ?></label></th>
                    <td><input name="wdmir_review_course" type="checkbox" id="wdmir_review_course"<?php echo $review_course;
    ?>>
                    <?php echo __('Enable admin approval for LearnDash course updates.', 'wdm_instructor_role');
    ?>
                    </td>
                </tr>


                <tr>
                    <th scope="row"><label for="wdm_enable_instructor_mail"><?php echo __('Instructor Email', 'wdm_instructor_role');
    ?></label></th>
                    <td><input name="wdm_enable_instructor_mail" type="checkbox" id="wdm_enable_instructor_mail"<?php echo $wl8_en_inst_mail;
    ?>>
                    <?php echo __('Enable email notification for instructor on quiz completion.', 'wdm_instructor_role');
    ?>
                    </td>
                </tr>

                 <tr>
                    <th scope="row"><label for="wdm_enable_instructor_commission"><?php echo __('Instructor Commission', 'wdm_instructor_role');
    ?></label></th>
                    <td><input name="wdm_enable_instructor_commission" type="checkbox" id="wdm_enable_instructor_commission"<?php echo $wl8_en_inst_commi;
    ?>>
                    <?php echo __('Disable Instructor commission feature.', 'wdm_instructor_role');
    ?>
                    </td>
                </tr>

                <tr>
                    <th scope="row"><label for="wdmir_review_course_content"><?php echo __('Review Course Content', 'wdm_instructor_role');
    ?></label></th>
                    <td>
                    <?php
                        $editor_settings = array('textarea_rows' => 100, 'editor_height' => 200);
                    wp_editor(($wdmir_admin_settings['review_course_content'] ? $wdmir_admin_settings['review_course_content'] : ''), 'wdmir_review_course_content', $editor_settings);
    ?>
                    </td>
                </tr>

                <?php
                do_action('wdmir_settings_after');
    ?>
            </tbody>
        </table>
        <?php
            do_action('wdmir_settings_after_table');
    ?>
        <p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="<?php echo __('Save Changes', 'wdm_instructor_role');
    ?>"></p>
    </form>
</div>
<?php

} // function wdmir_instructor_settings()

function wdmSetSettingVariable($wdmir_admin_settings, $key, &$value)
{
    if (isset($wdmir_admin_settings[$key]) && '1' == $wdmir_admin_settings[$key]) {
        $value = 'checked';
    }
}

/*
*   @since version 2.1
*   Saving HTML form content of Instructor Settings page.
*
*/
add_action('admin_init', 'wdmir_settings_save');
function wdmir_settings_save()
{
    if (isset($_POST['instructor_setting_nonce']) && wp_verify_nonce($_POST['instructor_setting_nonce'], 'instructor_setting_nonce_action') && is_admin()) {
        $wdmir_admin_settings = array();

        do_action('wdmir_settings_save_before');

        // Product Review
        $wdmir_admin_settings['review_product'] = '';
        if (isset($_POST['wdmir_review_product'])) {
            $wdmir_admin_settings['review_product'] = 1;
        }

        // Course Review
        $wdmir_admin_settings['review_course'] = '';
        if (isset($_POST['wdmir_review_course'])) {
            $wdmir_admin_settings['review_course'] = 1;
        }
        // Download Review
        $wdmir_admin_settings['review_download'] = '';
        if (isset($_POST['wdmir_review_download'])) {
            $wdmir_admin_settings['review_download'] = 1;
        }

        //Enable instructor mail
        $wdmir_admin_settings['instructor_mail'] = '';
        if (isset($_POST['wdm_enable_instructor_mail'])) {
            $wdmir_admin_settings['instructor_mail'] = 1;
        }

        // Course Review
        $wdmir_admin_settings['review_course_content'] = '';
        if (isset($_POST['wdmir_review_course_content'])) {
            $wdmir_admin_settings['review_course_content'] = $_POST['wdmir_review_course_content'];
        }

        //instructor commission
        $wdmir_admin_settings['instructor_commission'] = '';
        if (isset($_POST['wdm_enable_instructor_commission'])) {
            $wdmir_admin_settings['instructor_commission'] = 1;
        }

        // Saving instructor settings option
        update_option('_wdmir_admin_settings', $wdmir_admin_settings);

        do_action('wdmir_settings_save_after');

        wp_redirect($_POST['_wp_http_referer']);
    }
}
