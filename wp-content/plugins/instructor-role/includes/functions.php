<?php
/**
 * to check user role is instructor or not.
 *
 * @param int $user_id wp user id, if user_id is null then it considers current logged in user_id
 *
 * @return bool if instructor true, else false
 */
if (!function_exists('wdm_is_instructor')) {
    function wdm_is_instructor($user_id = 0)
    {
        if (empty($user_id)) {
            $user_id = get_current_user_id();
        }
        //v2.4.1 added condition to check is the user is intructor or not
        $isInstructor=false;
        $user_info = get_userdata($user_id);

        if ($user_info) {
            if (in_array('wdm_instructor', $user_info->roles)) {
                $isInstructor= true;
            }
        }

        return apply_filters('wdm_check_instructor', $isInstructor);

    }
}

/*
 * returns author id if post has author
 * @param int $post_id post id of post
 * @return int author_id author id of post
 */
if (!function_exists('wdm_get_author')) {
    function wdm_get_author($post_id = null)
    {
        if (empty($post_id)) {
            $post_id = get_the_ID();
        }
        if (empty($post_id)) {
            return;
        }

        $postdata = get_post($post_id);

        if (isset($postdata->post_author)) {
            return $postdata->post_author;
        }

        return;
    }
}

/*
 * to search item in multidimentional array
 */
function wdm_in_array($needle, $haystack, $strict = false)
{
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && wdm_in_array($needle, $item, $strict))) {
            return true;
        }
    }

    return false;
}

/**
 * to remove template field from edit question and edit quiz pages.
 */
function wdm_remove_template_field()
{
    if (wdm_is_instructor()) {
        echo '<style>
		input[name=templateName], select[name=templateSaveList], #wpProQuiz_saveTemplate {
			display:none !important;
		}
		select[name=templateLoadId], input[name=templateLoad] {
			display:none !important;
		}
		</style>';
        echo '<script>
		jQuery( document ).ready( function() {
			jQuery( "#wpProQuiz_saveTemplate" ).closest( "div" ).remove();
			jQuery("select[name=templateLoadId]").closest( "div" ).remove();
		});
		</script>';
    }
}

/**
 * Custom Author meta box to display on a edit post page.
 */
function wdm_post_author_meta_box($post)
{
    global $user_ID;
    ?>
    <label class="screen-reader-text" for="post_author_override"><?php _e('Author');
    ?></label>
    <?php
    $wdm_args = array(
        'name' => 'post_author_override',
        'selected' => empty($post->ID) ? $user_ID : $post->post_author,
        'include_selected' => true,
    );
    $args = apply_filters('wdm_author_args', $wdm_args);
    wdm_wp_dropdown_users($args);
}

/**
 * To create HTML dropdown element of the users for given argument.
 */
function wdm_wp_dropdown_users($args = '')
{
    $defaults = array(
        'show_option_all' => '',
        'show_option_none' => '',
        'hide_if_only_one_author' => '',
        'orderby' => 'display_name',
        'order' => 'ASC',
        'include' => '',
        'exclude' => '',
        'multi' => 0,
        'show' => 'display_name',
        'echo' => 1,
        'selected' => 0,
        'name' => 'user',
        'class' => '',
        'id' => '',
        'include_selected' => false,
        'option_none_value' => -1,
    );

    $defaults['selected'] = wdmCheckAuthor(get_query_var('author'));

    $rvar = wp_parse_args($args, $defaults);
    $show = $rvar['show'];
    $show_option_all = $rvar['show_option_all'];
    $show_option_none = $rvar['show_option_none'];
    $option_none_value = $rvar['option_none_value'];

    $query_args = wp_array_slice_assoc($rvar, array('blog_id', 'include', 'exclude', 'orderby', 'order'));
    $query_args['fields'] = array('ID', 'user_login', $show);

    $users = array_merge(get_users(array('role' => 'administrator')), get_users(array('role' => ROLE_ID)), get_users(array('role' => 'author')));

    if (!empty($users) && (count($users) > 1)) {
        $name = esc_attr($rvar['name']);
        if ($rvar['multi'] && !$rvar['id']) {
            $idd = '';
        } else {
            $idd = wdmCheckAndGetId($rvar['id'], $name);
        }
        $output = "<select name='{$name}'{$idd} class='".$rvar['class']."'>\n";

        if ($show_option_all) {
            $output .= "\t<option value='0'>$show_option_all</option>\n";
        }

        if ($show_option_none) {
            $_selected = selected($option_none_value, $rvar['selected'], false);
            $output .= "\t<option value='".esc_attr($option_none_value)."'$_selected>$show_option_none</option>\n";
        }

        $found_selected = false;
        foreach ((array) $users as $user) {
            $user->ID = (int) $user->ID;
            $_selected = selected($user->ID, $rvar['selected'], false);
            if ($_selected) {
                $found_selected = true;
            }
            $display = wdmGetDisplayName($user->$show, $user->user_login);
            $output .= "\t<option value='$user->ID'$_selected>".esc_html($display)."</option>\n";
        }

        if ($rvar['include_selected'] && !$found_selected && ($rvar['selected'] > 0)) {
            $user = get_userdata($rvar['selected']);
            $_selected = selected($user->ID, $rvar['selected'], false);

            $display = wdmGetDisplayName($user->$show, $user->user_login);
            $output .= "\t<option value='$user->ID'$_selected>".esc_html($display)."</option>\n";
        }

        $output .= '</select>';
    }
    wdmPrintOutput($rvar['echo'], $output);

    return $output;
}

function wdmCheckAuthor($query_var_author)
{
    if (is_author()) {
        return $query_var_author;
    }

    return 0;
}
function wdmGetDisplayName($user_show, $user_login)
{
    if (!empty($user_show)) {
        return $user_show;
    }

    return '('.$user_login.')';
}
function wdmCheckAndGetId($rvar_id, $name)
{
    if ($rvar_id) {
        return " id='".esc_attr($rvar_id)."'";
    }

    return " id='$name'";
}
function wdmPrintOutput($rvar_echo, $output)
{
    if ($rvar_echo) {
        echo $output;
    }
}

/**
 * @since 2.1
 * Get LearnDash content's parent course.
 */
function wdmir_get_ld_parent($post_id)
{
    $post = get_post($post_id);

    if (empty($post)) {
        return;
    }

    $parent_course_id = 0;

    $post_type = $post->post_type;

    switch ($post_type) {
        case 'sfwd-certificates':
            // Get all quizzes
            $quizzes = get_posts(
                array(
                                'post_type' => 'sfwd-quiz',
                                'posts_per_page' => -1,
                            )
            );

            foreach ($quizzes as $quiz) {
                $sfwd_quiz = get_post_meta($quiz->ID, '_sfwd-quiz', true);

                if (isset($sfwd_quiz['sfwd-quiz_certificate']) && $sfwd_quiz['sfwd-quiz_certificate'] == $post_id) {
                    if (isset($sfwd_quiz['sfwd-quiz_certificate'])) {
                        $parent_course_id = $sfwd_quiz['sfwd-quiz_course'];
                    } else {
                        $parent_course_id = get_post_meta($quiz->ID, 'course_id');
                    }

                    break;
                }
            }

            break;

        case 'sfwd-lessons':
        case 'sfwd-quiz':
        case 'sfwd-topic':
            $parent_course_id = get_post_meta($post_id, 'course_id', true);
            break;

        case 'sfwd-courses':
            $parent_course_id = $post_id;
            break;

        default:
            $parent_course_id = apply_filters('wdmir_parent_post_id', $post_id);
            break;
    }

    return $parent_course_id;
}

/**
 * @since 2.1
 * Description: To check if post is pending approval.
 *
 * @param $post_id int post ID of a post
 *
 * @return array/false string/boolean array of data if post has pending approval.
 */
function wdmir_am_i_pending_post($post_id)
{
    if (empty($post_id)) {
        return false;
    }

    $parent_course_id = wdmir_get_ld_parent($post_id);

    if (empty($parent_course_id)) {
        return false;
    }

    $approval_data = wdmir_get_approval_meta($parent_course_id);

    if (isset($approval_data[ $post_id ]) &&  'pending' == $approval_data[ $post_id ]['status']) {
        return $approval_data[ $post_id ];
    }

    return false;
}

/**
 * @since 2.1
 * Description: To get approval meta of a course
 *
 * @param $course_id int post ID of a course
 *
 * @return array/false string/boolean array of data.
 */
function wdmir_get_approval_meta($course_id)
{
    $approval_data = get_post_meta($course_id, '_wdmir_approval', true);

    if (empty($approval_data)) {
        $approval_data = array();
    }

    return $approval_data;
}

/**
 * @since 2.1
 * Description: To set approval meta of a course
 *
 * @param $course_id int post ID of a course
 * @param $approval_data array approbval meta data of a course
 */
function wdmir_set_approval_meta($course_id, $approval_data)
{
    update_post_meta($course_id, '_wdmir_approval', $approval_data);
}

/**
 * @since 2.1
 * Description: To recheck and update course approval data.
 *
 * @param $course_id int post ID of a course
 *
 * @return $approval_data array updated new approval data.
 */
function wdmir_update_approval_data($course_id)
{
    $approval_data = wdmir_get_approval_meta($course_id);

    if (!empty($approval_data)) {
        foreach ($approval_data as $content_id => $content_meta) {
            $content_meta = $content_meta;
            $parent_course_id = wdmir_get_ld_parent($content_id);

            if ($parent_course_id != $course_id) {
                unset($approval_data[ $content_id ]);
            }
        }

        wdmir_set_approval_meta($course_id, $approval_data);
    }

    return $approval_data;
}

/**
 * @since 2.1
 * Description: To check if parent post's content has pending approval.
 *
 * @param $course_id int post ID of a course
 *
 * @return true/false boolean true if course has pending approval.
 */
function wdmir_is_parent_course_pending($course_id)
{
    $approval_data = wdmir_get_approval_meta($course_id);

    if (empty($approval_data)) {
        return false;
    }

    foreach ($approval_data as $content_meta) {
        // If pending content found.
        if ('pending' == $content_meta['status']) {
            return true;
        }
    }
}

/**
 * @since 2.1
 * Description: To send an email using wp_mail() function
 *
 * @return bool value of wp_mail function.
 */
function wdmir_wp_mail($touser, $subject, $message, $headers, $attachments)
{
    if (!empty($touser)) {
        return wp_mail($touser, $subject, $message, $headers, $attachments);
    }

    return false;
}

/**
 * @since 2.1
 * Description: To set mail content type to HTML
 *
 * @return string content format for mails.
 */
function wdmir_html_mail()
{
    return 'text/html';
}

/**
 * @since 2.1
 * Description: To replace shortcodes in the template for the post.
 *
 * @param $post_id int post ID of a post
 * @param $template string template to replace words
 *
 * @return $template string template by replacing words
 */
function wdmir_post_shortcodes($post_id, $template, $is_course_content = false)
{
    if (empty($template) || empty($post_id)) {
        return $template;
    }
    $post = get_post($post_id);

    if (empty($post)) {
        return $template;
    }

    $post_author_id = $post->post_author;

    $author_login_name = get_the_author_meta('user_login', $post_author_id);

    if ($is_course_content) {
        $find = array(
            '[course_content_title]',
            '[course_content_edit]',
            '[content_update_datetime]',
            '[approved_datetime]',
            '[content_permalink]',
        );

        $replace = array(
            $post->post_title, // [course_content_title]
            admin_url('post.php?post='.$post_id.'&action=edit'), // [course_content_edit]
            $post->post_modified, // [content_update_datetime]
            $post->post_modified, // [approved_datetime]
            get_permalink($post_id), // [content_permalink]
        );

        $replace = apply_filters('wdmir_content_template_filter', $replace, $find);
    } else {
        $find = array(
            '[post_id]',
            '[course_id]',
            '[product_id]',
            '[download_id]', //v3.0.0
            '[post_title]',
            '[course_title]',
            '[download_title]', //v3.0.0
            '[product_title]',
            '[post_author]',
            '[course_permalink]',
            '[product_permalink]',
            '[download_permalink]', //v3.0.0
            '[course_update_datetime]',
            '[product_update_datetime]',
            '[download_update_datetime]', //v3.0.0
            '[ins_profile_link]',
        );

        $replace = array(
            $post_id, // [post_id]
            $post_id, // [course_id]
            $post_id, // [product_id]
            $post_id, // [download_id]
            $post->post_title, // [post_title]
            $post->post_title, // [course_title]
            $post->post_title, // [download_title]
            $post->post_title, // [product_title]
            $author_login_name, // [post_author]
            get_permalink($post_id), // [post_permalink]
            get_permalink($post_id), // [product_permalink]
            get_permalink($post_id), // [download_permalink]
            $post->post_modified, // [course_update_datetime]
            $post->post_modified, // [product_update_datetime]
            $post->post_modified, // [download_update_datetime]
            //get_edit_user_link( $post_author_id ), // [ins_profile_link]
            admin_url('user-edit.php?user_id='.$post_author_id), // [ins_profile_link]
        );

        $replace = apply_filters('wdmir_course_template_filter', $replace, $find);
    }

    $template = str_replace($find, $replace, $template);

    $template = wdmir_user_shortcodes($post_author_id, $template);

    //echo $template;

    return $template;
}

/**
 * @since 2.1
 * Description: To replace shortcodes in the template for the User.
 *
 * @param $user_id int user ID.
 * @param $template string template to replace words
 *
 * @return $template string template by replacing words
 */
function wdmir_user_shortcodes($user_id, $template)
{
    if (empty($template) || empty($user_id)) {
        return $template;
    }

    $userdata = get_userdata($user_id);

    $find = array(
        '[ins_first_name]',
        '[ins_last_name]',
        '[ins_login]',
        '[ins_profile_link]',
    );

    $replace = array(
        $userdata->first_name, // [ins_first_name]
        $userdata->last_name, // [ins_last_name]
        $userdata->user_login, // [ins_login]
        //get_edit_user_link( $user_id ), // [ins_profile_link]
        admin_url('user-edit.php?user_id='.$user_id),  // [ins_profile_link]
    );

    $replace = apply_filters('wdmir_user_template_filter', $replace, $find);

    $template = str_replace($find, $replace, $template);

    return $template;
}


/**
 * For checking woocommerce dependency
 * @return boolean returns true if plugin is active
 */
function wdmCheckWooDependency()
{
    if (is_multisite()) {
        if (!function_exists('is_plugin_active_for_network')) {
            require_once ABSPATH.'/wp-admin/includes/plugin.php';
        }

        if (is_plugin_active_for_network('learndash_woocommerce/learndash_woocommerce.php')) {
            // in the network
                return true;
        } elseif (in_array('learndash_woocommerce/learndash_woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
            // in the subsite
            return true;
        }
        return false;
    } elseif (!in_array('learndash_woocommerce/learndash_woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
        return false;
    }
    return true;
}



/**
 * For checking EDD dependency
 * @return boolean returns true if plugin is active
 */
function wdmCheckEDDDependency()
{
    if (is_multisite()) {
        if (!function_exists('is_plugin_active_for_network')) {
            require_once ABSPATH.'/wp-admin/includes/plugin.php';
        }

        if (is_plugin_active_for_network('learndash-edd/learndash-edd.php')) {
            // in the network
                return true;
        } elseif (in_array('learndash-edd/learndash-edd.php', apply_filters('active_plugins', get_option('active_plugins')))) {
            // in the subsite
            return true;
        }
        return false;
    } elseif (!in_array('learndash-edd/learndash-edd.php', apply_filters('active_plugins', get_option('active_plugins')))) {
        return false;
    }
    return true;
}