<?php
/**
 *Plugin Name: Instructor Role
 *Plugin URI: https://wisdmlabs.com/
 *Description: This extension adds a user role 'Instructor' into your WordPress website and provides capabilities to create courses content and track student progress in your LearnDash LMS.
 *Version: 3.0.7
 *Author: WisdmLabs
 *Author URI: https://wisdmlabs.com/
 *Text Domain: wdm_instructor_role
 *Domain Path: /languages
 */

//Exit if accessed directly
if (!defined('ABSPATH')) {
    exit;
}

if (!defined('INSTRUCTOR_ROLE_ABSPATH')) {
    define('INSTRUCTOR_ROLE_ABSPATH', plugin_dir_path(__FILE__));
}

load_plugin_textdomain('wdm_instructor_role', false, dirname(plugin_basename(__FILE__)).'/languages/');


global $instructorRolePluginData;


/**
 * This has to be done on plugins_loaded or after that as it will need dependent plugin versions.
 */
add_action('plugins_loaded', 'instructorRoleLoadLicense');

/**
 * Change file paths if not accessed from main file
 */
function instructorRoleLoadLicense()
{
    global $instructorRolePluginData;
    $instructorRolePluginData = include_once('license.config.php');
    require_once 'licensing/class-wdm-license.php';
    new \Licensing\WdmLicense($instructorRolePluginData);
}


/**
 * To show warning message that, Instructor role plugin will work if LD plugin is activated. And deactivates self.
 */
function wdm_instructor_ld_dependency_check()
{

    // if multisite
    if (is_multisite()) {
        if (!function_exists('is_plugin_active_for_network')) {
            require_once ABSPATH.'/wp-admin/includes/plugin.php';
        }

        $is_plugin_active = false;

        if (is_plugin_active_for_network('sfwd-lms/sfwd_lms.php')) {
            // in the network
            $is_plugin_active = true;
        } elseif (in_array('sfwd-lms/sfwd_lms.php', apply_filters('active_plugins', get_option('active_plugins')))) {
            // in the subsite
            $is_plugin_active = true;
        }

        if (!$is_plugin_active) {
            echo "<div class='error'><p><b>".__('LearnDash LMS', 'wdm_instructor_role').'</b> '.__('plugin is not active. In order to make', 'wdm_instructor_role').' <b>'.__("'Instructor Role'", 'wdm_instructor_role').'</b> '.__('plugin work, you need to install and activate', 'wdm_instructor_role').' <b>'.__('LearnDash LMS', 'wdm_instructor_role').'</b> '.__('first', 'wdm_instructor_role').'.</p></div>';

            deactivate_plugins(plugin_basename(__FILE__));
            if (isset($_GET['activate'])) {
                unset($_GET['activate']);
            }
            return false;
        }
    } elseif (!in_array('sfwd-lms/sfwd_lms.php', apply_filters('active_plugins', get_option('active_plugins')))) {
        echo "<div class='error'><p><b>".__('LearnDash LMS', 'wdm_instructor_role').'</b> '.__('plugin is not active. In order to make', 'wdm_instructor_role').' <b>'.__("'Instructor Role'", 'wdm_instructor_role').'</b> '.__('plugin work, you need to install and activate', 'wdm_instructor_role').' <b>'.__('LearnDash LMS', 'wdm_instructor_role').'</b> '.__('first', 'wdm_instructor_role').'.</p></div>';

        deactivate_plugins(plugin_basename(__FILE__));
        if (isset($_GET['activate'])) {
            unset($_GET['activate']);
        }
        return false;
    }
    return true;
}

// to notify user to activate LearnDash LMS, if not activated
add_action('admin_notices', 'wdm_instructor_ld_dependency_check');

/**
 * To show warning message that, Instructor role plugin will work if LD plugin is activated. And deactivates self in network multisite.
 */
function wdm_instructor_ld_dependency_check_network()
{
    if (!function_exists('is_plugin_active_for_network')) {
        require_once ABSPATH.'/wp-admin/includes/plugin.php';
    }

    // Makes sure the plugin is defined before trying to use it
    if (!is_plugin_active_for_network('sfwd-lms/sfwd_lms.php')) {

        echo "<div class='error'><p><b>".__('LearnDash LMS', 'wdm_instructor_role').'</b> '.__('plugin is not active. In order to make', 'wdm_instructor_role').' <b>'.__("'Instructor Role'", 'wdm_instructor_role').'</b> '.__('plugin work, you need to install and activate', 'wdm_instructor_role').' <b>'.__('LearnDash LMS', 'wdm_instructor_role').'</b> '.__('first', 'wdm_instructor_role').'.</p></div>';
        deactivate_plugins(plugin_basename(__FILE__));
        if (isset($_GET['activate'])) {
            unset($_GET['activate']);
        }
    }
}
add_action('network_admin_notices', 'wdm_instructor_ld_dependency_check_network');

require_once 'includes/functions.php';
require_once 'includes/constants.php';
require_once 'admin/admin.php';
include_once 'widgets/wdm-instructor-widget.php'; // widgets of instructor
require_once 'instructor-wc/instructor-wc.php'; // handling WooCommerce part of instructor

//Added in v1.4 to add 'instructor_reports' cap to administrator. This cap is used to show course reports tab.
function wdmir_plugin_activate($network_wide)
{
    if (is_multisite() && $network_wide) {
        global $wpdb;
        foreach ($wpdb->get_col("SELECT blog_id FROM $wpdb->blogs") as $blog_id) {
            switch_to_blog($blog_id);
            $wdm_admin = get_role('administrator');
            $wdm_admin->add_cap('instructor_reports');
            $wdm_admin->add_cap('instructor_page');
           restore_current_blog();
        }
    } else {
        $wdm_admin = get_role('administrator');
        $wdm_admin->add_cap('instructor_reports');
        $wdm_admin->add_cap('instructor_page');
    }
}
register_activation_hook(__FILE__, 'wdmir_plugin_activate');

//Added in v1.4. To remove 'instructor_reports' cap to administrator. This cap is used to show course reports tab and all caps of instructor on deactivation.
function wdmir_plugin_deactivate()
{
    // $wdm_instructor = get_role(ROLE_ID);
    // echo "<pre>";
    // var_dump($wdm_instructor->capabilities);
    // die();
    if (function_exists("is_multisite") && is_multisite()) {
        // die('multisite');
        global $wpdb;

        foreach ($wpdb->get_col("SELECT blog_id FROM $wpdb->blogs") as $blog_id) {
            echo $blog_id;
            switch_to_blog($blog_id);

            $wdm_admin = get_role('administrator');
            $wdm_admin->remove_cap('instructor_reports');
            $wdm_admin->remove_cap('instructor_page');
            // Get the role object.
            $wdm_instructor = get_role(ROLE_ID);
            if ($wdm_instructor!=null) {
             // A list of capabilities to remove from Instructors.
                // $wdm_ins_caps = unserialize(WDM_INS_CAPS);

                foreach ($wdm_instructor->capabilities as $key_cap => $cap_value) {
                    $cap_value=$cap_value;
                    if ('read' != $key_cap) {
                        //var_dump("<br>".$key_cap.'<br>');
                        // Remove the capability.
                        $wdm_instructor->remove_cap($key_cap);
                    }
                }
            }
            restore_current_blog();
        }
        //die('<br>M');
    } else {
                // die('single');
        $wdm_admin = get_role('administrator');
        $wdm_admin->remove_cap('instructor_reports');
        $wdm_admin->remove_cap('instructor_page');
        // Get the role object.
        $wdm_instructor = get_role(ROLE_ID);
        if ($wdm_instructor!=null) {
            // A list of capabilities to remove from Instructors.
            //$wdm_ins_caps = unserialize(WDM_INS_CAPS);

            foreach ($wdm_instructor->capabilities as $key_cap => $cap_value) {
                    $cap_value=$cap_value;
                if ('read' != $key_cap) {
                    //var_dump("<br>".$key_cap.'<br>');
                    // Remove the capability.
                    $wdm_instructor->remove_cap($key_cap);
                }
            }
        }
    }

}
register_deactivation_hook(__FILE__, 'wdmir_plugin_deactivate');
