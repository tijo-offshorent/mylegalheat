=== Instructor Role Extension for LearnDash ===
Current Version: 3.0.7
Author:  WisdmLabs
Author URI: http://wisdmlabs.com/
Plugin URI: http://wisdmlabs.com/instructor-role-extension-for-learndash/
Tags: LearnDash Add-on, Instructor Role LearnDash, User Role LearnDash
Requires at least: 4.1.1
Tested up to: 4.7.3
License: GNU General Public License v2 or later

Tested with LearnDash version: 2.5.5
Tested with WooCommerce version: 3.3.1
Tested with EDD version: 2.8.18

== Description ==

The Instructor Role Extension for LearnDash adds a user role 'Instructor' into your WordPress website. An 'Instructor' has capabilities to create courses content and track student progress, thus behaving as a teacher, instructor, or guide for students enrolled in your LearnDash LMS.

== Installation Guide ==
1. Upon purchasing the Instructor Role Extension plugin, an email will be sent to the registered email id, with the download link for the plugin and a purchase receipt id. Download the plugin using the download link.
2. Go to Plugin-> Add New menu in your dashboard and click on the ‘Upload’ tab. Choose the ‘instructore-role.zip’ file to be uploaded and click on ‘Install Now’.
3. After the plugin has been installed successfully, click on the Activate Plugin link or activate the Instructor Role plugin from your Plugins page.
4. A Instructor Role License sub-menu will be created under Plugins menu in your dashboard. Click on this menu and enter your purchased product's license key. Click on Activate License. If license in valid, an 'Active' status message will be displayed, else 'Inactive' will be displayed.
5. Upon entering a valid license key, and activating the license, a new user role ‘Instructor’ will be added to your LearnDash website.

== User Guide ==
How do I create an Instructor?
To create an instructor, create a new user and set the user’s role as ‘Instructor’ and save. To change an existing user’s role to an instructor, edit the user’s profile and change the user’s role to ‘Instructor’ and save the changes made.

How can an Instructor view Course Reports?
An instructor can view reports for courses created by going to LearnDash LMS -> Course Reports. The report for a course is displayed using a pie chart, and details for each student enrolled for the course are displayed in a tabular format. The course report can be downloaded as a CSV using the 'Export Course Data' button.

== Features ==
1. Instructor has the capability to create and edit his/her own courses/lessons/topics/quizzes/certificates.
2. Instructor can approve assigments for his/her own courses.
3. Instructor can view and download course reports.


== Changelog ==

= 3.0.7 =
* Improved licensing code.

= 3.0.5 =
* Improved licensing code.
* Compatiblity with LearnDash v2.5.*
* Added BadgeOS support.
* Fixed menu issue on multi-language.


= 3.0.4 =
* Fixed a bug, was throwing an error for prior 2.4 LearnDash versions.
* Fixed a bug, resolved fatal error if LearnDash is deactivated.

= 3.0.3 =
* Fixed a bug, instructors were having access to import/export tab and could edit/delete quizzes and questions of other instructors/users.

= 3.0.2 =
* Added a feature to enroll instructor automatically into their course.
* Fixed a bug of "menu is not visible for instructor" for LearnDash LMS version >2.4.0.

= 3.0.1 =
* Compatible with LD v2.3.0.2

= 3.0.0 =
* Integrated with EDD v.2.6.6
* Now instructor's email ID will be used when an instructor is sending an email to a student.

= 2.4.1 =
* Fixed the issue of report and instructor page which was not accessible by Admin.

= 2.4.0 =
* Compatible with LD v2.2.1.1
* Compatible with WooCommerce v2.6.3
* Fixed issue of export data on Hebrew.
* Provided a feature to disable commission feature.
* Added Bio widget for administrator.
* Added hooks for developers.
* Fixed issue of lesson access which was not accessible by instructor.


= 2.0 =
* Commission Feature implemented for Instructor Role
* Fixed minor bugs

= 1.4 =
* Issue resolved for incorrect course reports for multilingual sites.
* Course reports tab available to the administrator.
* On some websites, Instructor was able to see content from other users. This issue is fixed now.

= 1.3 =
* Compatible with WooCommerce (2.3.11) for instructors to create courses as WooCommerce products
* Issue resolved for the appearence tab (for some themes) displayed on the instructor dashboard
* Issue with the reports display for multi lingual resolved
* Filters to add additional post types access

= 1.2 =
* Made plugin compatible with Multisite network
* Add media issue fixed

= 1.1 =
* Pagination added for course data

= 1.0 = 
* Plugin Released

= *0.9 =
* Beta Version Released.


== FAQ ==

For how long is a license valid?
Every license is valid for a year from the date of purchase. During this year you will receive free support. After the license expires you can renew the license for a discounted price.

What will happen if my license expires?
If your license expires, you will still be able to use the plugin, but you will not receive any support or updates. To continue receiving support for the plugin, you will need to purchase a new license key.

Is the license valid for more than one site?
Every purchased license is valid for one site. For multiple site, you will have to purchase additional license keys.

Help! I lost my license key!
In case you have misplaced your purchased product license key, kindly go back and retrieve your purchase receipt id from your mailbox. Use this receipt id to make a support request to retrieve your license key.

How do I contact you for support?
You can direct your support request to us, using the Support form on our website.

Do you have a refund policy?
Yes. we offer refunds requested within 30 days of purchase. But the refund will be granted only if the plugin does not work on your site and has integration issues, which we are unable to fix, even after support requests have been made.

Kindly refer to http://wisdmlabs.com/instructor-role-extension-for-learndash/ for additional details.

