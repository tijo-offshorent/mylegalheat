=== LearnDash LMS - Event Espresso ===

== Changelog ==
= 1.0 =
Initial release

= 1.0.1 =
* Changed processing logic to user EE action hook 'AHEE__EE_Single_Page_Checkout__process_attendee_information__end' for updating user to LD Courses. 
* Updated logic for wp_new_user_notification() to handle WP pre-4.3.0, 4.3.0, and 4.3.1 and newer.
* Updated activation logic to properly call dbDelta() to create needed database table.

= 1.0.2 =

* Added database upgrade function, change course_ids col to VARCHAR(1000)