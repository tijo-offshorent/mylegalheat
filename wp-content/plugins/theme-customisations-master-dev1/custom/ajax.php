<?php 
add_action('wp_ajax_course_add_to_cart','course_add_to_cart_Callback');
add_action('wp_ajax_nopriv_course_add_to_cart','course_add_to_cart_Callback');
function course_add_to_cart_Callback(){
	/*$data = $_POST;
	print_r($data);
	echo "dddd";*/
	$data = $_POST;
	global $woocommerce;
	$product_id = $data['product_id'];
	$quantity = $data['quantity'];
	$name = implode(',',$data['student_name']);
	$variation_id = $data['variation_id'];
	// Cart item data to send & save in order
	$cart_item_data = array('student_name' => $name);   
	// woocommerce function to add product into cart check its documentation also 
	// what we need here is only $product_id & $cart_item_data other can be default.
	//$woocommerce->cart->empty_cart(); 
	$woocommerce->cart->add_to_cart( $product_id, $quantity, $variation_id, $variation, $cart_item_data );
	// Calculate totals
	$woocommerce->cart->calculate_totals();
	// Save cart to session
	$woocommerce->cart->set_session();
	// Maybe set cart cookies
	$woocommerce->cart->maybe_set_cart_cookies();
	//print_r($_POST);
	$cart_url = $woocommerce->cart->get_cart_url();
	//echo '<a class="button view_cart_url" href="'.$cart_url.'">View Cart click here</a>';
	echo json_encode(array('url' => $cart_url));
	wp_die();
}
?>