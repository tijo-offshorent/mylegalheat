<?php 
function legal_search_widget_init() {
    register_widget( 'legal_search_widget' );
}
add_action( 'widgets_init', 'legal_search_widget_init' );
 
class legal_search_widget extends WP_Widget {
 
	function __construct() {
	parent::__construct( 'class_search_widget', __('Class Search Widget', 'wpb_widget_domain'), array( 'description' => __( 'Class Search based on State,City,Zipcode', 'legal_widget' ), ) );
	}
	 
	// Creating widget front-end
	 
	public function widget( $args, $instance ) {
		if(is_archive('sfwd-courses')){
			$term = 'partner';
		}else{
			$term = 'course';
		}
		$html .= '';
		$html .= $args['before_widget'];	 
		
		$html .= '<div class="class_search_widget">';
			$html .= '<form name="class_search_widget_form" id="class_search_widget_form" class="class_search_widget_form" action="" method="post">';
			$html .= '<input type="text" name="class_search_text" id="class_search_text_id" placeholder="Search by State, City, Zipcode" /><button type="submit" name="class_search_btn"><i class="icon-search"></i></button>';
			$html .= '<input type="hidden" name="serach_type" value="'.$term .'" />';
			if(is_archive('sfwd-courses')){
				$pterms = get_queried_object();
				$html .= '<input type="hidden" name="serach_partner" value="'.$pterms->slug .'" />';
			}
			$html .= '</form>';
		$html .= '</div>';

		$html .= $args['after_widget'];
		echo $html;
	}
	         

	public function form( $instance ) {
		
	}     
	public function update( $new_instance, $old_instance ) {
		
	}
}
?>