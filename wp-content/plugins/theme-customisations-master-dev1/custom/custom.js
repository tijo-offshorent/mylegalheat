jQuery(document).ready(function($){


	function preloader_mylegal_theme(data){
        if(data == 'show'){
            $('.mylegal_preloader_overlay,.mylegal_preloader').show();
        }else if(data == 'hide'){
            $('.mylegal_preloader_overlay,.mylegal_preloader').hide();
        }
    }  

	// Custom jQuery goes here

	if($('#my-account-content').length > 0){
		//var get_a_url = $('.view_student_course .ld-course-list-items.row h2.entry-title a').attr('href');
		//$('.view_student_course .ld-course-list-items.row .entry-content').append('<div class="button_div"><a class="button button-primary" href="'+get_a_url+'">Click to view Course Details</a></div>');
		$('.view_student_course .ld-course-list-items.row h2.entry-title a').each(function(){
			var get_a_url = $(this).attr('href');
			$(this).removeAttr('href');
			$(this).parent().append('<a class="button-primary" href="'+get_a_url+'">View Classes</a>');
		})
	}

	$(document).on('click','.course_cateory_tab_content div span a',function(){		
		$(this).parent().parent().find('ul').slideToggle('slow');
		$(this).toggleClass('active');

		if($(this).parent().find('i').hasClass('icon-down-chevron')){
			$(this).parent().find('i').removeClass('icon-down-chevron');
			$(this).parent().find('i').addClass('icon-right-chevron');
		}else{
			$(this).parent().find('i').removeClass('icon-right-chevron');
			$(this).parent().find('i').addClass('icon-down-chevron');
		}
		return false;
	});

	// $(document).on('click','.course_cateory_tab_content .class-list > a',function(){
	// 	$(this).parent().parent().find('ul').slideToggle('slow');
	// 	if($(this).parent().find('i').hasClass('icon-right-chevron')){
	// 		$(this).parent().find('i').removeClass('icon-right-chevron');
	// 		$(this).parent().find('i').addClass('icon-down-chevron');
	// 	}else{
	// 		$(this).parent().find('i').removeClass('icon-down-chevron');
	// 		$(this).parent().find('i').addClass('icon-right-chevron');
	// 	}
	// 	return false;
	// });

	$(document).on('click','.course_cateory_tab ul li a',function(){		
		var getid = $(this).attr('href');
		$('.course_cateory_tab li').removeClass('active');
		$(this).parent().addClass('active');
		$('.course_cateory_tab_content').hide();
		$('.tab_content_box '+getid).fadeIn('slow');

		if($('#map_hidden_data').length > 0){
			var getdatamap = $(this).attr('data_id');
			var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}	
			var GetMapData = $('#map_hidden_data').val();
			var decodedString = Base64.decode(GetMapData);
			var obj = JSON.parse(decodedString);
			var getMapObj = obj[getdatamap];
			/*if(getMapObj.length > 0){
				for(var i = 0; i < getMapObj.length; i++){
					//console.log(getMapObj[i]);
				}
			}*/
			//console.log(getMapObj);			
		}
		return false;
	});

	if($('.course_cateory_tab').length > 0){
		$('body').addClass('course_archive_page');
	}

	$('#course_product_button').click(function(){
		var student_name = Array();
		var fullnode = Array();;
		$('.student_name').each(function(){
			student_name.push($(this).val());
			if($(this).val() == ''){
				fullnode.push(1);
			}else{
				fullnode.push(0);
			}
		});
		if($('input[name="variation_id"]').val() == 0){
			alert('Please select the variation.');
		}else if($.inArray( 1,fullnode) !== -1){
			alert('Please enter student name.');
		}else{
			var datavar = {
				'action' : 'course_add_to_cart',
				'product_id' : $('input[name="product_id"]').val(),
				'variation_id' : $('input[name="variation_id"]').val(),
				'quantity' : $('.input-text.qty').val(),
				'student_name' : student_name
			}
			$.ajax({
				type:'POST',
				url:adminurl.ajax,
				data:datavar,
				dataType:'json',
				success:function(res){
					//console.log(res);
					window.location.href = res.url;
				}
			});
		}
		//var getdata = $('.variations_form.cart').serialize()+'&action=course_add_to_cart';
		/*var student_name = Array();
		$('.student_name').each(function(){
			student_name.push($(this).val());
		});
		var datavar = {
			'action' : 'course_add_to_cart',
			'product_id' : $('input[name="product_id"]').val(),
			'variation_id' : $('input[name="variation_id"]').val(),
			'quantity' : $('.input-text.qty').val(),
			'student_name' : student_name
		}
		$.ajax({
			type:'POST',
			url:adminurl.ajax,
			data:datavar,
			dataType:'json',
			success:function(res){
				//console.log(res);
				window.location.href = res.url;
			}
		});*/
		return false; 
	});

	$('.faq_post_list ul li h2.faq_accrd_title a').click(function(){
		var gethref = $(this).attr('href');
		$('.faq_post_list ul li div.faq_accrd_content').removeClass('active');
		$(gethref).slideToggle('slow').toggleClass('active');
		$('.faq_accrd_title').removeClass('active');
		$(gethref).parent().find('.faq_accrd_title').addClass('active');
		$('.faq_post_list ul li div.faq_accrd_content:not(".active")').slideUp('slow');


		if($(this).parent().find('i').hasClass('icon-right-chevron')){
			$('.faq_post_list ul li h2.faq_accrd_title i').removeClass('icon-down-chevron').addClass('icon-right-chevron');	
			$(this).parent().find('i').removeClass('icon-right-chevron').addClass('icon-down-chevron');
			
		}else{
			$('.faq_post_list ul li h2.faq_accrd_title i').removeClass('icon-down-chevron').addClass('icon-right-chevron');	
			$(this).parent().find('i').removeClass('icon-down-chevron').addClass('icon-right-chevron');
		}
	
		
		return false;
	});

	$(document).on('submit','.class_search_widget_form',function(){
		preloader_mylegal_theme('show');
		var searchform = $(this).serialize()+'&action=SearchCourseByText'
		$.ajax({
				type:'POST',
				url:adminurl.ajax,
				data:searchform,
				dataType:'html',
				success:function(res){
					$('.course_section').html(res);
					/*res.find('.course_cateory_tab_content').each(function(){
						var getul = $(this).find('ul li');
						console.log(getul);
					});*/
					$('.course_cateory_tab_content').each(function(){
						var getul = $(this).find('ul li').length;
						if(getul == 0){
							$(this).html('<p class="alert alert-danger">Your search does not match with available classes.</p>');
						}
					});
					preloader_mylegal_theme('hide');
					if($('#map_hidden_data').length > 0){
						var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}	
						var GetMapData = $('#map_hidden_data').val();
						var decodedString = Base64.decode(GetMapData);
						var obj = JSON.parse(decodedString);

						var infowindow =  new google.maps.InfoWindow({});
						var marker, count;
						for(var t = 0; t < obj.length; t++){							
							var getMapObj = obj[t].maploc;		
							var map = new google.maps.Map(document.getElementById('location_id_tab_'+obj[t].cat), {
								//center: {lat: -33.866, lng: 151.196},
								center: {lat: 40.773706, lng: -106.204173},
								zoom:2,
								//mapTypeId: 'terrain'
							});
							var features = []; 
							if(getMapObj.length > 0){
								for(var i = 0; i < getMapObj.length; i++){
									features.push({
										position: new google.maps.LatLng(getMapObj[i].lat, getMapObj[i].lng),
										type: 'info',
										address:getMapObj[i].address
									});
								}
							}							
							features.forEach(function(feature) {	
					          var marker = new google.maps.Marker({
					            position: feature.position,
					            //icon: 'https://maps.google.com/mapfiles/kml/shapes/library_maps.png',
					            map: map,
					            title:feature.address
					          });

					          google.maps.event.addListener(marker, 'click', (function (marker, count) {
								return function () {
									infowindow.setContent(feature.address);
									infowindow.open(map, marker);
								}
							  })(marker, count));	


					        });   
						}
					}
					/*if($('#map_hidden_data').length > 0){
						var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}	
						var GetMapData = $('#map_hidden_data').val();
						var decodedString = Base64.decode(GetMapData);
						var obj = JSON.parse(decodedString);
						var getMapObj = obj[0];	
						var map = new google.maps.Map(document.getElementById('location_id_tab0'), {
							center: {lat: -33.866, lng: 151.196},
							zoom: 15
						});
						var features = []; 
						if(getMapObj.length > 0){
							for(var i = 0; i < getMapObj.length; i++){
								features.push({
									position: new google.maps.LatLng(getMapObj[i].lat, getMapObj[i].lng),
									type: 'info'
								});
							}
						}

						features.forEach(function(feature) {
						  console.log()		
				          var marker = new google.maps.Marker({
				            position: feature.position,
				            icon: 'https://maps.google.com/mapfiles/kml/shapes/library_maps.png',
				            map: map
				          });
				        });								
					}*/
				}
		});
		
		return false;
	});




function new_map( $el ) {
	
	// var
	var $markers = $el.find('.marker');
	
	
	// vars
	var args = {
		zoom		: 16,
		center		: new google.maps.LatLng(0, 0),
		mapTypeId	: google.maps.MapTypeId.ROADMAP
	};
	
	
	// create map	        	
	var map = new google.maps.Map( $el[0], args);
	
	
	// add a markers reference
	map.markers = [];
	
	
	// add markers
	$markers.each(function(){
		
    	add_marker( $(this), map );
		
	});
	
	
	// center map
	center_map( map );
	
	
	// return
	return map;
	
}
function add_marker( $marker, map ) {

	// var
	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

	// create marker
	var marker = new google.maps.Marker({
		position	: latlng,
		map			: map
	});

	// add to array
	map.markers.push( marker );

	// if marker contains HTML, add it to an infoWindow
	if( $marker.html() )
	{
		// create info window
		var infowindow = new google.maps.InfoWindow({
			content		: $marker.html()
		});

		//infowindow.open( map, marker );
		// show info window when marker is clicked
		google.maps.event.addListener(marker, 'click', function() {

			infowindow.open( map, marker );

		});
	}

}

/*
*  center_map
*
*  This function will center the map, showing all markers attached to this map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	map (Google Map object)
*  @return	n/a
*/

function center_map( map ) {

	// vars
	var bounds = new google.maps.LatLngBounds();

	// loop through all markers and create bounds
	$.each( map.markers, function( i, marker ){

		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

		bounds.extend( latlng );

	});

	// only 1 marker?
	if( map.markers.length == 1 )
	{
		// set center of map
	    map.setCenter( bounds.getCenter() );
	    map.setZoom( 10);
	}
	else
	{
		// fit to bounds
		map.fitBounds( bounds );
	}

}

$('.acf-map').each(function(){

		// create map
		map = new_map( $(this) );

	});


 


});




/*function initMap() {
    var map = new google.maps.Map(document.getElementsByClassName('course_location_div'), {
      center: {lat: -33.866, lng: 151.196},
      zoom: 15
    });

    var infowindow = new google.maps.InfoWindow();
    var service = new google.maps.places.PlacesService(map);

    service.getDetails({
      placeId: 'ChIJN1t_tDeuEmsRUsoyG83frY4'
    }, function(place, status) {
      if (status === google.maps.places.PlacesServiceStatus.OK) {
        var marker = new google.maps.Marker({
          map: map,
          position: place.geometry.location
        });
        google.maps.event.addListener(marker, 'click', function() {
          infowindow.setContent('<div><strong>' + place.name + '</strong><br>' +
            'Place ID: ' + place.place_id + '<br>' +
            place.formatted_address + '</div>');
          infowindow.open(map, this);
        });
      }
    });
  }*/
