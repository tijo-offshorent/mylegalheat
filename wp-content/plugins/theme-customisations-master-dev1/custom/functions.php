<?php
/**
 * Functions.php
 *
 * @package  Theme_Customisations
 * @author   WooThemes
 * @since    1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

/**
 * functions.php
 * Add PHP snippets here
 */

add_action( 'woocommerce_register_form_start', 'mylegal_add_name_woo_account_registration' );
 
function mylegal_add_name_woo_account_registration() {
    ?>

    <p class="form-row form-row-wide user_type_reg_field">
        <label><input type="radio" class="input-text user_type" name="user_type" value="legal_blogger" />Blogger</label>
        <label><input type="radio" class="input-text user_type" name="user_type" value="legal_student" />Student</label>
    </p>
 
    <p class="form-row form-row-first">
    <label for="reg_billing_first_name"><?php _e( 'First name', 'woocommerce' ); ?> <span class="required">*</span></label>
    <input type="text" class="input-text" name="billing_first_name" id="reg_billing_first_name" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" />
    </p>
 
    <p class="form-row form-row-last">
    <label for="reg_billing_last_name"><?php _e( 'Last name', 'woocommerce' ); ?> <span class="required">*</span></label>
    <input type="text" class="input-text" name="billing_last_name" id="reg_billing_last_name" value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) esc_attr_e( $_POST['billing_last_name'] ); ?>" />
    </p>
 
    <div class="clear"></div>
    <?php
}
 
///////////////////////////////
// 2. VALIDATE FIELDS
 
add_filter( 'woocommerce_registration_errors', 'myleagal_validate_name_fields', 10, 3 );
 
function myleagal_validate_name_fields( $errors, $username, $email ) {
    if ( isset( $_POST['billing_first_name'] ) && empty( $_POST['billing_first_name'] ) ) {
        $errors->add( 'billing_first_name_error', __( 'First name is required!', 'woocommerce' ) );
    }
    if ( isset( $_POST['billing_last_name'] ) && empty( $_POST['billing_last_name'] ) ) {
        $errors->add( 'billing_last_name_error', __( 'Last name is required!.', 'woocommerce' ) );
    }
    if ( isset( $_POST['user_type'] ) && empty( $_POST['user_type'] ) ) {
        $errors->add( 'user_type', __( 'Please select user type!.', 'woocommerce' ) );
    }
    return $errors;
}
 
///////////////////////////////
// 3. SAVE FIELDS
 
add_action( 'woocommerce_created_customer', 'nylegal_save_name_fields' );
 
function nylegal_save_name_fields( $customer_id ) {
    if ( isset( $_POST['billing_first_name'] ) ) {
        update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
        update_user_meta( $customer_id, 'first_name', sanitize_text_field($_POST['billing_first_name']) );
    }
    if ( isset( $_POST['billing_last_name'] ) ) {
        update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
        update_user_meta( $customer_id, 'last_name', sanitize_text_field($_POST['billing_last_name']) );
    }
    if ( isset( $_POST['user_type'] ) ) {
        $u = new WP_User( $customer_id );
        $u->set_role( $_POST['user_type'] );
    }
 
}

function myleagal_woocommerce_registration_autologout(){
   if ( is_user_logged_in() ) {  
        session_start();      
        $current_user = wp_get_current_user();
        $user_id = $current_user->ID;
        $approved_status = get_user_meta($user_id, 'wp-approve-user', true);
        //if the user hasn't been approved yet by WP Approve User plugin, destroy the cookie to kill the session and log them out
        if ( $approved_status == 1 ){
            return $redirect_url;
        }else{
            wp_logout();
            $_SESSION['wp_user_approve_data'] = true;
            //return get_permalink(woocommerce_get_page_id('myaccount')) . "?approved=false";
            return get_permalink(woocommerce_get_page_id('myaccount'));
        }
    }
}
add_action('woocommerce_registration_redirect', 'myleagal_woocommerce_registration_autologout', 99);

function mylegal_registration_message(){
    session_start();
    if($_SESSION['wp_user_approve_data'] == true){
         echo '<p class="woocommerce-error"><strong>Registration successful!</strong> You will be notified upon approval of your account.</p>';
         unset ($_SESSION["wp_user_approve_data"]);
    }
}
add_action('woocommerce_before_customer_login_form', 'mylegal_registration_message', 99);

function storefront_myaccount_customer_avatar() {
     //$current_user = wp_get_current_user();
     echo do_shortcode('[avatar_upload]');
 }
 add_action( 'woocommerce_before_edit_account_form', 'storefront_myaccount_customer_avatar', 99 );

add_shortcode('view_post_blogger_data','view_post_blogger_dataCallback');
function view_post_blogger_dataCallback(){
    include('woo-menu-page-functions/functions.php');
    return Woo_Menu_Items_Shortcodes::view_user_post();
}

add_action( 'woocommerce_edit_account_form', 'legal_woocommerce_edit_account_form' );
function legal_woocommerce_edit_account_form(){
    $user_id = get_current_user_id();
    $user = get_userdata( $user_id );
    $description = get_user_meta( $user_id, 'description', true );
    ?>
    <fieldset>
    <p class="form-row form-row-thirds">
      <label for="Profile Description">Profile Description</label>
      <textarea name="description" class="input-text" /><?php echo $description; ?></textarea>
    </p>
  </fieldset>
    <?php
}
add_action( 'woocommerce_save_account_details', 'legal_woocommerce_save_account_details' );
function legal_woocommerce_save_account_details($user_id){
    update_user_meta( $user_id, 'description', htmlentities( $_POST[ 'description' ] ) );
}

add_shortcode('student_view_class','student_view_class_callback');
function student_view_class_callback(){
    include('woo-menu-page-functions/functions.php');
    return Woo_Menu_Items_Shortcodes::view_student_class();
}


add_shortcode('legal_ld_course_list','legal_ld_course_list_callback');
function legal_ld_course_list_callback(){
    include('woo-menu-page-functions/functions.php');
    return Woo_Menu_Items_Shortcodes::legal_ld_course_list();
}

add_shortcode('course_list_by_partner','legal_ld_course_list_by_partner_callback');
function legal_ld_course_list_by_partner_callback(){
    include('woo-menu-page-functions/functions.php');
    return Woo_Menu_Items_Shortcodes::legal_ld_course_list_by_partner();
}



/*add_filter('woocommerce_cart_item_name','add_custom_meta_cartitem_data',99,3);
function add_custom_meta_cartitem_data($product_name, $cart_item, $cart_item_key){
    return $product_name.'<br/><br/>Student Name:<b>'.$cart_item['student_name'].'</b>';
    //print_r($cart_item);
}*/

add_shortcode('legal_blog_post','legal_blog_post_callback');
function legal_blog_post_callback(){
    include('woo-menu-page-functions/functions.php');
    return Woo_Menu_Items_Shortcodes::legal_blog_post_list();
}

add_shortcode('legal_faq_post','legal_faq_post_callback');
function legal_faq_post_callback(){
    include('woo-menu-page-functions/functions.php');
    return Woo_Menu_Items_Shortcodes::legal_faq_post_list();
}

function legal_post_single_template($single_template) {
    global $post;
    if ($post->post_type == 'post') {
        $single_template = LEGAL_DIR . 'theme-template/single-post.php';
    }
    return $single_template;
}
add_filter( 'single_template', 'legal_post_single_template',99,1);

function legal_post_archive_template( $template ) {
    global $wp_query;
    $tax = $wp_query->get_queried_object();
     if( $tax->taxonomy == 'category'){
          $template = LEGAL_DIR . 'theme-template/archive-post-template.php';
     }     
     return $template;
}
add_filter( 'template_include', 'legal_post_archive_template',99,1 ) ;

function legal_move_comment_field_to_bottom( $fields ) {
    $comment_field  = $fields['comment'];
    $cookies        = $fields['cookies'];
    unset( $fields['comment'] );
    unset( $fields['cookies'] );
    $fields['comment'] = $comment_field;
    $fields['cookies'] = $cookies;
    return $fields;
} 
add_filter( 'comment_form_fields', 'legal_move_comment_field_to_bottom' );

add_action( 'woocommerce_review_order_before_submit', 'legal_add_checkout_custom_check_box', 9 );   
function legal_add_checkout_custom_check_box() {  
/*woocommerce_form_field( 'non_refundable_check', array(
    'type'          => 'checkbox',
    'class'         => array('form-row privacy'),
    'label_class'   => array('woocommerce-form__label woocommerce-form__label-for-checkbox checkbox'),
    'input_class'   => array('woocommerce-form__input woocommerce-form__input-checkbox input-checkbox'),
    'required'      => true,
    'label'         => 'I understand that all advance ticket purchases are completely Non Refundable',
));*/
$terms_link = home_url('/terms-and-conditions');
woocommerce_form_field( 'legal_terms_and_condition', array(
    'type'          => 'checkbox',
    'class'         => array('form-row terms'),
    'label_class'   => array('container_custom_checkbox'),
    'input_class'   => array('woocommerce-form__input woocommerce-form__input-checkbox input-checkbox'),
    'required'      => true,
    'label'         => '<span class="checkmark"></span> <b class="padd_left">I have read and agree to <a href="'.$terms_link.'">all the terms and conditions</a> for this course, and by signing up for the course agree to all of the terms and conditions and liability waivers for entering this course.</b>',
)); 
woocommerce_form_field( 'non_refundable_check', array(
    'type'          => 'checkbox',
    'class'         => array('form-row terms'),
    'label_class'   => array('container_custom_checkbox'),
    'input_class'   => array('woocommerce-form__input woocommerce-form__input-checkbox input-checkbox'),
    'required'      => true,
    'label'         => '<span class="checkmark"></span> <b class="padd_left"><span class="red">I also understand that class fees are NON-REFUNDABLE.</span></b>',
)); 
//echo '<p class="pull-left padd_left margin_toped"><span class="red">I also understand that class fees are NON-REFUNDABLE.</span></p>';
}  
   
add_action( 'woocommerce_checkout_process', 'legal_custom_checkbox_privacy' );  
function legal_custom_checkbox_privacy() {
    if ( ! (int) isset( $_POST['non_refundable_check'] ) ) {
        wc_add_notice( __( '<strong>Please acknowledge the Refundable Policy</strong>' ), 'error' );
    }
    if ( ! (int) isset( $_POST['legal_terms_and_condition'] ) ) {
        wc_add_notice( __( '<strong>Please accept the terms &amp; condition</strong>' ), 'error' );
    }    
    if ( isset( $_POST['shipping_first_name'] ) && $_POST['shipping_first_name'] == '' ) {
        wc_add_notice( __( '<strong>Shipping First name</strong> is a required field.' ), 'error' );
    }
    if ( isset( $_POST['shipping_last_name'] ) && $_POST['shipping_last_name'] == '' ) {
        wc_add_notice( __( '<strong>Shipping Last name</strong> is a required field.' ), 'error' );
    }
    if ( isset( $_POST['shipping_company'] ) && $_POST['shipping_company'] == '' ) {
        wc_add_notice( __( '<strong>Shipping Company</strong> is a required field.' ), 'error' );
    }
    if ( isset( $_POST['shipping_email'] ) && $_POST['shipping_email'] == '' ) {
        wc_add_notice( __( '<strong>Shipping Email</strong> is a required field.' ), 'error' );
    }
    if ( isset( $_POST['shipping_address_1'] ) && $_POST['shipping_address_1'] == '' ) {
        wc_add_notice( __( '<strong>Shipping Address (Line 1)</strong> is a required field.' ), 'error' );
    }
    if ( isset( $_POST['shipping_city'] ) && $_POST['shipping_city'] == '' ) {
        wc_add_notice( __( '<strong>Shipping City</strong> is a required field.' ), 'error' );
    }
    if ( isset( $_POST['shipping_postcode'] ) && $_POST['shipping_postcode'] == '' ) {
        wc_add_notice( __( '<strong>Shipping Zip Code</strong> is a required field.' ), 'error' );
    }
}


add_shortcode('legal_partner_logo_list','partner_logo_list_callback');
function partner_logo_list_callback(){
      
    $partners = get_terms('partner',array('hide_empty' => 1));
    
    $htmlout = '';
    $htmlout .= '<div class="partner_list">';
            $htmlout .= '<ul>';

    foreach ($partners as $partner) {
            # code...
            $partner->term_id;
            $term_link = get_term_link( $partner );
            $partner_logo_url=get_term_meta($partner->term_id,'wpcf-partner-image',true);
            $htmlout .= '<li><a href=" '.$term_link.' " alt=" '.$partner->name.' ">
            <img width="200" height="50" src=" '.$partner_logo_url.' " class="partner-logo"/></a></li>';
        
    }
    $htmlout .= '</ul>';
    $htmlout .= '</div>';

    return $htmlout;
}

add_action( 'save_post', 'save_course_store_meta', 10, 3 );


function save_course_store_meta($post_id, $post, $update){

        $post_type = get_post_type($post_id);

        if($post_type == 'sfwd-courses' || $post_type == 'product'):
            $fields= $_POST['fields'];
            //print_r($fields);
            if ( $post_type !=('sfwd-courses'||'product') ) return;

           // echo $fields['field_5b83df151dd19'];
            //exit;
            if($fields['field_5b83df151dd19']!='null'){
            remove_action( 'save_post', 'save_course_store_meta', 10, 3 );   
            $user_id=$fields['field_5b83df151dd19'];
            $arg = array(
            'ID' => $post_id,
            'post_author' => $user_id,
            );
            wp_update_post( $arg ); 
            add_action( 'save_post', 'save_course_store_meta', 10, 3 );
           }
           else{
                remove_action( 'save_post', 'save_course_store_meta', 10, 3 );   
            $user_id=$fields['field_5b83df151dd19'];
            $arg = array(
            'ID' => $post_id,
            'post_author' => 2,
            );
            wp_update_post( $arg ); 
            add_action( 'save_post', 'save_course_store_meta', 10, 3 );
           }
        endif;
}

add_action( 'pre_get_posts', 'course_based_on_post_author' );

function course_based_on_post_author($query){    
    global $order_idArray;  
       if ( is_user_logged_in() ) {
            $user = wp_get_current_user();
            if ( in_array( 'legal_store', (array) $user->roles ) ) {
                if(isset($_REQUEST['post_type']) && $_REQUEST['post_type'] == 'sfwd-courses'):  
                    $query->set( 'author', $user->ID );
                endif;
                if(isset($_REQUEST['post_type']) && $_REQUEST['post_type'] == 'product'):  
                    $query->set( 'author', $user->ID );
                endif;
                if(isset($_REQUEST['post_type']) && $_REQUEST['post_type'] == 'shop_order'):
                    $query->set( 'post__in', $order_idArray);
                    //$query->set( 'posts_per_page', '2' );
                endif;
            }            
            //echo '<pre>'; print_r($order_idArray); echo '</pre>';
        }
        return $query; 

}

add_action( 'woocommerce_thankyou', 'product_author_to_order' );

function product_author_to_order($order_id) {

    $order = wc_get_order( $order_id );
    $items = $order->get_items();
    $post_author_ids = array();
    foreach ( $items as $item ) {
    $product_name = $item->get_name();
    $product_id = $item->get_product_id();
    //$product_variation_id = $item->get_variation_id();
    $post_author_ids[] = get_post_field( 'post_author', $product_id );
    }
   // $order->update_meta_data( '_author_ids_meta_key', $post_author_ids );

    wc_update_order_item_meta($order_id,'_author_ids_meta_key',$post_author_ids);
   // print_r($post_author_ids);
    //exit; 
    
}

add_action('admin_init','filter_shop_order_storemanager');
function filter_shop_order_storemanager(){    
    if(isset($_REQUEST['post_type']) && $_REQUEST['post_type'] == 'shop_order'): 
    global $order_idArray;
    $getcurrentuser = get_current_user_id();
    $OrderData = get_posts( array( 
        'numberposts'    => -1,
        'post_type' => 'shop_order',
        'post_status'    => array_keys( wc_get_order_statuses() ) 
    ));
    $order_idArray = array();
    foreach ( $OrderData as $OrderDt ) {
        $order_id = $OrderDt->ID;
        $order = wc_get_order( $order_id );
        foreach($order->get_items() as $item_id => $item_values){
            $product_id = $item_values['product_id'];
            $post_author_id = get_post_field( 'post_author', $product_id );
            if($post_author_id == $getcurrentuser):
               $order_idArray[] =  $order_id;
            endif;
        }
    }
    endif;
}

function wc_empty_cart_redirect_url() {
    return home_url('/classes/');
}
add_filter( 'woocommerce_return_to_shop_redirect', 'wc_empty_cart_redirect_url' );

add_action('admin_footer','remove_admin_menu_for_legal_store');
function remove_admin_menu_for_legal_store(){
    if ( is_user_logged_in() ) {
        $user = wp_get_current_user();
        if ( in_array( 'legal_store', (array) $user->roles ) ) {
            ?>
            <script type="text/javascript">
                jQuery(document).ready(function($){
                    $('#toplevel_page_woocommerce .wp-menu-name').html('Orders');
                    $('#toplevel_page_woocommerce ul').remove();
                });
            </script>
            <?php
        }
    }
}

add_action('init', 'CSR_Role_Creation');
function CSR_Role_Creation()
{
    global $wp_roles;
    $all_roles = $wp_roles->roles;
    $UserRole = array();
    foreach($all_roles as $all_role){
        $UserRole[] = $all_role['name'];
    }
    if(!in_array('Customer Service Representative',$UserRole)):
        if ( ! isset( $wp_roles ) )
            $wp_roles = new WP_Roles();

        $adm = $wp_roles->get_role('administrator');
        //Adding a 'new_role' with all admin caps
        $wp_roles->add_role('legal_csr', 'Customer Service Representative', $adm->capabilities);
    endif;
}

add_action('admin_footer','remove_admin_menu_for_legal_csr');
function remove_admin_menu_for_legal_csr(){
    if ( is_user_logged_in() ) {
        $user = wp_get_current_user();
        if ( in_array( 'legal_csr', (array) $user->roles ) ) {
            ?>
            <script type="text/javascript">
                jQuery(document).ready(function($){
                    $('#toplevel_page_woocommerce').remove();
                    $('#toplevel_page_xlplugins').remove();
                    $('#menu-dashboard a[href="update-core.php"]').parent().remove();
                    $('#menu-posts').remove();
                    $('#menu-media').remove();
                    $('#menu-comments').remove();
                    $('#menu-posts-faqs').remove();
                    $('#toplevel_page_wpcf7').remove();
                    $('#toplevel_page_wp-user-frontend').remove();
                    $('#menu-appearance').remove();
                    $('#toplevel_page_yit_plugin_panel').remove();
                    $('#menu-plugins').remove();
                    $('#menu-tools').remove();
                    $('#toplevel_page_vc-general').remove();
                    $('#toplevel_page_ai1wm_export').remove();
                    $('#menu-settings').remove();
                    $('#toplevel_page_edit-post_type-acf').remove();
                    $('#toplevel_page_uncanny-learnDash-toolkit').remove();
                    $('#toplevel_page_h5p').remove();
                    $('#toplevel_page_my-menu').remove();
                    $('#toplevel_page_mailchimp-woocommerce').remove();
                    $('#toplevel_page_toolset-dashboard').remove();
                    $('#toplevel_page_wp-user-avatar').remove();
                    $('#toplevel_page_wisdmlabs-licenses').remove();
                    $('a[href="admin.php?page=learndash_lms_settings"]').parent().remove();
                    $('a[href="admin.php?page=learndash_lms_addons"]').parent().remove();

                    if($('.notice-error span').text() == 'YITH WooCommerce Customize My Account Page'){
                        $('.notice-error').remove();
                    }
                });
            </script>
            <?php
        }
    }
}

function remove_dashboard_widgets_legal() {
    global $wp_meta_boxes; 
    if ( is_user_logged_in() ) {
        $user = wp_get_current_user();
        if ( in_array( 'legal_csr', (array) $user->roles ) ) {
           //echo '<pre>'; print_r($wp_meta_boxes['dashboard']); echo '</pre>';
            //exit;
           unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
           unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
           unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
           unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);

           remove_meta_box( 'woocommerce_dashboard_status', 'dashboard', 'normal');
           remove_meta_box( 'woocommerce_dashboard_recent_reviews', 'dashboard', 'normal');
           //remove_meta_box('wpdm_dashboard_widget', 'dashboard', 'normal');
        }
    }
}
 
add_action('wp_dashboard_setup', 'remove_dashboard_widgets_legal' );

function my_acf_google_map_api( $api ){    
    $api['key'] = 'AIzaSyAB3g6zs0xQoIDiA4EiNGZ7AHHEfkWzVco';    
    return $api;    
}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

function my_acf_init() {
    
    acf_update_setting('google_api_key', 'AIzaSyAB3g6zs0xQoIDiA4EiNGZ7AHHEfkWzVco');
}

add_action('acf/init', 'my_acf_init');

add_action('wp_ajax_SearchCourseByText','SearchCourseByTextCallback');
add_action('wp_ajax_nopriv_SearchCourseByText','SearchCourseByTextCallback');
function SearchCourseByTextCallback(){
    $type = $_POST['serach_type'];
    $search_text = $_POST['class_search_text'];
    include('woo-menu-page-functions/functions.php');
    if($type == 'course'):
        if($search_text == ''){
            $searchData = Woo_Menu_Items_Shortcodes::legal_ld_course_list();
        }else{
            $searchData = Woo_Menu_Items_Shortcodes::legal_ld_course_list_bysearch($search_text);
        }
    else:
        if($search_text == ''){
            $searchData = Woo_Menu_Items_Shortcodes::legal_ld_course_list_by_partner($partner);
        }else{
            $partner = $_POST['serach_partner'];
            $searchData = Woo_Menu_Items_Shortcodes::legal_ld_course_list_by_partner_bysearch($search_text,$partner);
        }
    endif;
    echo $searchData;
    //print_r($course_locations);
    wp_die();
}


function ee_event_sponsor_area() {

    ?>
    <div class="sponsordetails">
    <h3><span class="dashicons  dashicons-heart "></span>Event Sponsor</h3>

    <h4><img align="right" src="<?php the_field( "sponsor_logo") ?>" width="200" height="150"></h4>    
    <strong>Sponsor Name</strong><?php the_field( "sponsor_name" )?>
    </div>
   
    <?php 
}
//add_action( 'AHEE_event_details_after_the_content', 'ee_event_sponsor_area' );


function show_map_in_event_page(){
 


 $location = get_field('class_address');
 //print_r($location);


if( !empty($location) ):
?>
<style type="text/css">

.acf-map {
    width: 100%;
    height: 400px;
    border: #ccc solid 1px;
    margin: 50px 0;
}

/* fixes potential theme css conflict */
.acf-map img {
   max-width: inherit !important;
}

</style>
<div class="acf-map">
    <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">  
    <p class="address"><?php echo $location['address']; ?></p>  
</div>
</div>
<?php endif; ?>
<?php 
}

add_action( 'AHEE_event_details_after_the_content', 'show_map_in_event_page' );

/*
Plugin Name: EE Custom Messages Shortcode - Site Specific Plugin
Description: Add custom messages shortcodes for Event Espresso 4 here.
*/
/* Begin Adding Functions Below This Line; Do not include an opening PHP tag as this sample code already includes one! */
function register_new_tony_shortcodes( $shortcodes, EE_Shortcodes $lib ) {
  
    //Add a shortcode to be used with the EE Datetimes within messages
    if ( $lib instanceof EE_Datetime_Shortcodes ) {
        //Add your shortcode to the add as the key, the value should be a description of the shortcode.
        $shortcodes['[TONYS_DATETIME_SHORTCODE]'] = _('This is  Custom DATETIME Shortcode!');
    }
    //Add a shortcode to be used with the EE Event List within messages
    if ( $lib instanceof EE_Event_Shortcodes ) {
        //Add your shortcode to the add as the key, the value should be a description of the shortcode.
        $shortcodes['[EVENT_SPONSOR_SHORTCODE]'] = _('This is EVENT Sponsor Shortcode!');
    }
    //Return the shortcodes.
    return $shortcodes;
}
add_filter( 'FHEE__EE_Shortcodes__shortcodes', 'register_new_tony_shortcodes', 10, 2 );

function register_new_tony_shortcodes_parser( $parsed, $shortcode, $data, $extra_data, EE_Shortcodes $lib ) {
    
    //Check for the datetime shortcodes as that's were we added the custom shortcode above
    //also check that $data is the expected object (in this case an EE_Datetime)
    if ( $lib instanceof EE_Datetime_Shortcodes  && $data instanceof EE_Datetime ) {
        //Then check for our specific shortcode
        if ( $shortcode == '[TONYS_DATETIME_SHORTCODE]' ) {
            
            //Do whatever you need to do here and return the value for that specific datetime.
            return $data->ID();
        }
    }
    if ( $lib instanceof EE_Event_Shortcodes ) {
    //Then check for our specific shortcode
        if ( $shortcode == '[EVENT_SPONSOR_SHORTCODE]' ) {
        
            //First check we have an event object.
            $event = $data instanceof EE_Event ? $data : null;
            //if no event, then let's see if there is a reg_obj.  If there IS, then we'll try and grab the event from the reg_obj instead.
            if ( empty( $event ) ) {
                $aee = $data instanceof EE_Messages_Addressee ? $data : NULL;
                $aee = $extra_data instanceof EE_Messages_Addressee ? $extra_data : $aee;
                $event = $aee instanceof EE_Messages_Addressee && $aee->reg_obj instanceof EE_Registration ? $aee->reg_obj->event() : NULL;
            }
            
            //Check we do now actually have the event object.
            if ( !empty( $event ) ) {
                //Do whatever you need to do using the event object, which is now $event and return.
                //return $event->ID();
                $sponsor_logo= get_field( "sponsor_logo", $event->ID() );
                $sponsor_name= get_field( "sponsor_name", $event->ID() );

                //$get_price_custom = get_post_meta($event->ID(),'event_custom_price',true);
                //print_r($get_price_custom);
                $spshtml= '';
                $spshtml.='<div class="sponsor_name">'.$sponsor_name.' </div>'; 
                $spshtml.='<div class="sponsor_logo"><img src='.$sponsor_logo.' width="100%" height="auto" /> </div>'; 
                return $spshtml;
            }
        }
    }
    //If not within the correct section, or parsing the correct shortcode,
    //Return the currently parsed content.
    return $parsed;
}
add_filter( 'FHEE__EE_Shortcodes__parser_after', 'register_new_tony_shortcodes_parser', 10, 5 );

// Register Custom Taxonomy


function be_register_taxonomies() {
    $taxonomies = array(
        array(
            'slug'         => 'course-location',
            'single_name'  => 'Course Location',
            'plural_name'  => 'Course Locations',
            'post_type'    => array('sfwd-courses','espresso_events'),
            //'rewrite'      => array( 'slug' => 'department' ),
        ),
        array(
            'slug'         => 'ld_course_category',
            'single_name'  => 'Course category',
            'plural_name'  => 'Course categories',
            'post_type'    => array('sfwd-courses','espresso_events'),
            //'hierarchical' => false,
        ),
        array(
            'slug'         => 'partner',
            'single_name'  => 'Partner',
            'plural_name'  => 'Partners',
            'post_type'    => array('sfwd-courses','espresso_events'),
        ),
    );
    foreach( $taxonomies as $taxonomy ) {
        $labels = array(
            'name' => $taxonomy['plural_name'],
            'singular_name' => $taxonomy['single_name'],
            'search_items' =>  'Search ' . $taxonomy['plural_name'],
            'all_items' => 'All ' . $taxonomy['plural_name'],
            'parent_item' => 'Parent ' . $taxonomy['single_name'],
            'parent_item_colon' => 'Parent ' . $taxonomy['single_name'] . ':',
            'edit_item' => 'Edit ' . $taxonomy['single_name'],
            'update_item' => 'Update ' . $taxonomy['single_name'],
            'add_new_item' => 'Add New ' . $taxonomy['single_name'],
            'new_item_name' => 'New ' . $taxonomy['single_name'] . ' Name',
            'menu_name' => $taxonomy['plural_name']
        );
        
        $rewrite = isset( $taxonomy['rewrite'] ) ? $taxonomy['rewrite'] : array( 'slug' => $taxonomy['slug'] );
        $hierarchical = isset( $taxonomy['hierarchical'] ) ? $taxonomy['hierarchical'] : true;
    
        register_taxonomy( $taxonomy['slug'], $taxonomy['post_type'], array(
            'hierarchical' => $hierarchical,
            'labels' => $labels,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => $rewrite,
        ));
    }
    
}
add_action( 'init', 'be_register_taxonomies' );


add_shortcode( 'mobile_class_search', 'mobile_class_search_widget');
function mobile_class_search_widget(){
    if(is_archive('sfwd-courses')){
        $term = 'partner';
    }else{
        $term = 'course';
    }
    $html  = '';    
    $html .= '<div class="class_search_widget">';
    $html .= '<form name="class_search_widget_form" id="class_search_widget_form" class="class_search_widget_form" action="" method="post">';
    $html .= '<input type="text" name="class_search_text" id="class_search_text_id" placeholder="Search by State, City, Zipcode" /><button type="submit" name="class_search_btn"><i class="icon-search"></i></button>';
    $html .= '<input type="hidden" name="serach_type" value="'.$term .'" />';
    if(is_archive('sfwd-courses')){
        $pterms = get_queried_object();
        $html .= '<input type="hidden" name="serach_partner" value="'.$pterms->slug .'" />';
    }
    $html .= '</form>';
$html .= '</div>';
return $html;
}

add_action( 'AHEE__SPCO__before_registration_steps', 'legal_registration_form_text');

function legal_registration_form_text(){

    echo "<div class='student-info'>
Please enter student name exactly as it appears on student driver's license or government ID for each student attending the class.</div>";

}

add_action( 'wp_print_scripts', 'my_deregister_eevalidation', 100 );

function my_deregister_eevalidation() {
    wp_deregister_script( 'validation' );
}