<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package LegalHeat
 */

get_header();
?>
<div class="container">		
	<div class="row">
	<div id="primary" class="col-12 col-sm-8 content-area">
		<main id="main" class="site-main">

		<?php if ( have_posts() ) : ?>

				<!-- <header class="page-header">-->
				<?php
				//the_archive_title( '<h1 class="page-title">', '</h1>' );
				//the_archive_description( '<div class="archive-description">', '</div>' );
				?>
				<!-- </header> --> <!-- .page-header -->

			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				$postid = get_the_ID();
				$featured_img_url = get_the_post_thumbnail_url($postid,'full'); 

			?>
			<article id="post-<?php the_ID(); ?>" <?php post_class('blog-post-article'); ?>>
				<div class="blog_list_page">
					<div class="entry-content">
						<ul class="blog_lsit">
							<li class="list_post">
								<header class="entry-header">
									<?php
									if ( is_singular() ) :
										the_title( '<h1 class="entry-title">', '</h1>' );
									else :
										the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
									endif;

									$links = array(
										'facebook' => 'https://www.facebook.com/sharer/sharer.php?u=' . get_permalink(),
										'twitter'  => 'https://twitter.com/intent/tweet?text='. get_the_title() .'&url='. get_permalink() .'&via='. $twitter,
										'mail'     => 'mailto:?subject='. get_the_title() .'&body=Take a look at this link - ' . get_permalink(),
										'linkedin' => 'https://www.linkedin.com/shareArticle?mini=true&url='. get_permalink() .'&title='. get_the_title() .'&summary=' . get_the_excerpt(),
										'gplus'    => 'https://plus.google.com/share?url=' . get_permalink()
									);

									if ( 'post' === get_post_type() ) :
										?>
										<div class="entry-meta">
											<?php
											legal_heat_posted_on();
											legal_heat_posted_by();
											?>
										</div><!-- .entry-meta -->
									<?php endif; ?>
								</header><!-- .entry-header -->

								<?php if($featured_img_url): ?>
								<div class="post-thumbnail"><?php legal_heat_post_thumbnail(); ?></div>
								<?php else: ?>
								<div class="post-thumbnail"><img class="legal_image_post" alt="<?php echo get_the_title(); ?>" src="<?php echo esc_url(LEGAL_URL); ?>no-image.png" /></div>
								<?php endif; ?>

								<div class="entry-content">
									<?php
									the_excerpt();

									wp_link_pages( array(
										'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'legal-heat' ),
										'after'  => '</div>',
									) );
									?>
								</div><!-- .entry-content -->
								<?php 
									echo '<div class="post_footer clearfix">';
									echo '<div class="post_footer_left">'.get_comments_number().' Comments</div>';
									echo '<div class="post_footer_right">
									<nav class="share" role="menu" aria-label="Share Links">
									<li class="share__facebook">
									<a href="'.$links['facebook'].'" class="share__link"><i class="icon-facebook"></i><span class="is-hidden">Share</span></a>
									</li>
									<li class="share__twitter">
									<a href="'.$links['twitter'].'" class="share__link"><i class="icon-twitter"></i><span class="is-hidden">Tweet</span></a>
									</li>			
									</nav>
									</div>';
									echo '</div>';
								?>
							</li>
						</ul>
					</div>
				</div>	
			</article><!-- #post-<?php the_ID(); ?> -->
			<?php	

			endwhile;

			//the_posts_navigation();
			global $wp_query;

			$big = 999999999; // need an unlikely integer

			echo '<div class="pagination_legal pagination_legal_category">'.paginate_links( array(
			'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format' => '?paged=%#%',
			'current' => max( 1, get_query_var('paged') ),
			'total' => $wp_query->max_num_pages
			) ).'</div>';
		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</main><!-- #main -->
	</div><!-- #primary -->
	<?php get_sidebar(); ?>
	</div>
</div>
<?php
//get_sidebar();
get_footer();
