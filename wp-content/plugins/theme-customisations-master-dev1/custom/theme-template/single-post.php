<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package LegalHeat
 */

get_header();
?>
<div class="container">		
	<div class="row">
	<div id="primary" class="col-12 col-sm-8 content-area">
		<main id="main" class="site-main">

		<?php if ( have_posts() ) : ?>

				<!-- <header class="page-header">-->
				<?php
				//the_archive_title( '<h1 class="page-title">', '</h1>' );
				//the_archive_description( '<div class="archive-description">', '</div>' );
				?>
				<!-- </header> --> <!-- .page-header -->

			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

			?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="blog_list_page">
					<div class="entry-content">
						<ul class="blog_lsit">
							<li class="list_post">
								<header class="entry-header">
									<?php
									if ( is_singular() ) :
										the_title( '<h1 class="entry-title">', '</h1>' );
									else :
										the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
									endif;

									if ( 'post' === get_post_type() ) :
										?>
										<div class="entry-meta">
											<?php
											legal_heat_posted_on();
											legal_heat_posted_by();
											?>
										</div><!-- .entry-meta -->
									<?php endif; ?>
								</header><!-- .entry-header -->

								<?php legal_heat_post_thumbnail(); ?>

								<div class="entry-content">
									<?php
									the_content( sprintf(
										wp_kses(
											/* translators: %s: Name of current post. Only visible to screen readers */
											__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'legal-heat' ),
											array(
												'span' => array(
													'class' => array(),
												),
											)
										),
										get_the_title()
									) );

									wp_link_pages( array(
										'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'legal-heat' ),
										'after'  => '</div>',
									) );
									?>
								</div><!-- .entry-content -->

								<footer class="entry-footer">
									<?php legal_heat_entry_footer(); ?>
								</footer><!-- .entry-footer -->
							</li>
						</ul>
					</div>
				</div>	
			</article><!-- #post-<?php the_ID(); ?> -->
			<?php	

			endwhile;
		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		<div class="blog_single_comment">
		<?php 
		if ( comments_open() || get_comments_number() ) :
		    comments_template();
		endif;
		?>
		</div>	

		</main><!-- #main -->
	</div><!-- #primary -->
	<?php get_sidebar(); ?>
	</div>
</div>
<?php
//get_sidebar();
get_footer();
