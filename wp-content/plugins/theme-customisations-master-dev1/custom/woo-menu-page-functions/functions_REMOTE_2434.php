<?php 
class Woo_Menu_Items_Shortcodes{
	public function view_user_post(){
		global $wpdb;
		$current_user = wp_get_current_user();
		$userid = $current_user->ID;
		$drafts = get_posts(array('author' => $userid,'post_status' => 'draft'));
		$publish = get_posts(array('author' => $userid,'post_status' => 'publish'));

		$allPosts = get_posts(array('author' => $userid,'post_status' => array('publish','draft')));
		$approved_comments = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) AS total FROM $wpdb->comments WHERE comment_approved = 1 AND user_id = %s", $userid ) );

		$spamcount = 0;
		if($publish): foreach($publish as $post):
			$comments_count = wp_count_comments($post->ID);
			$spamcount += $comments_count->spam;
		endforeach; endif;
		$dhtml = '';
		$dhtml .= '<div class="view_post_blogger_point">';
			$dhtml .= '<h2>My Approvals</h2>';
			$dhtml .= '<ul class="view_post_blogger">';
				$dhtml .= '<li><span>'.count($publish).'</span> Approved Posts</li>';
				$dhtml .= '<li><span>'.count($drafts).'</span> Pending Posts</li>';
				$dhtml .= '<li><span>'.$approved_comments.'</span> Comments Approved</li>';
				$dhtml .= '<li><span>'.$spamcount.'</span> Spam Comments</li>';
			$dhtml .= '</ul>';
		$dhtml .= '</div>';

		$dhtml .= '<div class="show_posts_list_blogger">';
			$dhtml .= '<h2>View All Posts</h2>';
			$dhtml .= '<ul>';
			if($allPosts):
				foreach($allPosts as $allPost):
					//$edit_page = (int) wpuf_get_option( 'edit_page_id', 'wpuf_general' );
					$edit_page = (int) wpuf_get_option('edit_page_id','wpuf_frontend_posting');
					$url = get_permalink( $edit_page );
					$nonceedit = wp_nonce_url($url.'?pid='.$allPost->ID,'wpuf_edit');

					$del_url = home_url('/dashboard/').'?action=del&pid='.$allPost->ID;
					$message = "'".__( 'Are you sure to delete?', 'wp-user-frontend' )."'";
					$noncedelete = wp_nonce_url($del_url, 'wpuf_del' );

					$dhtml .= '<li><span class="post_list_title">'.get_the_title($allPost->ID).'</span><span class="post_list_status '.$allPost->post_status.'">'.$allPost->post_status.' &nbsp; <a href="'.$nonceedit.'"><i class="fa fa-edit"></i></a> | <a href="'.$noncedelete.'" onclick="return confirm('.$message.');"><i class="fa fa-trash"></i></a></span>						
					</li>';
				endforeach;
			else:
				$dhtml .= '<li>Sorry No posts</li>';
			endif;
			$dhtml .= '</ul>';
		$dhtml .= '</div>';
		return $dhtml;
	}

	public function view_student_class(){
		$html = '';
		$html .= '<div class="view_student_course">';
			//$html .= '<h2 class="title">View Courses</h2>';
			$html .= do_shortcode('[ld_course_list]');
		$html .= '</div>';
		return $html;
	}

	public function legal_ld_course_list(){
		$htmldoc = '';
		$category_courses = get_terms('ld_course_category',array('hide_empty' => 1));
		//print_r($category_course);

		$htmldoc .= '<div class="course_cateory_tab">';
			$htmldoc .= '<ul>';
				$htmldoc .= '<li class="active"><a href="#all">All Categories</a></li>';
				if($category_courses):
				foreach($category_courses as $category_course):
					$htmldoc .= '<li><a href="#'.$category_course->slug.'">'.$category_course->name.'</a></li>';
				endforeach;
				endif;
			$htmldoc .= '</ul>';			
		$htmldoc .= '</div>';

		$htmldoc .= '<div class="tab_content_box">';

		$htmldoc .= '<div id="all" class="course_cateory_tab_content">';
			$course_locations = get_terms('course-location',array('hide_empty' => 1));
			if($course_locations):
				foreach($course_locations as $course_location):

					$htmldoc .= '<div class="class-list"><span><a class="clearfix" href="#'.$course_location->slug.'">'.$course_location->name.' <i class="icon-right-chevron"></i><span class="show_panel">View Class</span></a></span>';
						$args = array(
							'post_type' => 'sfwd-courses',
							'posts_per_page' => -1,
							'tax_query' => array(
							array(
								'taxonomy' => 'course-location',
								'field' => 'slug',
								'terms' => $course_location->slug,
								)
							)
						);
						$query = new WP_Query( $args );
						if ( $query->have_posts() ) {
						$htmldoc .= '<ul>';	
						while ( $query->have_posts() ) {
						$query->the_post();						
							$htmldoc .= '<li><a href="'.get_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a></li>';
						}
						$htmldoc .= '</ul>';
						}
						// Restore original post data.
						wp_reset_postdata();
					$htmldoc .= '</div>';
				endforeach;
			endif;	
		$htmldoc .= '</div>';

		if($category_courses):
			foreach($category_courses as $category_course):
			$htmldoc .= '<div id="'.$category_course->slug.'" class="course_cateory_tab_content displayhidden">';
				$course_locations = get_terms('course-location',array('hide_empty' => 1));
				if($course_locations):
					foreach($course_locations as $course_location):
						$args = array(
								'post_type' => 'sfwd-courses',
								'posts_per_page' => -1,
								'tax_query' => array(
								'relation' => 'AND',	
								array(
									'taxonomy' => 'course-location',
									'field' => 'slug',
									'terms' => $course_location->slug,
									),
								array(
									'taxonomy' => 'ld_course_category',
									'field' => 'slug',
									'terms' => $category_course->slug,
									)
								)
							);
						$query = new WP_Query( $args );							
							if ( $query->have_posts() ) {
							$htmldoc .= '<div class="class-list"><span><a class="clearfix" href="#'.$course_location->slug.'">'.$course_location->name.'<i class="icon-right-chevron"></i><span class="show_panel">View Class</span></a></span>';		
							$htmldoc .= '<ul>';	
							while ( $query->have_posts() ) {
							$query->the_post();						
								$htmldoc .= '<li><a href="'.get_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a></li>';
							}
							$htmldoc .= '</ul>';
							$htmldoc .= '</div>';
							}
							// Restore original post data.
							wp_reset_postdata();
					endforeach;
				endif;
			$htmldoc .= '</div>';
			endforeach;
		endif;	
		$htmldoc .= '</div>';

		return $htmldoc;
	}

public function legal_ld_course_list_by_partner(){
		$htmldoc = '';
		$category_courses = get_terms('ld_course_category',array('hide_empty' => 1));
		//print_r($category_course);

		$htmldoc .= '<div class="course_cateory_tab">';
			$htmldoc .= '<ul>';
				$htmldoc .= '<li class="active"><a href="#all">All Categories</a></li>';
				if($category_courses):
				foreach($category_courses as $category_course):
					$htmldoc .= '<li><a href="#'.$category_course->slug.'">'.$category_course->name.'</a></li>';
				endforeach;
				endif;
			$htmldoc .= '</ul>';			
		$htmldoc .= '</div>';

		$htmldoc .= '<div class="tab_content_box">';

		$htmldoc .= '<div id="all" class="course_cateory_tab_content">';
			$course_locations = get_terms('course-location',array('hide_empty' => 1));
			if($course_locations):
				foreach($course_locations as $course_location):

					$htmldoc .= '<div class="class-list"><span><a class="clearfix" href="#'.$course_location->slug.'">'.$course_location->name.' <i class="icon-right-chevron"></i><span class="show_panel">View Class</span></a></span>';
						$args = array(
							'post_type' => 'sfwd-courses',
							'posts_per_page' => -1,
							'tax_query' => array(
							array(
								'taxonomy' => 'course-location',
								'field' => 'slug',
								'terms' => $course_location->slug,
								)
							)
						);
						$query = new WP_Query( $args );
						if ( $query->have_posts() ) {
						$htmldoc .= '<ul>';	
						while ( $query->have_posts() ) {
						$query->the_post();						
							$htmldoc .= '<li><a href="'.get_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a></li>';
						}
						$htmldoc .= '</ul>';
						}
						// Restore original post data.
						wp_reset_postdata();
					$htmldoc .= '</div>';
				endforeach;
			endif;	
		$htmldoc .= '</div>';

		if($category_courses):
			foreach($category_courses as $category_course):
			$htmldoc .= '<div id="'.$category_course->slug.'" class="course_cateory_tab_content displayhidden">';
				$course_locations = get_terms('course-location',array('hide_empty' => 1));
				if($course_locations):
					foreach($course_locations as $course_location):
						$args = array(
								'post_type' => 'sfwd-courses',
								'posts_per_page' => -1,
								'tax_query' => array(
								'relation' => 'AND',	
								array(
									'taxonomy' => 'course-location',
									'field' => 'slug',
									'terms' => $course_location->slug,
									),
								array(
									'taxonomy' => 'ld_course_category',
									'field' => 'slug',
									'terms' => $category_course->slug,
									)
								)
							);
						$query = new WP_Query( $args );							
							if ( $query->have_posts() ) {
							$htmldoc .= '<div class="class-list"><span><a class="clearfix" href="#'.$course_location->slug.'">'.$course_location->name.'<i class="icon-right-chevron"></i><span class="show_panel">View Class</span></a></span>';		
							$htmldoc .= '<ul>';	
							while ( $query->have_posts() ) {
							$query->the_post();						
								$htmldoc .= '<li><a href="'.get_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a></li>';
							}
							$htmldoc .= '</ul>';
							$htmldoc .= '</div>';
							}
							// Restore original post data.
							wp_reset_postdata();
					endforeach;
				endif;
			$htmldoc .= '</div>';
			endforeach;
		endif;	
		$htmldoc .= '</div>';

		return $htmldoc;
	}

	public function legal_blog_post_list(){
		$dhtml = '';
		$dhtml .= '<div class="blog_list_page">';
		$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		$query = new WP_Query( array(
		'post_type' => 'post',
		'posts_per_page' => 10,
		'paged' => $paged
		));
		if ( $query->have_posts() ) :
		$dhtml .= '<ul class="blog_lsit">'	;
		while ( $query->have_posts() ) : $query->the_post();
			$postid = get_the_ID();
			$featured_img_url = get_the_post_thumbnail_url($postid,'full'); 

			$links = array(
			'facebook' => 'https://www.facebook.com/sharer/sharer.php?u=' . get_permalink(),
			'twitter'  => 'https://twitter.com/intent/tweet?text='. get_the_title() .'&url='. get_permalink() .'&via='. $twitter,
			'mail'     => 'mailto:?subject='. get_the_title() .'&body=Take a look at this link - ' . get_permalink(),
			'linkedin' => 'https://www.linkedin.com/shareArticle?mini=true&url='. get_permalink() .'&title='. get_the_title() .'&summary=' . get_the_excerpt(),
			'gplus'    => 'https://plus.google.com/share?url=' . get_permalink()
			);


			$dhtml .= '<li id="post-'.get_the_ID().'" class="list_post">';
			$dhtml .= '<h2><a href="'.get_the_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a></h2>';
			$dhtml .= '<div class="category_meta">';
			$cats = wp_get_post_categories($postid);
			if($cats): 
				$dhtml .= 'Categories : <ul>';
				foreach ( $cats as $category ) {
					$current_cat = get_cat_name($category);
					$cat_link = get_category_link($category);
					$dhtml .= '<li><a href='.$cat_link.'>'.$current_cat.'</a></li>';
				}
				$dhtml .= '</ul>'; 
			endif;
			$dhtml .= '</div>';
			$dhtml .= '<span class="authors">'.get_the_date('l, F j, Y').'  by '.get_the_author().'</span>';
			if($featured_img_url):
			$dhtml .= '<img class="legal_image_post" alt="'.get_the_title().'" src="'.esc_url($featured_img_url).'" />';
			else:
			$dhtml .= '<img class="legal_image_post" alt="'.get_the_title().'" src="'.esc_url(LEGAL_URL).'no-image.png" />';
			endif;
			$dhtml .= '<div class="post_excerpt">'.get_the_excerpt().'</div>';
			$dhtml .= '<div class="post_footer clearfix">';
			$dhtml .= '<div class="post_footer_left">'.get_comments_number().' Comments</div>';
			$dhtml .= '<div class="post_footer_right">
			<nav class="share" role="menu" aria-label="Share Links">
			<li class="share__facebook">
			<a href="'.$links['facebook'].'" class="share__link"><i class="icon-facebook"></i><span class="is-hidden">Share</span></a>
			</li>
			<li class="share__twitter">
			<a href="'.$links['twitter'].'" class="share__link"><i class="icon-twitter"></i><span class="is-hidden">Tweet</span></a>
			</li>			
			</nav>
			</div>';
			$dhtml .= '</div>';
			$dhtml .= '</li>';
		endwhile;			
		$dhtml .= '</ul>';
		wp_reset_postdata();
		else :
		$dhtml .= '<p>Sorry, no posts matched your criteria.</p>';	
		endif;

		$dhtml .= '<div class="pagination_legal">';

        $dhtml .= paginate_links( array(
            'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
            'total'        => $query->max_num_pages,
            'current'      => max( 1, get_query_var( 'paged' ) ),
            'format'       => '?paged=%#%',
            'show_all'     => false,
            'type'         => 'plain',
            'end_size'     => 2,
            'mid_size'     => 1,
            'prev_next'    => true,
            'prev_text'    => '<',
            'next_text'    => '>',
            'add_args'     => false,
            'add_fragment' => '',
        ));

		$dhtml .= '</div>';

		$dhtml .= '</div>';		
		return $dhtml;			
	}

	public function legal_faq_post_list(){ 
		$html = '';
		$html .= '<div class="faq_post_list">';
		$query = new WP_Query( array('post_type' => 'faqs','posts_per_page' => -1,'order' => 'ASC'));
		if ( $query->have_posts() ) :
		$html .= '<ul class="faq_list">';
		while ( $query->have_posts() ) : $query->the_post();
			$html .= '<li class="faq_list_li">';
			$html .= '<h2 class="faq_accrd_title"><a href="#faq_postid_'.get_the_ID().'">'.get_the_title().'<i class="icon-right-chevron"></i></a></h2>';
			$html .= '<div class="faq_accrd_content" id="faq_postid_'.get_the_ID().'"><p>'.get_the_content().'</p>';
			$html .= '</li>';
		endwhile;
		$html .= '</ul>';
		else:
			$html .= '<p>Sorry, no posts matched your criteria.</p>';	
		endif;
		$html .= '</div>';
		return $html;
	}
}
?>