jQuery(document).ready(function($){
        // Custom jQuery goes here

    function preloader_mylegal(data){
        if(data == 'show'){
            $('.mylegal_preloader_overlay,.mylegal_preloader').show();
        }else if(data == 'hide'){
            $('.mylegal_preloader_overlay,.mylegal_preloader').hide();
        }
    }    


    /*$('.quantity .input-text').on('change', function(){

        var user_count=$('.quantity .input-text').val();
        $('#inner_user_name_box').html('');
                //  alert(user_count);
        var i=1;
        for(i=1;i<=user_count;i++)
        {
            $('#inner_user_name_box')
            .append('<input placeholder="Enter Student Name #'+i+'"  class="student_name" type="text" name="student_name['+i+']" /><br/>');
        }
        
    });*/

    $('#newsletter').hide();

    $('#get_pdf').on('click', function(){
        $('#newsletter').toggle();
    });

    $('.popup-choice-white').on('click', function(){
        $('#pum-329').hide();
    });
        
    
     $('.close-my-popup').on('click', function(){
        $('#pum-335').hide();
    });

    jQuery( '.ajax-coupon-redeem button[type="submit"]').click( function( ev ) {
        var getbtn = $(this);
        getbtn.parent().find('.ajaxloader').show();
        var code = getbtn.parent().find( 'input.coupon').val();
        data = {
        action: 'rempty_coupon_page',
        coupon_code: code
        };
        jQuery.post( woocommerce_params.ajax_url, data, function( returned_data ) {
        if( returned_data.result == 'error' ) {
        //jQuery( 'p.result' ).html( returned_data.message );
        getbtn.parent().find('.ajaxloader').hide();
        location.reload();
        } else {
        // Hijack the browser and redirect user to cart page
        // window.location.href = returned_data.href;
        getbtn.parent().find('.ajaxloader').hide();
        location.reload();
        }
        });
        ev.preventDefault();
    });

    /*$('.mylegal_checkout a.wc-forward').click(function(){
        var student_name = Array();
        var student_email = Array();

        var namestudent = Array();
        var emailstudent = Array();
        var refirecthref = $(this).attr('href');
        $('.student_name').each(function(){
            if($(this).val() == ''){
                student_name.push(1);                
            }else{
                student_name.push(0);
                namestudent.push($(this).val());
            }
        });
        $('.student_email').each(function(){
            if($(this).val() == ''){
                student_email.push(1);                
            }else{
                student_email.push(0);
                emailstudent.push($(this).val());
            }
        });
        //console.log(namestudent);
        //console.log(emailstudent);
        if($.inArray( 1,student_name) !== -1){
            alert('Please Enter Student Name.');
        }else if($.inArray( 1,student_email) !== -1){
            alert('Please Enter Student Email.');
        }else{
            preloader_mylegal('show');
            $.ajax({
                type:'POST',
                url:woocommerce_params.ajax_url,
                data:{'action':'AddCustomMetaToCart','name':namestudent,'email':emailstudent},
                dataType:'html',
                success:function(res){
                    //console.log(res);
                    window.location.href = refirecthref;                    
                    preloader_mylegal('hide');
                }

            })
        }
        return false;
    });*/

    $('input[name="one_email_notification_check"]').click(function(){
        if($(this).is(':checked')){
            var getemail = $(this).parent().parent().find('.student_details_start .student_email').val();
            $(this).parent().parent().find('.student_details.down .student_email').val(getemail);
        }else{
           
            $(this).parent().parent().find('.student_details.down .student_email').each(function(){
                if($(this).attr('datavalue') == ''){
                    $(this).val('');
                }else{
                    $(this).val($(this).attr('datavalue'));
                }
            });
        }
    });

    $( 'body' ).on( 'updated_checkout', function(res) {
        $.ajax({
                type:'POST',
                url:woocommerce_params.ajax_url,
                data:{'action':'UpdateLegalCheckoutPrice'},
                dataType:'json',
                success:function(res){
                    $('.mylegal_checkout_total tr.order-total td').html(res.grand_total);
                    $('.mylegal_checkout_total tr.shipping-total th').html(res.shippinglabel);
                    $('.mylegal_checkout_total tr.shipping-total td').html(res.shippingcost);
                    $('.shipping_method_right').html(res.shippinglabel + ' '+ res.shippingcost);
                }

        });        
    });


    // --------------------------------   BILLING INFO --------------------------------  //
    if($('.my_legal_billing #billing_first_name').val() != ''){
        var firstname = $('.my_legal_billing #billing_first_name').val(); 
        var middlename = $('.my_legal_billing #billing_middle_name').val(); 
        var lastname = $('.my_legal_billing #billing_last_name').val(); 
        var name = firstname+' '+middlename+' '+lastname;
        $('.billing_info span.name').html(name);
    }

    if($('.my_legal_billing #billing_company').val() != ''){
        var company = $('.my_legal_billing #billing_company').val(); 
        $('.billing_info span.company').html(company);
    }

    if($('.my_legal_billing #billing_address_1').val() != ''){
        var billing_address_1 = $('.my_legal_billing #billing_address_1').val(); 
        $('.billing_info span.address1').html(billing_address_1);
    }

    if($('.my_legal_billing #billing_address_2').val() != ''){
        var billing_address_2 = $('.my_legal_billing #billing_address_2').val(); 
        $('.billing_info span.address2').html(billing_address_2);
    }    

    if($('.my_legal_billing #billing_phone').val() != ''){
        var phone = $('.my_legal_billing #billing_phone').val(); 
        $('.billing_info span.phone').html(phone);
    }
    // --------------------------------   BILLING INFO --------------------------------  //

    // --------------------------------   SHIPPING INFO --------------------------------  //
    if($('.my_legal_shipping #shipping_first_name').val() != ''){
        var firstname = $('.my_legal_shipping #shipping_first_name').val(); 
        var middlename = $('.my_legal_shipping #shipping_middle_name').val(); 
        var lastname = $('.my_legal_shipping #shipping_last_name').val(); 
        var name = firstname+' '+middlename+' '+lastname;
        $('.shipping_info span.name').html(name);
    }

    if($('.my_legal_shipping #shipping_company').val() != ''){
        var company = $('.my_legal_shipping #shipping_company').val(); 
        $('.shipping_info span.company').html(company);
    }

    if($('.my_legal_shipping #shipping_address_1').val() != ''){
        var billing_address_1 = $('.my_legal_shipping #shippingaddress_1').val(); 
        $('.shipping_info span.address1').html(billing_address_1);
    }

    if($('.my_legal_shipping #shipping_address_2').val() != ''){
        var billing_address_2 = $('.my_legal_shipping #shipping_address_2').val(); 
        $('.shipping_info span.address2').html(billing_address_2);
    }    

    if($('.my_legal_shipping #shipping_telephone').val() != ''){
        var phone = $('.my_legal_shipping #shipping_telephone').val(); 
        $('.shipping_info span.phone').html(phone);
    }
    // --------------------------------   SHIPPING INFO --------------------------------  //


    /*----------------------------------------  BILLING KEYUP ------------------------------*/
    $('.my_legal_billing #billing_first_name,.my_legal_billing #billing_middle_name,.my_legal_billing #billing_last_name').keyup(function(){
        //$('.billing_info span.name').html($(this).val());
        var firstname = $('.my_legal_billing #billing_first_name').val(); 
        var middlename = $('.my_legal_billing #billing_middle_name').val(); 
        var lastname = $('.my_legal_billing #billing_last_name').val(); 
        var name = firstname+' '+middlename+' '+lastname;
        $('.billing_info span.name').html(name);
    });

    $('.my_legal_billing #billing_company').keyup(function(){
        $('.billing_info span.company').html($(this).val());
    });

    $('.my_legal_billing #billing_address_1').keyup(function(){
        $('.billing_info span.address1').html($(this).val());
    });

    $('.my_legal_billing #billing_address_2').keyup(function(){
        $('.billing_info span.address2').html($(this).val());
    });

    $('.my_legal_billing #billing_city,.my_legal_billing #billing_postcode').keyup(function(){
        //$('.billing_info span.name').html($(this).val());
        var billing_city = $('.my_legal_billing #billing_city').val(); 
        var billing_state = $('.my_legal_billing #select2-billing_state-container').attr('title');
        var billing_postcode = $('.my_legal_billing #billing_postcode').val(); 
        var location = billing_city+','+billing_state+','+billing_postcode;
        $('.billing_info span.location').html(location);
    });

    $(document).on('change','.my_legal_billing #billing_state',function(){
        var billing_city = $('.my_legal_billing #billing_city').val(); 
        var billing_state = $('#billing_state option:selected').text();
        var billing_postcode = $('.my_legal_billing #billing_postcode').val(); 
        var location = billing_city+','+billing_state+','+billing_postcode;
        $('.billing_info span.location').html(location);
    });

    $('.my_legal_billing #billing_phone').keyup(function(){
        $('.billing_info span.phone').html($(this).val());
    });

    $('.my_legal_billing #billing_country').change(function(){
        $('.billing_info span.country').html($('#billing_country option:selected').text());
    });
    /*----------------------------------------  BILLING KEYUP ------------------------------*/

    /*----------------------------------------  SHIPPING KEYUP ------------------------------*/
    $('.my_legal_shipping #shipping_first_name,.my_legal_shipping #shipping_middle_name,.my_legal_shipping #shipping_last_name').keyup(function(){
        //$('.shipping_info span.name').html($(this).val());
        var firstname = $('.my_legal_shipping #shipping_first_name').val(); 
        var middlename = $('.my_legal_shipping #shipping_middle_name').val(); 
        var lastname = $('.my_legal_shipping #shipping_last_name').val(); 
        var name = firstname+' '+middlename+' '+lastname;
        $('.shipping_info span.name').html(name);
    });

    $('.my_legal_shipping #shipping_company').keyup(function(){
        $('.shipping_info span.company').html($(this).val());
    });

    $('.my_legal_shipping #shipping_address_1').keyup(function(){
        $('.shipping_info span.address1').html($(this).val());
    });

    $('.my_legal_shipping #shipping_address_2').keyup(function(){
        $('.shipping_info span.address2').html($(this).val());
    });

    $('.my_legal_shipping #shipping_city,.my_legal_shipping #shipping_postcode').keyup(function(){
        //$('.shipping_info span.name').html($(this).val());
        var shipping_city = $('.my_legal_shipping #shipping_city').val(); 
        var shipping_state = $('.my_legal_shipping #select2-shipping_state-container').attr('title');
        var shipping_postcode = $('.my_legal_shipping #shipping_postcode').val(); 
        var location = shipping_city+','+shipping_state+','+shipping_postcode;
        $('.shipping_info span.location').html(location);
    });

    $(document).on('change','.my_legal_shipping #shipping_state',function(){
        var shipping_city = $('.my_legal_shipping #shipping_city').val(); 
        var shipping_state = $('#shipping_state option:selected').text();
        var shipping_postcode = $('.my_legal_shipping #shipping_postcode').val(); 
        var location = shipping_city+','+shipping_state+','+shipping_postcode;
        $('.shipping_info span.location').html(location);
    });

    $('.my_legal_shipping #shipping_telephone').keyup(function(){
        $('.shipping_info span.phone').html($(this).val());
    });

    $('.my_legal_shipping #shipping_country').change(function(){
        $('.shipping_info span.country').html($('#shipping_country option:selected').text());
    });
    /*----------------------------------------  SHIPPING KEYUP ------------------------------*/

    $('input[name="shipto_billing_address"]').click(function(){
        if($(this).is(':checked')){
            $('#shipping_first_name').val($('#billing_first_name').val());
            $('#shipping_middle_name').val($('#billing_middle_name').val());
            $('#shipping_last_name').val($('#billing_last_name').val());
            $('#shipping_company').val($('#billing_company').val());
            $('#shipping_email').val($('#billing_email').val());
            $('#shipping_address_1').val($('#billing_address_1').val());
            $('#shipping_address_2').val($('#billing_address_2').val());
            $('#shipping_city').val($('#billing_city').val());
            $('#shipping_postcode').val($('#billing_postcode').val());
            $('#shipping_telephone').val($('#billing_phone').val());
            $('#shipping_fax').val($('#billing_fax').val());
        }else{
            //alert('FALSE');
        }
    });

    //$('#optin_form_step_1_section').modal('show');
    if ($.cookie("no_thanks") == null) {
        // Show the modal, with delay func.
        $('#optin_form_step_1_section').appendTo("body");
        function show_modal(){
            $('#optin_form_step_1_section').modal();
            $('#optin_form_step_1_section').on('shown.bs.modal', function(){
                $('#optin_step_1_form')[0].reset();
            });
            //$('#optin_form_step_2_section').modal();
        }
        if(optin_data.form_frontend_show == 'on'){
            show_modal();
        }
    }

    $(".nothanks").click(function() {
        $.cookie('no_thanks', 'true', { expires: 365, path: '/' });
    });

    $(document).on('submit','#optin_step_1_form',function(){
        var getstate = $('#legal_optin_state').val();
        var getemail = $('#legal_optin_email').val();
        if(getstate != '' && getemail != ''){
            $('#legal_optin_btn').removeClass('disabled');
            preloader_mylegal('show');
            /*$('#optin_form_step_1_section').modal('hide');
            $('#optin_form_step_2_section').modal('show');
            $('#legal_optin_btn').removeClass('disabled');*/
          $.ajax({
            type:'POST',
            url:woocommerce_params.ajax_url,
            data:{'action':'SendOptinFormMail','state':getstate,'email':getemail},
            dataType:'html',
            success:function(res){
                $('#optin_form_step_1_section').modal('hide');
                $('#optin_form_step_2_section').modal();
                $('#optin_form_step_2_section').on('shown.bs.modal', function(){
                    $('.optin_2_downloadbtn').html(res);
                });
                preloader_mylegal('hide');
                $.cookie('no_thanks', 'true', { expires: 365, path: '/' });
                //console.log(res);
            }
          });
          //preloader_mylegal('show');
        }
        return false;
    });

    $(document).on('change','#legal_optin_state',function(){
        var getstate = $(this).val();
        var getemail = $('#legal_optin_email').val();
        if(getstate != '' && getemail != ''){
            $('#legal_optin_btn').removeClass('disabled');
        }else{
            $('#legal_optin_btn').addClass('disabled');
        }
    });

    $(document).on('keyup','#legal_optin_email',function(){
        var getstate = $('#legal_optin_state').val();
        var getemail = $(this).val();        
        if((getstate != '') && (getemail != '')){
            $('#legal_optin_btn').removeClass('disabled');
        }else{
            $('#legal_optin_btn').addClass('disabled');
        }
    });

    //$('#ee-spco-attendee_information-reg-step-form').attr('action','http://localhost/mylegalheat/cart');
    //$('.event-tickets form').append()

    /*$(document).on('submit','#ee-spco-attendee_information-reg-step-form',function(){
        alert('dddd');
        return false;
    });
    $('#ee-spco-attendee_information-reg-step-form').addClass('dddddd');*/

    /*$(document).ajaxSuccess(function(e, xhr, settings) {
        //console.log(settings.data);
        if(settings.data.indexOf( "step=attendee_information") != -1){
            //alert('dddd');
            //$('#spco-go-to-step-finalize_registration-submit').click();
            //alert('dddd');
            //$('#spco-go-to-step-finalize_registration-submit').addClass('dddddd');
            //$('#ee-spco-payment_options-reg-step-form').submit();
            //$('#spco-go-to-step-finalize_registration-submit').trigger('click');
            $('#ee-spco-payment_options-reg-step-form').submit();
            $('#spco-go-to-step-finalize_registration-submit').click();
            //return true;
            //alert('dddd');
        }//
    });*/

    $(document).on('click','a.woocommerce-remove-coupon',function(){
        //location.reload();
        setTimeout(function(){
            location.reload();
        },2000);
        return false;
    });

    $(document).on('click','.ticket_view_url',function(){
        //$('#OrderTicketModal').modal('show'); 
        preloader_mylegal('show');
        var ticket_url = $(this).attr('ticket_regurl');
        var current_url = $(this).attr('href');
        $.ajax({
                type:'POST',
                url:woocommerce_params.ajax_url,
                data:{'action':'GetOrderTicketRegistrationDetails','regurl':ticket_url,'current_url':current_url},
                success:function(res){
                    $('#OrderTicketModal .modal-body').html(res);
                    $('#OrderTicketModal').modal('show');
                    preloader_mylegal('hide');
                }

        }); 
        return false;
    });

    $('#spco-payment-info-table th').removeClass('jst-cntr').addClass('jst-rght');

   
    $('input.ee-reg-qstn-email').each(function(i,data){
        if(i == 0){
            $(data).attr('required','required');
        }
    });

});

jQuery(window).load(function(){
    var $ = jQuery;
    // --------------------------------   BILLING INFO --------------------------------  //
    if($('.my_legal_billing #billing_city').val() != '' && $('.my_legal_billing #billing_state:selected').val() != '' && $('.my_legal_billing #billing_postcode').val() != ''){
        var billing_city = $('.my_legal_billing #billing_city').val(); 
        var billing_state = $('.my_legal_billing #select2-billing_state-container').attr('title');
        var billing_postcode = $('.my_legal_billing #billing_postcode').val(); 
        if(billing_state == null){
            billing_state = 'NULL';
        }
        var location = billing_city+','+billing_state+','+billing_postcode;
        $('.billing_info span.location').html(location);
    }

    if($('.my_legal_billing #select2-billing_country-container').attr('title') != ''){
        var billing_country = $('.my_legal_billing #select2-billing_country-container').attr('title'); 
        $('.billing_info span.country').html(billing_country);
    }
    // --------------------------------   BILLING INFO --------------------------------  //

    // --------------------------------   SHIPPING INFO --------------------------------  //
    if($('.my_legal_shipping #shipping_city').val() != '' && $('.my_legal_billing #shipping_state:selected').val() != '' && $('.my_legal_billing #shipping_postcode').val() != ''){
        var billing_city = $('.my_legal_shipping #shipping_city').val(); 
        var billing_state = $('.my_legal_shipping #select2-shipping_state-container').attr('title');
        var billing_postcode = $('.my_legal_shipping #shipping_postcode').val(); 
        var location = billing_city+','+billing_state+','+billing_postcode;
        $('.shipping_info span.location').html(location);
    }

    if($('.my_legal_shipping #select2-shipping_country-container').attr('title') != ''){
        var billing_country = $('.my_legal_shipping #select2-shipping_country-container').attr('title'); 
        $('.shipping_info span.country').html(billing_country);
    }
    // --------------------------------   SHIPPING INFO --------------------------------  //

    $('.checkout_user_data_right #legal_terms_and_condition_field,.checkout_user_data_right #non_refundable_check_field').remove();

    $('#checkout_login_btn').click(function(){
        $('.show_loader').show();
        $('.mylegal_checkout_page p.status').hide();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: woocommerce_params.ajax_url,
            data: { 
                'action': 'ajaxloginMylegal', //calls wp_ajax_nopriv_ajaxlogin
                'username': $('.mylegal_checkout_page #checkout_user_email').val(), 
                'password': $('.mylegal_checkout_page #checkout_user_password').val(), 
                'security': $('.mylegal_checkout_page #security').val() },
            success: function(data){                
                if (data.loggedin == false){ 
                    $('.mylegal_checkout_page p.status').html(data.message).show().removeClass('alert-success').addClass(data.class);
                    $('.show_loader').hide();
                }
                if (data.loggedin == true){ 
                    $('.mylegal_checkout_page p.status').html(data.message).show().removeClass('alert-danger').addClass(data.class);
                }
                if (data.loggedin == true){                    
                    //document.location.href = ajax_login_object.redirecturl;
                    //location.reload();
                    setTimeout(function(){
                        $('.show_loader').hide();
                        window.location.reload(true);
                    },2000);
                }
            }
        });
        return false;
    }); 

    $('#checkout_as_guest').click(function(){
        $('.guest_checkout').fadeToggle('slow');
        $('#billing_country,#billing_state,#shipping_country,#shipping_state').select2();  
        $('.woocommerce .mylegal_checkout_page .my_legal_billing .create-account').remove();      
        return false;
    });

    $('.woocommerce .woocommerce_legal_cart .cart_update_button').removeAttr('disabled');


    $('.ticket_select_radio').click(function(){
        var qty_id = $(this).attr('qty_id');
        var event_id = $(this).attr('event_id');
        var event_price = $(this).attr('event_price');

        var event_price_unformatted = $(this).attr('event_price_unformatted');

        var ticketname = $(this).attr('ticketname');

        var ticketid = $(this).val();

        var pro_id = $(this).attr('product_id');
        $('.ticket-selector-tbl-qty-slct').attr('id','ticket-selector-tbl-qty-slct-'+event_id+'-'+qty_id);
        $('.ticket-selector-tbl-qty-slct').attr('name','tkt-slctr-qty-'+event_id+'[]');   
        $('.price.custom_price_data_legal.local_price_table .woocommerce-Price-amount.amount').html(event_price);
        $('#tkt-slctr-product').val(pro_id);
        $('#tkt-slctr-product-price').val(event_price_unformatted);
        $('#tkt-slctr-product-ticketname').val(ticketname);
        $('.ticket-selector-tbl-tickedid').val(ticketid);
    });

    if($('.ywsfd-social > .ywsfd-social-button > .ywsfd-facebook-share').length > 0){
        $('.ywsfd-social > .ywsfd-social-button > .ywsfd-facebook-share').html('<i class="fa fa-facebook-f"></i>');
    }

    if($('.ywsfd-social > .ywsfd-social-button > .ywsfd-tweet-button').length > 0){
        $('.ywsfd-social > .ywsfd-social-button > .ywsfd-tweet-button').html('<i class="fa fa-twitter"></i>');
    }

});