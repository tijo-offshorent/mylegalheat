<?php 
add_action('wp_head','show_registration_details');
function show_registration_details(){
if(isset($_REQUEST['view_registration_details']) && $_REQUEST['view_registration_details'] == true):
	?>
	<script type="text/javascript">
		jQuery(document).ready(function($){
			$('.ee-button.ee-roundish').attr('target','_blank');
		});
	</script>
	<?php 
	echo '<div class="registration_modal">';
	global $wpdb;
	$regurl = $_REQUEST['regurl'];
	$TXN_model = EE_Registry::instance()->load_model('Transaction');
	$current_txn = $TXN_model->get_transaction_from_reg_url_link($regurl);
	
	$template_args['TXN_receipt_url'] = $current_txn->receipt_url('html');
    if (! empty($template_args['TXN_receipt_url'])) {
        $template_args['order_conf_desc'] = __(
            '%1$sCongratulations%2$sYour registration has been successfully processed.%3$sCheck your email for your registration confirmation or click the button below to view / download / print a full description of your purchases and registration information.',
            'event_espresso'
        );
    } else {
        $template_args['order_conf_desc'] = __(
            '%1$sCongratulations%2$sYour registration has been successfully processed.%3$sCheck your email for your registration confirmation.',
            'event_espresso'
        );
    }
    $template_args['transaction'] = $current_txn;
    $template_args['revisit'] = EE_Registry::instance()->REQ->get('revisit', false);

	echo EEH_Template::locate_template(
	        THANK_YOU_TEMPLATES_PATH . 'thank-you-page-overview.template.php',
	        $template_args,
	        true,
	        true
	);

	//*-----------------------------   ATTENDEE RESULTS -------------------------------------------
	$template_args = array();
	$template_args['transaction'] = $current_txn;
	$template_args['reg_url_link'] = $regurl;    

	$primary_registrant = $current_txn->primary_registration() instanceof EE_Registration
	        ? $current_txn->primary_registration()
	        : null;      
	$_SPCO_attendee_information_url = $primary_registrant instanceof EE_Registration
	        ? $primary_registrant->edit_attendee_information_url()
	        : false;    



	$template_args['is_primary'] = 1;
	$template_args['SPCO_attendee_information_url'] = $_SPCO_attendee_information_url;
	$template_args['resend_reg_confirmation_url'] = add_query_arg(
	    array('token' => $regurl, 'resend_reg_confirmation' => 'true'),
	    EE_Registry::instance()->CFG->core->thank_you_page_url()
	);
	// verify template arguments
	EEH_Template_Validator::verify_instanceof($template_args['transaction'], '$transaction', 'EE_Transaction');
	EEH_Template_Validator::verify_isnt_null(
	    $template_args['SPCO_attendee_information_url'],
	    '$SPCO_attendee_information_url'
	);
	echo EEH_Template::locate_template(
	    THANK_YOU_TEMPLATES_PATH . 'thank-you-page-registration-details.template.php',
	    $template_args,
	    true,
	    true
	);
	echo '</div>';
	exit;
endif;
}
?>