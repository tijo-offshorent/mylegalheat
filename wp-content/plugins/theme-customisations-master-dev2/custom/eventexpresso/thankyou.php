<?php 
//echo '<pre>';
global $wpdb;
$regids = array();
$regurl = '';
$tableregistration = $wpdb->prefix.'esp_registration';
$items = $order->get_items();
$regidarrays = array();
foreach ( $items as $item_id => $item ) {
    $data = wc_get_order_item_meta( $item_id, 'event_regid', true );
    $regids[] = $data;
    $regidarrays = $data;	
}
$status = EEM_Registration::status_id_approved;
$reg_url = array();
if($regids): foreach($regids as $regid):
	foreach($regid as $reid):
	$getData = $wpdb->get_row('SELECT * FROM '.$tableregistration.' WHERE REG_ID='.$reid,ARRAY_A);
	$REG = EEM_Registration::instance()->get_one_by_ID($reid);
	$REG->set_status( $status);
	$REG->save();
	$reg_url[] = $getData['REG_url_link'];
    $wpdb->update($wpdb->prefix.'esp_transaction', array('STS_ID' => 'TCM'), array('TXN_ID' => $getData['TXN_ID']));
	endforeach;	
endforeach;
endif;


foreach($regidarrays as $regidsdata){
    $resend_ticket_notice_url = EEH_URL::add_query_args_and_nonce(
        array(
            'action'  => 'resend_ticket_notice',
            '_REG_ID' => $regidsdata,
        ),
        admin_url('admin.php?page=espresso_registrations')
    );
}
?>
<script type="text/javascript">
    jQuery(window).load(function(){
        //alert('dddd');
        jQuery.ajax({
        url: '<?php echo $resend_ticket_notice_url; ?>',
        dataType: 'json',
        //async: true,
        success: function (data) {
            return false;
        },
        error: function (data) {
            return false; 
        }
        });
    });
</script>
