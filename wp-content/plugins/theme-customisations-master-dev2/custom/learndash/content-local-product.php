<?php $event = get_field('add_event_id_with_course',get_the_ID()); ?>
<div class="class_single_type">
	<div class="row">
		<div class="col-lg-8 col-md-12 col-sm-12">	
			<?php the_content(); ?>
		</div>
		<div class="col-lg-4 col-md-12 col-sm-12">
			<div class="local_product_sidebar">
				<div class="local_product_sidebar_top">
					<div class="local_product_sidebar_image"><?php include('single-product/product-image.php'); ?></div>
					<div class="local_product_sidebar_content">
						<div class="local_product_sidebar_price"><?php include('single-product/local-price.php'); ?></div>
						<div class="local_product_sidebar_variable">
						<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>
						<div class="event-tickets" style="clear: both;">
							<h2 class="label_title">Choose a date and time:</h2>
							<?php //espresso_ticket_selector( $event ); ?>
							<?php $courselink = get_permalink(); include('single-product/event-expresso-localcourse.php'); ?>
						</div>
						<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>							
						</div>
						<div class="local_product_sidebar_include">
							<h2>Includes:</h2>
							<ul>
								<li>Full lifetime access</li>
								<li>Access on mobile and Desktop</li>
								<li>Certificate of Completion</li>
							</ul>
						</div>
						<?php wc_print_notices(); ?>
						<div class="local_product_sidebar_havecopoun"><a data-toggle="modal" data-target="#coupon_modal" href="#">Have a coupon?</a></div>
				</div>
				</div>
				<div class="local_product_sidebar_bottom local">
					<!-- <h2>Want to share this course?</h2> -->
					<?php echo do_shortcode('[ywsfd_shortcode]'); ?>
					<!-- <ul>
						<li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-envelope"></i></a></li>
						<li><a href="#"><i class="fa fa-link"></i>	</a></li>
					</ul> -->
				</div>				
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div id="coupon_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="ajax-coupon-redeem coupon">
			<input type="text" name="coupon" class="coupon"  value="" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>"/>
			<button type="submit" class="button" name="redeem-coupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>"><?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?></button>
				<?php do_action( 'woocommerce_cart_coupon' ); ?>
			<img class="ajaxloader" alt="" src="<?php echo LEGAL_URL2; ?>/loader.gif">	
		</div>
      </div>
    </div>

  </div>
</div>