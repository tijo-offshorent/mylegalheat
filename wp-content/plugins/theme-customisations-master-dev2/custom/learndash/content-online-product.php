<div class="class_single_type">
	<div class="row">
		<div class="col-lg-8 col-md-12 col-sm-12">
			<?php the_content(); ?>
		</div>
		<div class="col-lg-4 col-md-12 col-sm-12">
			<div class="local_product_sidebar">
				<div class="local_product_sidebar_top">
					<div class="local_product_sidebar_image"><?php include('single-product/product-video.php'); ?></div>
					<div class="local_product_sidebar_content">
						<div class="local_product_sidebar_price"><?php include('single-product/online-price.php'); ?></div>
						<div class="local_product_sidebar_variable">
							<?php
							$getproduct = get_field('product_course',get_the_ID());
							$product = wc_get_product( $getproduct->ID );

							$pid= $product->get_id();
							$is_related_to_course=get_post_meta( $pid, 'is_related_to_course', true );

							if ( ! $product->is_purchasable() ) {
							return;
							}


							if ( $product->is_in_stock() ) : ?>

							<?php do_action( 'woocommerce_before_add_to_cart_form' ); global $woocommerce; ?>
							<?php /*if($is_related_to_course==1): ?>
							<div><label><b># of students attending</b></label><br/></div>
							<?php endif;*/ ?>

							<form class="cart" action="<?php echo $woocommerce->cart->get_cart_url(); ?>" method="post" enctype='multipart/form-data'>
							<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

							<?php
							/*do_action( 'woocommerce_before_add_to_cart_quantity' );

							woocommerce_quantity_input( array(
							'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
							'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
							'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
							) );

							do_action( 'woocommerce_after_add_to_cart_quantity' );*/
							?>
							<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt">Buy Now</button>



							<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
							</form>

							<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

							<?php endif; ?>
						</div>
						<div class="local_product_sidebar_include">
							<h2>Includes:</h2>
							<ul>
								<li>Full lifetime access</li>
								<li>Access on mobile and Desktop</li>
								<li>Certificate of Completion</li>
							</ul>
						</div>
						<?php wc_print_notices(); ?>
						<div class="local_product_sidebar_havecopoun"><a data-toggle="modal" data-target="#coupon_modal" href="#">Have a coupon?</a></div>
						<!-- <div class="local_product_sidebar_addwhishlist"><a href="#">Add to Wish List</a></div> -->
						<div class="local_product_sidebar_addwhishlist"><?php echo do_shortcode('[ti_wishlists_addtowishlist product_id="'.$getproduct->ID.'"]'); ?></div>
					</div>
				</div>
				</div>
				<div class="local_product_sidebar_bottom">
					<?php echo do_shortcode('[ywsfd_shortcode]'); ?>
					<!-- <h2>Want to share this course?</h2>
					<ul>
						<li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-envelope"></i></a></li>
						<li><a href="#"><i class="fa fa-link"></i>	</a></li>
					</ul> -->
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div id="coupon_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="ajax-coupon-redeem coupon">
			<input type="text" name="coupon" class="coupon"  value="" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>"/>
			<button type="submit" class="button" name="redeem-coupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>"><?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?></button>
				<?php do_action( 'woocommerce_cart_coupon' ); ?>
			<img class="ajaxloader" alt="" src="<?php echo LEGAL_URL2; ?>/loader.gif">	
		</div>
      </div>
    </div>

  </div>
</div>