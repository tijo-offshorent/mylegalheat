<?php $event_id = $event; global $wpdb; ?>
<form method="POST" action="<?php echo get_permalink($event_id); ?>" name="ticket-selector-form-<?php echo $event_id; ?>">
	<input name="ee" value="process_ticket_selections" type="hidden">
	<div id="tkt-slctr-tbl-wrap-dv-<?php echo $event_id; ?>" class="tkt-slctr-tbl-wrap-dv">
		<?php 
		$tickets = EEH_Event_View::event_tickets_available( $event_id );
		if ( is_array( $tickets ) && ! empty( $tickets )) {
			?>
			<ul class="class_tickets">
			<?php 
			$k = 1;
			$getproductid = get_field('event_product',$event_id);
			//print_r($getproductid->ID);
			foreach ( $tickets as $ticket ) {
				if ( $ticket instanceof EE_Ticket ) { ?>
				<?php 
				$args = apply_filters(
				'wc_price_args', wp_parse_args(
				$args, array(
				'ex_tax_label'       => false,
				'currency'           => '',
				'decimal_separator'  => wc_get_price_decimal_separator(),
				'thousand_separator' => wc_get_price_thousand_separator(),
				'decimals'           => wc_get_price_decimals(),
				'price_format'       => get_woocommerce_price_format(),
				)
				)
				);

				//print_r($ticket->ticket_status());

				$get_price_custom = get_post_meta($event_id,'event_custom_price',true);

				//$unformatted_price = $ticket->get_ticket_total_with_taxes();
				$unformatted_price = $ticket->get_ticket_total_with_taxes();
				$price             = apply_filters( 'raw_woocommerce_price', floatval( $negative ? $unformatted_price * -1 : $unformatted_price ) );
				$price             = apply_filters( 'formatted_woocommerce_price', number_format( $price, $args['decimals'], $args['decimal_separator'], $args['thousand_separator'] ), $price, $args['decimals'], $args['decimal_separator'], $args['thousand_separator'] );

				if ( apply_filters( 'woocommerce_price_trim_zeros', false ) && $args['decimals'] > 0 ) {
				$price = wc_trim_zeros( $price );
				}

				$formatted_price = ( $negative ? '-' : '' ) . sprintf( $args['price_format'], get_woocommerce_currency_symbol( $args['currency'] ), $price );
				?>

				<?php if($ticket->ticket_status() == 'TKO'): ?>
				<?php if($k == 1):
				$ticket_id = $ticket->ID();
				?>
				<li>
				    <input ticketname="<?php echo $ticket->name(); ?>" event_price_unformatted="<?php echo $unformatted_price; ?>" product_id="<?php echo $getproductid->ID; ?>" type="radio" id="tkt-slctr-ticket-id-val-<?php echo $event_id;?>-<?php echo $k; ?>" name="radio-group" checked event_price="<?php echo $formatted_price; ?>" checked name="tkt-slctr-ticket-id-val-<?php echo $event_id;?>" class="ticket_select_radio" qty_id="<?php echo $k; ?>" event_id="<?php echo $event_id; ?>" value="<?php echo $ticket->ID();?>">
				    <label for="tkt-slctr-ticket-id-val-<?php echo $event_id;?>-<?php echo $k; ?>"><?php echo $ticket->name(); ?></label>
				    <!--input name="tkt-slctr-ticket-id-<?php echo $event_id; ?>[]" value="<?php echo $ticket->ID();?>" type="hidden"-->
				</li>	
				<?php else: ?>
				<li>	
				    <input ticketname="<?php echo $ticket->name(); ?>" event_price_unformatted="<?php echo $unformatted_price; ?>" product_id="<?php echo $getproductid->ID; ?>" type="radio" id="tkt-slctr-ticket-id-val-<?php echo $event_id;?>-<?php echo $k; ?>" name="radio-group" event_price="<?php echo $formatted_price; ?>" name="tkt-slctr-ticket-id-val-<?php echo $event_id;?>" class="ticket_select_radio" qty_id="<?php echo $k; ?>" event_id="<?php echo $event_id; ?>" value="<?php echo $ticket->ID();?>">
				    <label for="tkt-slctr-ticket-id-val-<?php echo $event_id;?>-<?php echo $k; ?>"><?php echo $ticket->name(); ?></label>
				    <!--input name="tkt-slctr-ticket-id-<?php echo $event_id; ?>[]" value="<?php echo $ticket->ID();?>" type="hidden"-->
				</li>	
				<?php endif; ?>
				<?php endif; ?>
				<?php
				} $k++;
			}
			?>
			</ul>
			<?php
		}
		$return_url = $courselink.'#tkt-slctr-tbl-'.$event_id;
		?>
		<h2 class="label_title"># of students attending:</h2>
		<input name="tkt-slctr-qty-<?php echo $event_id; ?>[]" id="ticket-selector-tbl-qty-slct-<?php echo $event_id; ?>-1" class="ticket-selector-tbl-qty-slct" type="text" value="1">		
		<input name="tkt-slctr-ticket-id-<?php echo $event_id; ?>[]" value="<?php echo $ticket_id; ?>" type="hidden" class="ticket-selector-tbl-tickedid">

		<input name="tkt-slctr-product" id="tkt-slctr-product" class="tkt-slctr-product-class" type="hidden" value="<?php echo $getproductid->ID; ?>">

		<input name="tkt-slctr-product-price" id="tkt-slctr-product-price" class="tkt-slctr-product-price-class" type="hidden" value="<?php echo $firstprice; ?>" />

		<input name="tkt-slctr-product-ticketname" id="tkt-slctr-product-ticketname" class="tkt-slctr-product-ticketname-class" type="hidden" value="<?php echo $ticketname; ?>" />

		<input name="noheader" value="true" type="hidden">

		<input name="tkt-slctr-return-url-<?php echo $event_id; ?>" value="<?php echo $return_url; ?>" type="hidden">

		<input name="tkt-slctr-rows-<?php echo $event_id; ?>" value="2" type="hidden"><input name="tkt-slctr-max-atndz-<?php echo $event_id; ?>" value="10" type="hidden"><input name="tkt-slctr-event-id" value="<?php echo $event_id; ?>" type="hidden">

		<?php 
		$do_not_enter = esc_html__('please do not enter anything in this input', 'event_espresso');
        $time = microtime(true);
        $html = '<div class="tkt-slctr-request-processor-dv" style="float:left; margin:0 0 0 -999em; height: 0;">';
        $html .= '<label for="tkt-slctr-request-processor-email-' . $time . '">' . $do_not_enter . '</label>';
        $html .= '<input type="email" id="tkt-slctr-request-processor-email-';
        $html .= $time . '" name="tkt-slctr-request-processor-email" value=""/>';
        $html .= '<input type="hidden" name="tkt-slctr-request-processor-token" value="';
        if (EE_Registry::instance()->CFG->registration->use_encryption) {
            EE_Registry::instance()->load_core('EE_Encryption');
            $html .= EE_Encryption::instance()->encrypt($time);
        } else {
            $html .= $time;
        }
        $html .= '"/>';
        $html .= '</div><!-- .tkt-slctr-request-processor-dv -->';
        echo $html;


        $btn_text = __('Add to Cart', 'event_espresso');
        $btmlhtml = EEH_HTML::div(            '',
            'ticket-selector-submit-' . $event_id . '-btn-wrap',
            'ticket-selector-submit-btn-wrap'
        );
        $btmlhtml .= '<input id="ticket-selector-submit-' . $event_id . '-btn"';
        $btmlhtml .= ' class="ticket-selector-submit-btn ticket-selector-submit-ajax"';
        $btmlhtml .= ' type="submit" value="' . $btn_text . '" />';
        $btmlhtml .= EEH_HTML::divx() . '<!-- .ticket-selector-submit-btn-wrap -->';
        echo $btmlhtml;
        ?>	
	</div>
</form>