<?php
/**
 * Single Product Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/*$getproduct = get_field('product_course',get_the_ID());
$product = wc_get_product($getproduct->ID);
if ( $product && is_object( $product ) && $product instanceof WC_Product ) {
	$prCampaign_id = $product->get_id();
}

$single_data = WCCT_Core()->public->get_single_campaign_pro_data( $prCampaign_id );

?>
<!--p class="price"><?php echo $product->get_sale_price(); ?></p-->
<?php 
//$regular_price = $product->regular_price;
//$sale_price = $product->sale_price;
//$price_amt = $product->price;
echo my_commonPriceHtmlVariable($product,$single_data);*/
?>
<!--div class="price custom_price_data_legal local_price_table">
	<div class="expire_data_top">
		<ins><?php //echo wc_price('00.00'); ?></ins>
	</div>
</div-->
<?php 
$event_id = $event;
$t = 0;
$tickets = EEH_Event_View::event_tickets_available( $event_id );
//$get_price_custom = get_post_meta($event_id,'event_custom_price',true);
global $firstprice;
if ( is_array( $tickets ) && ! empty( $tickets )) {
	foreach ( $tickets as $ticket ) {
		if ( $ticket instanceof EE_Ticket ) {
			if($ticket->ticket_status() == 'TKO'):
			if($t == 0){ $unformatted_price = $ticket->get_ticket_total_with_taxes(); $firstprice = $ticket->get_ticket_total_with_taxes(); $ticketname = $ticket->name();  ?>				
				<div class="price custom_price_data_legal local_price_table">
				<div class="expire_data_top">
					<ins><?php echo wc_price($unformatted_price); ?></ins>
				</div>
				</div>
		<?php }
		$t++;
		endif;
		}		
	}
}


