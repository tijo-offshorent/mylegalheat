<?php
/**
 * Single Product Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$getproduct = get_field('product_course',get_the_ID());
$product = wc_get_product($getproduct->ID);
if ( $product && is_object( $product ) && $product instanceof WC_Product ) {
	$prCampaign_id = $product->get_id();
}

$single_data = WCCT_Core()->public->get_single_campaign_pro_data( $prCampaign_id );

//print_r($single_data);

?>
<!--p class="price"><?php echo $product->get_sale_price(); ?></p-->
<?php 
$regular_price = $product->regular_price;
$sale_price = $product->sale_price;
$price_amt = $product->price;
echo my_commonPriceHtml($price_amt, $regular_price, $sale_price,$single_data);
?>
