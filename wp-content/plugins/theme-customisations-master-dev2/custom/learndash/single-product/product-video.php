<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.3.2
 */

defined( 'ABSPATH' ) || exit;

$productid = get_the_ID();
$video = get_post_meta($productid,'wpcf-online-course-video-url',true); 
if($video):
?>
<figure class="woocommerce-product-gallery__wrapper">
<?php echo do_shortcode('[video_lightbox_youtube video_id="'.$video.'" width="800" height="480" auto_thumb="1"]'); ?>
</figure>
<?php endif; ?>
