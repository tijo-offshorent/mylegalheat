<?php 
add_action('admin_menu', 'legal_optin_form_report');
function legal_optin_form_report(){
	add_submenu_page('edit.php?post_type=optin-form', __('Reports','mylegal'), __('Reports','mylegal'), 'manage_options', 'optin-report', 'mylegal_option_reportCallback');
}
function mylegal_option_reportCallback() {
	global $wpdb;
	$table_name = $wpdb->prefix . 'optinform';
	//$optim_datas = $wpdb->get_results('SELECT * FROM '.$table_name,ARRAY_A);
	$pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;
	$limit = 10; // number of rows in page
	$offset = ( $pagenum - 1 ) * $limit;
	$total = $wpdb->get_var("SELECT COUNT(`id`) FROM {$wpdb->prefix}optinform" );
	$num_of_pages = ceil( $total / $limit );
	$optim_datas = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}optinform LIMIT $offset, $limit",ARRAY_A );
	//print_r($offset);
	?>
<div class="wrap">
		<h2>Optin Reports</h2>
		<table class="widefat fixed" cellspacing="0">
	    <thead>
		    <tr>
		    	<th>ID</th>
		    	<th>State</th>
		    	<th>Email</th>
		    	<th>PDF</th>
		    	<th>Client IP</th>
		    	<th>Browser</th>
		    	<th>Date</th>
		    </tr>
	    </thead>

	    <tfoot>
		    <tr>
				<th>ID</th>
				<th>State</th>
				<th>Email</th>
				<th>PDF</th>
				<th>Client IP</th>
				<th>Browser</th>
				<th>Date</th>
		    </tr>
	    </tfoot>

	    <tbody>
	        <?php $k = 1; if($optim_datas): foreach($optim_datas as $optim_data):
	        	$pdf_url = do_shortcode('[media-downloader media_id="'.$optim_data['pdf'].'" texts="Download" class="btn btn-danger"]');
	        	$browser = explode(':',$optim_data['browser']);
	        	$id = $offset+$k;
	         ?>
	        	<tr>
	        		<td><?php echo $id; ?></td>
	        		<td><?php echo $optim_data['state']; ?></td>
	        		<td><?php echo $optim_data['email']; ?></td>
	        		<td><?php echo $pdf_url; ?></td>
	        		<td><?php echo $optim_data['client_ip']; ?></td>
	        		<td><?php echo $browser[1]; ?></td>
	        		<td><?php echo date('d M Y h:i A',strtotime($optim_data['creation_time'])); ?></td>
	        	</tr>
	        <?php $k++; endforeach; else: ?>
	        	<tr><td>Sorry No Data Available</td></tr>
	        <?php endif; ?>     
	    </tbody>
	</table>

	<div class="optin_pagination">
		<?php 
		$page_links = paginate_links( array(
		    'base' => add_query_arg( 'pagenum', '%#%' ),
		    'format' => '',
		    'prev_text' => __( '&laquo;', 'text-domain' ),
		    'next_text' => __( '&raquo;', 'text-domain' ),
		    'total' => $num_of_pages,
		    'current' => $pagenum
		) );

		if ( $page_links ) {
		    echo '<div class="tablenav"><div class="tablenav-pages" style="margin: 1em 0">' . $page_links . '</div></div>';
		}
		?>
	</div>
</div>
	<?php   
}
?>