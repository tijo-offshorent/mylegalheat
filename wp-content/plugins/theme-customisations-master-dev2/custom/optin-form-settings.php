<?php 
add_action('admin_menu', 'legal_optin_form_settings');
function legal_optin_form_settings(){
  add_submenu_page('edit.php?post_type=optin-form', __('Settings','mylegal'), __('Settings','mylegal'), 'manage_options', 'optin-settings', 'mylegal_option_settingsCallback');
}
function mylegal_option_settingsCallback() {
	$dataoptin = get_option('optin_settings_data');
?>
<style type="text/css">
	.widefat.fixed.table-bordered{border-collapse: collapse;}
	.widefat.fixed.table-bordered tr td,.widefat.fixed.table-bordered tr th{border:1px solid #CCC;}
	}
</style>
	<div class="wrap">
		<h2>Optin Settings</h2>
		<form name="optin_form_data_save" id="optin_form_data_save" action="">
		<table class="widefat fixed table-bordered" cellspacing="0">
			<tr>
				<th>Show the Form Startup</th>
				<td><input <?php echo (isset($dataoptin['form_frontend_show'])) ? 'checked': ''; ?> type="checkbox" id="show_form_status" name="show_form_status">
					<i><small>Check to stop the form showing on page load</small></i></td>
			</tr>
			<tr>
				<th>Show Final Message</th>
				<td><textarea name="optin_form_final_message" id="optin_form_final_message" class="widefat"><?php echo base64_decode($dataoptin['final_message']); ?></textarea></td>
			</tr>
			<tr>
				<td colspan="2" align="right">
					<button type="submit" class="save_option_settings button button-primary" id="save_option_settings">Save</button>
				</td>
			</tr>
		</table>
		</form>
	</div>
<script type="text/javascript">
	jQuery(document).ready(function($){
		$('#optin_form_data_save').submit(function(){
			var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
			var datapass = $('#optin_form_data_save').serialize()+'&action=option_save_form_data';
			$.ajax({
				type:'POST',
				url:ajaxurl,
				data:datapass,
				dataType:'html',
				success:function(res){
					//console.log(res);
					location.reload();
				}
			});
			return false;
		});
	});
</script>	
<?php
}
?>