<?php 
add_action('init','create_optin_table');
function create_optin_table(){
  global $wpdb;
  $charset_collate = $wpdb->get_charset_collate();
  $table_name = $wpdb->prefix . 'optinform';
  $sql = "CREATE TABLE $table_name (
    id mediumint(9) NOT NULL AUTO_INCREMENT,  
    state varchar(100) NOT NULL,
    email varchar(100) NOT NULL,
    post_id int(100) NOT NULL,
    pdf int(100) NOT NULL,
    client_ip text NOT NULL,
    browser text NOT NULL,
    creation_time DATETIME DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY  (id)
  )$charset_collate;";
  require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
  dbDelta( $sql );
}

add_action('wp_footer','optin_form_step1');
function optin_form_step1(){
	$html = '';
	global $woocommerce;
	$html .='<div class="modal fade" id="optin_form_step_1_section">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        </div>
        <div class="modal-body">
          <div class="optin_1_logo"><img alt="Mylegalheat" src="'.LEGAL_URL2.'optin-form-logo.png" /></div>
          <div class="optin_1_subtext">CONFIRM YOUR STATE AND EMAIL TO RECEIVE YOUR</div>
          <h2 class="optin_1_heading">Firearm Laws Guide</h2>
          <div class="optin_1_form">
          <form name="optin_step_1_form" id="optin_step_1_form">
          	<div class="form-group">
          		<span class="country_flag" style="background-image:url('.LEGAL_URL2.'us.png)"></span>
          		<select name="legal_optin_state" id="legal_optin_state" class="form-control">
          			<option value="">Choose a State</option>';
          			/*if($default_county_states): foreach($default_county_states as $key => $val):
          				$html .= '<option value="'.$key.'">'.$val.'</option>';
          			endforeach; endif;  */
          			$the_query = new WP_Query(array('post_type' => 'optin-form','posts_per_page' => -1));
					if ( $the_query->have_posts() ) { while ( $the_query->have_posts() ) { $the_query->the_post();
							$html .= '<option value="'.get_the_ID().'">'.get_the_title().'</option>';
					}wp_reset_postdata();}         			
          	$html .= '</select>
          	</div>
			<div class="input-group form-group">
				<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
				<input id="legal_optin_email" type="email" class="form-control" name="legal_optin_email" placeholder="Email Address">
			</div>
			<div class="form-group no-padding">
          		<button class="btn btn-danger disabled" id="legal_optin_btn" name="step_2">SEND THE FIREARM LAWS GUIDE</button>
          	</div>
          </form>
          </div>
        </div>
        <div class="modal-footer">
          <!--button class="btn btn-default nothanks" data-dismiss="modal" aria-hidden="true">Dont Show Me This Again</button>
          <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Subscribe</button-->
          <button class="btn btn-default continue_browsing" data-dismiss="modal" aria-hidden="true">Continue Browsing</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->';
  echo $html;
}

add_action('wp_footer','optin_form_step2');
function optin_form_step2(){
  $dataoptin = get_option('optin_settings_data');
  $html = '';
  $html .='<div class="modal fade" id="optin_form_step_2_section">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        </div>
        <div class="modal-body">
          <div class="optin_2_logo"><img alt="Mylegalheat" src="'.LEGAL_URL2.'optin-form-logo.png" /></div>
          <h2 class="optin_2_heading">'.base64_decode($dataoptin['final_message']).'</h2>
          <h2 class="optin_2_downloadbtn"></h2>
        </div>
        <div class="modal-footer">
          <button class="btn btn-default continue_browsing" data-dismiss="modal" aria-hidden="true">Continue Browsing</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->';
  echo $html;
}
?>