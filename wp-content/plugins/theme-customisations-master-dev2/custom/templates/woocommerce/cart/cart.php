<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

wc_print_notices();
global $wpdb;
do_action( 'woocommerce_before_cart' ); ?>

<form class="woocommerce-cart-form mylegal_checkout" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
	<?php do_action( 'woocommerce_before_cart_table' ); ?>
<div class="row woocommerce_legal_cart">
	<div class="col-md-9">
		<ul class="woocommerce_legal_cart_list">
			<?php do_action( 'woocommerce_before_cart_contents' ); ?>
				<?php foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) { 
					$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
					$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
				$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );	
				?>
				<li>
					<?php
						// @codingStandardsIgnoreLine
						echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
							'<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
							esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
							__( 'Remove this item', 'woocommerce' ),
							esc_attr( $product_id ),
							esc_attr( $_product->get_sku() )
						), $cart_item_key );
					?>	
					<div class="cart_title">
						<?php
						if ( ! $product_permalink ) {
							echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' );
						} else {
							echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '%s',$_product->get_name()), $cart_item, $cart_item_key ) );
							if($cart_item['ticketname']):
								echo '<br/><span class="red_color">Ticket: '.$cart_item['ticketname'].'</span>';
							endif;
						}
						?>
					</div>
					<div class="cart_price">
						<?php
							echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
						?>
					</div>
					<div class="cart_qty">					
						<b>Qty : <?php echo $cart_item['quantity']; ?></b>
					</div>	
					<?php $metadata = get_post_meta($product_id,'wpcf-class-type',true);
					if($metadata == 'local-class'): ?>
					<div class="cart_student_details">
						<h2>Student Information:</h2>
						<?php 	
						//print_r($cart_item['regid']);	
						$regid = implode(',',$cart_item['regid']);				
						$getdetails = $wpdb->get_results('SELECT espatnd.* FROM '.$wpdb->prefix.'esp_attendee_meta AS espatnd INNER JOIN '.$wpdb->prefix.'esp_registration AS espreg ON espreg.ATT_ID = espatnd.ATT_ID WHERE espreg.REG_ID IN ('.$regid.')',ARRAY_A);
						?>
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Name</th>
									<th>Email</th>
								</tr>
							</thead>
							<tbody>
								<?php if($getdetails): foreach($getdetails as $getdetail): ?>
									<tr>
										<td><?php echo $getdetail['ATT_fname']; ?></td>
										<td><?php echo $getdetail['ATT_email']; ?></td>
									</tr>
								<?php endforeach; endif; ?>
							</tbody>
						</table>
					</div>
					<?php endif; ?>
				</li>
				<?php } ?>
				<?php } ?>
			<?php do_action( 'woocommerce_after_cart_contents' ); ?>
		</ul>
		<div class="coupon_and_total">
			<div class="coupon_and_total_left">
				<div class="ajax-coupon-redeem coupon">
					<input type="text" name="coupon" class="coupon"  value="" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>"/>
					<button type="submit" class="button" name="redeem-coupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>"><?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?></button>
						<?php do_action( 'woocommerce_cart_coupon' ); ?>
					<img class="ajaxloader" alt="" src="<?php echo LEGAL_URL2; ?>/loader.gif">	
				</div>
			</div>	
			<div class="coupon_and_total_right">				
				<b>Subtotal (<?php echo WC()->cart->get_cart_contents_count(); ?> items): <span class="red_color_cart"><?php wc_cart_totals_subtotal_html(); ?></span></b>
			</div>
		</div>
	</div>	
	<div class="col-md-3">
		<div class="cart-collaterals">
		<?php
			do_action( 'woocommerce_cart_collaterals' );
		?>

		<?php do_action( 'woocommerce_cart_actions' ); ?>

		<?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?>

		<div class="left_side_coupon">
		<?php if ( wc_coupons_enabled() ) { ?>
			<div class="coupon">
				<!--label for="coupon_code"><?php esc_html_e( 'Coupon:', 'woocommerce' ); ?></label--> <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" /> <button type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>"><?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?></button>
				<?php do_action( 'woocommerce_cart_coupon' ); ?>
			</div>
		<?php } ?>
		</div>
		</div>
	</div>	
</div>
<?php do_action( 'woocommerce_after_cart_table' ); ?>
</form>
<?php do_action( 'woocommerce_after_cart' ); ?>