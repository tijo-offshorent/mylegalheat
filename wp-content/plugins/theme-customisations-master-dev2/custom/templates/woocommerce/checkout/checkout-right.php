<div class="user_info">
	<h4>Billing Address</h4>
	<ul class="billing_info">
		<li><span class="name"></span></li>
		<li><span class="company"></span></li>
		<li><span class="address1"></span></li>
		<li><span class="address2"></span></li>
		<li><span class="location"></span></li>
		<li><span class="country"></span></li>
		<li><span class="phone"></span></li>
	</ul>
</div>

<?php if(count($online_local_class) == 0): ?>
<div class="user_info">
	<h4>Shipping Address</h4>
	<ul class="shipping_info">
		<li><span class="name"></span></li>
		<li><span class="company"></span></li>
		<li><span class="address1"></span></li>
		<li><span class="address2"></span></li>
		<li><span class="location"></span></li>
		<li><span class="country"></span></li>
		<li><span class="phone"></span></li>
	</ul>
</div>

<div class="user_info">
	<h4>Shipping Method</h4>
	<span class="shipping_method_right"></span>
</div>

<?php endif; ?>


<?php if(is_user_logged_in()): ?>
<div class="legal_order_button">
<?php echo apply_filters( 'woocommerce_order_button_html', '<button type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="' . esc_attr( 'Place order' ) . '" data-value="' . esc_attr( 'Place order' ) . '">' . esc_html( 'Place order' ) . '</button>' ); // @codingStandardsIgnoreLine ?>
</div>
<?php else: ?>
<div class="legal_order_button guest_checkout">
<?php echo apply_filters( 'woocommerce_order_button_html', '<button type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="' . esc_attr( 'Place order' ) . '" data-value="' . esc_attr( 'Place order' ) . '">' . esc_html( 'Place order' ) . '</button>' ); // @codingStandardsIgnoreLine ?>
</div>	
<?php endif; ?>
