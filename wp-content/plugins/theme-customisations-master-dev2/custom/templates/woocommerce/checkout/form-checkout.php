<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();

do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout
/*if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
	return;
}*/

$online_local_class = array();
foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) { 
	$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
	$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
	$classtype = get_post_meta($product_id,'wpcf-class-type',true);
	if($classtype):
		$online_local_class[] = get_post_meta($product_id,'wpcf-class-type',true);
	endif;
}
?>
<div class="mylegal_checkout_page">
<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">	
<div class="row">
<div class="col-md-9">
	<?php if ( $checkout->get_checkout_fields() ) : ?>

		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

		<div class="col2-set" id="customer_details">
			<div class="my_legal_gust_and_login pull-left">
				<?php include(LEGAL_DIR2.'templates/woocommerce/checkout/login-register.php'); ?>
			</div>

			<?php if(is_user_logged_in()): ?>

			<div class="my_legal_billing pull-left">
				<?php do_action( 'woocommerce_checkout_billing',$online_local_class ); ?>
			</div>

			<?php //print_r($online_local_class); ?>

			<?php if(count($online_local_class) == 0): ?>

			<div class="my_legal_shipping pull-left">
				<?php do_action( 'woocommerce_checkout_shipping' ); ?>
			</div>

			<div class="my_legal_shipping_method pull-left">
				<?php include(LEGAL_DIR2.'templates/woocommerce/checkout/shipping-method.php'); ?>
			</div>

			<?php endif; ?>

			<div class="my_legal_payment pull-left">
				<?php include(LEGAL_DIR2.'templates/woocommerce/checkout/payment-method-custom.php'); ?>
			</div>

			<div class="my_legal_order_review pull-left">
				<?php include(LEGAL_DIR2.'templates/woocommerce/checkout/order-review-mylegal.php'); ?>
			</div>			

			<div class="my_legal_place_order pull-left">
				<div class="form-row place-order">
				<noscript>
				<?php esc_html_e( 'Since your browser does not support JavaScript, or it is disabled, please ensure you click the <em>Update Totals</em> button before placing your order. You may be charged more than the amount stated above if you fail to do so.', 'woocommerce' ); ?>
				<br/><button type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="<?php esc_attr_e( 'Update totals', 'woocommerce' ); ?>"><?php esc_html_e( 'Update totals', 'woocommerce' ); ?></button>
				</noscript>

				<?php wc_get_template( 'checkout/terms.php' ); ?>

				<?php do_action( 'woocommerce_review_order_before_submit' ); ?>
				
				<?php do_action( 'woocommerce_review_order_after_submit' ); ?>

				<?php wp_nonce_field( 'woocommerce-process_checkout', 'woocommerce-process-checkout-nonce' ); ?>
				</div>
			</div>
			<div class="checkout_button_down">
				<?php echo apply_filters( 'woocommerce_order_button_html', '<button type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="' . esc_attr( 'Place order' ) . '" data-value="' . esc_attr( 'Place order' ) . '">' . esc_html( 'Place order' ) . '</button>' ); // @codingStandardsIgnoreLine ?>
			</div>
			<?php else: ?>
			<div class="my_legal_billing pull-left guest_checkout">
				<?php do_action( 'woocommerce_checkout_billing' ); ?>
			</div>

			<?php if(count($online_local_class) == 0): ?>

			<div class="my_legal_shipping pull-left guest_checkout">
				<?php do_action( 'woocommerce_checkout_shipping' ); ?>
			</div>


			<div class="my_legal_shipping_method pull-left guest_checkout">
				<?php include(LEGAL_DIR2.'templates/woocommerce/checkout/shipping-method.php'); ?>
			</div>

			<?php endif; ?>

			<div class="my_legal_payment pull-left guest_checkout">
				<?php include(LEGAL_DIR2.'templates/woocommerce/checkout/payment-method-custom.php'); ?>
			</div>

			<div class="my_legal_order_review pull-left guest_checkout">
				<?php include(LEGAL_DIR2.'templates/woocommerce/checkout/order-review-mylegal.php'); ?>
			</div>			

			<div class="my_legal_place_order pull-left guest_checkout">
				<div class="form-row place-order">
				<noscript>
				<?php esc_html_e( 'Since your browser does not support JavaScript, or it is disabled, please ensure you click the <em>Update Totals</em> button before placing your order. You may be charged more than the amount stated above if you fail to do so.', 'woocommerce' ); ?>
				<br/><button type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="<?php esc_attr_e( 'Update totals', 'woocommerce' ); ?>"><?php esc_html_e( 'Update totals', 'woocommerce' ); ?></button>
				</noscript>

				<?php wc_get_template( 'checkout/terms.php' ); ?>

				<?php do_action( 'woocommerce_review_order_before_submit' ); ?>
				
				<?php do_action( 'woocommerce_review_order_after_submit' ); ?>

				<?php wp_nonce_field( 'woocommerce-process_checkout', 'woocommerce-process-checkout-nonce' ); ?>
				</div>
			</div>

			<div class="checkout_button_down guest_checkout">
				<?php echo apply_filters( 'woocommerce_order_button_html', '<button type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="' . esc_attr( 'Place order' ) . '" data-value="' . esc_attr( 'Place order' ) . '">' . esc_html( 'Place order' ) . '</button>' ); // @codingStandardsIgnoreLine ?>
			</div>
			<?php endif;?>			
		</div>

		<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

	<?php endif; ?>

	<?php /*
	<h3 id="order_review_heading"><?php _e( 'Your order', 'woocommerce' ); ?></h3>

	<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

	<div id="order_review" class="woocommerce-checkout-review-order">
		<?php do_action( 'woocommerce_checkout_order_review' ); ?>
	</div>

	<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
	*/
	?>


</div>

		<div class="col-md-3">
			<div class="checkout_user_data_right">
				<?php include(LEGAL_DIR2.'templates/woocommerce/checkout/checkout-right.php'); ?>
			</div>
		</div>
	</div>
</form>	
</div>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
