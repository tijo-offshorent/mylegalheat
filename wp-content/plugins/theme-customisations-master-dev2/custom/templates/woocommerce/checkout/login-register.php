<?php 
/**
 * Checkout Order Review
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/order-review-mylegal.php.
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<h3><?php _e( '1 Checkout Method', 'woocommerce' ); ?></h3>
<?php if(! is_user_logged_in()): ?>
<div class="checkoutoginRegisterPage">
	<div class="row">
		<div class="col-md-6">
			<div class="checkoutoginRegisterPage-left">
				<h2>New Customers</h2>
				<p>You’ll have an opportunity to create an account later.</p>
				<a href="#" id="checkout_as_guest">CONTINUE AS GUEST</a>
			</div>	
		</div>
		<div class="col-md-6">
			<div class="checkoutoginRegisterPage-right">				
				<h2>Returning Customers</h2>
				<input type="text" name="checkout_user_email" id="checkout_user_email" placeholder="Email Address">
				<input type="password" name="checkout_user_password" id="checkout_user_password" placeholder="Password">
				<?php wp_nonce_field( 'checkout-user-nonce', 'security' ); ?>
				<a href="" id="checkout_login_btn">LOGIN</a>
				<a href="" id="checkout_forgot_password">Forgot your password?</a>
				<div class="show_loader"><img src="<?php echo LEGAL_URL2; ?>login_loader.gif" alt="Login Loader"></div>
				<p class="status"></p>
			</div>
		</div>
	</div>
</div>
<?php else: ?>
	<div class="checkoutoginRegisterPageNormal">
		<h2 class="myaccount_heading"><a target="_blank" href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('My Account','woothemes'); ?>"><?php _e('My Account','woothemes'); ?></a></h2>
		<h2 class="myaccount_heading"><a href="<?php echo wp_logout_url( get_permalink() ); ?>" title="<?php _e('Logout','woothemes'); ?>"><?php _e('Log Out','woothemes'); ?></a></h2>
	</div>
<?php endif; ?>
