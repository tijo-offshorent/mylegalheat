<?php 
/**
 * Checkout Order Review
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/order-review-mylegal.php.
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $wpdb;
?>
<?php if(count($online_local_class) == 0): ?>
	<h3><?php _e( '6 Order Review', 'woocommerce' ); ?></h3>
<?php else: ?>
	<h3><?php _e( '4 Order Review', 'woocommerce' ); ?></h3>
<?php endif; ?>
<ul class="woocommerce_legal_cart_list">
	<?php do_action( 'woocommerce_before_cart_contents' ); ?>
		<?php foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) { 
			$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
			$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

		if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
		$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );	
		?>
		<li>
			<div class="cart_title">
				<?php
				if ( ! $product_permalink ) {
					echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' );
				} else {
					echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '%s',$_product->get_name()), $cart_item, $cart_item_key ) );
					if($cart_item['ticketname']):
						echo '<br/><span class="red_color">Ticket: '.$cart_item['ticketname'].'</span>';
					endif;
				}				
				?>
	</div>
			<div class="cart_price">
				<?php
					echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
					echo '&nbsp;&nbsp; Qty:'.$cart_item['quantity'];
				?>									
			</div>
			<?php $metadata = get_post_meta($product_id,'wpcf-class-type',true);
					if($metadata == 'local-class'): ?>
					<div class="cart_student_details">
						<h2>Student Information:</h2>
						<?php 	
						//print_r($cart_item['regid']);	
						$regid = implode(',',$cart_item['regid']);				
						$getdetails = $wpdb->get_results('SELECT espatnd.* FROM '.$wpdb->prefix.'esp_attendee_meta AS espatnd INNER JOIN '.$wpdb->prefix.'esp_registration AS espreg ON espreg.ATT_ID = espatnd.ATT_ID WHERE espreg.REG_ID IN ('.$regid.')',ARRAY_A);
						?>
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Name</th>
									<th>Email</th>
								</tr>
							</thead>
							<tbody>
								<?php if($getdetails): foreach($getdetails as $getdetail): ?>
									<tr>
										<td><?php echo $getdetail['ATT_fname']; ?></td>
										<td><?php echo $getdetail['ATT_email']; ?></td>
									</tr>
								<?php endforeach; endif; ?>
							</tbody>
						</table>
					</div>
					<?php endif; ?>											
		</li>
		<?php } ?>
		<?php } ?>
	<?php do_action( 'woocommerce_after_cart_contents' ); ?>
</ul>

<div class="mylegal_checkout_review_price">
	<div class="row">
		<div class="col-md-6"></div>
		<div class="col-md-6 mylegal_checkout_total">
			<table class="">

			<tfoot>

				<tr class="cart-subtotal">
					<th><?php _e( 'Subtotal', 'woocommerce' ); ?> (<?php echo WC()->cart->get_cart_contents_count(); ?> items)</th>
					<td><?php wc_cart_totals_subtotal_html(); ?></td>
				</tr>

				<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
					<tr class="cart-discount coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
						<th><?php wc_cart_totals_coupon_label( $coupon ); ?></th>
						<td><?php wc_cart_totals_coupon_html( $coupon ); ?></td>
					</tr>
				<?php endforeach; ?>


				<?php if(count($online_local_class) == 0): ?>
				<?php 
				foreach( WC()->session->get('shipping_for_package_0')['rates'] as $method_id => $rate ){
				    if( WC()->session->get('chosen_shipping_methods')[0] == $method_id ){
				        $rate_label = $rate->label; // The shipping method label name
				        $rate_cost_excl_tax = floatval($rate->cost); // The cost excluding tax
				        // The taxes cost
				        $rate_taxes = 0;
				        foreach ($rate->taxes as $rate_tax)
				            $rate_taxes += floatval($rate_tax);
				        // The cost including tax
				        $rate_cost_incl_tax = $rate_cost_excl_tax + $rate_taxes;
				        ?>
				        <tr class="shipping-total">
				            <th class="label">Shipping &amp; Handling (<?php echo $rate_label; ?>):</th>
				            <td class="totals"><?php echo WC()->cart->get_cart_shipping_total(); ?></td>
				        </tr>
				        <?php
				        break;
				    }
				}
				?>
				<?php endif; ?>
				

				<?php foreach ( WC()->cart->get_fees() as $fee ) : ?>
					<tr class="fee">
						<th><?php echo esc_html( $fee->name ); ?></th>
						<td><?php wc_cart_totals_fee_html( $fee ); ?></td>
					</tr>
				<?php endforeach; ?>

				<?php if ( wc_tax_enabled() && ! WC()->cart->display_prices_including_tax() ) : ?>
					<?php if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) : ?>
						<?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : ?>
							<tr class="tax-rate tax-rate-<?php echo sanitize_title( $code ); ?>">
								<th><?php echo esc_html( $tax->label ); ?></th>
								<td><?php echo wp_kses_post( $tax->formatted_amount ); ?></td>
							</tr>
						<?php endforeach; ?>
					<?php else : ?>
						<tr class="tax-total">
							<th><?php echo esc_html( WC()->countries->tax_or_vat() ); ?></th>
							<td><?php wc_cart_totals_taxes_total_html(); ?></td>
						</tr>
					<?php endif; ?>
				<?php endif; ?>

				<?php do_action( 'woocommerce_review_order_before_order_total' ); ?>

				<?php do_action( 'woocommerce_review_order_new_order_total' ); ?>

				<tr class="order-total">
					<th><?php _e( 'Grand Total:', 'woocommerce' ); ?></th>
					<td><?php wc_cart_totals_order_total_html(); ?></td>
				</tr>

				<?php do_action( 'woocommerce_review_order_after_order_total' ); ?>

			</tfoot>
		</table>
		</div>
	</div>
</div>
