<?php 
/**
 * Checkout Payment method
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/payment-method-custom.php.
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<?php if(count($online_local_class) == 0): ?>
	<h3><?php _e( '5 Payment Information', 'woocommerce' ); ?></h3>
<?php else: ?>
	<h3><?php _e( '3 Payment Information', 'woocommerce' ); ?></h3>
<?php endif; ?>


<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

<div id="order_review" class="woocommerce-checkout-review-order">
<?php do_action( 'woocommerce_checkout_order_review' ); ?>
</div>

<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
