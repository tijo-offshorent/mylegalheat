<?php 
/**
 * Checkout Shipping method
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/shipping-method.php.
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<h3><?php _e( '4 Shipping Method', 'woocommerce' ); ?></h3>

<?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>

	<?php do_action( 'woocommerce_review_order_before_shipping' ); ?>

	<?php wc_cart_totals_shipping_html(); ?>

	<?php do_action( 'woocommerce_review_order_after_shipping' ); ?>

<?php endif; ?>