<?php
/**
 * Plugin Name:       Theme Customisations 2
 * Description:       A handy little plugin to contain your theme customisation snippets.
 * Plugin URI:        https://virtina.com
 * Version:           1.0.0
 * Author:            Virtina
 * Author URI:        https://virtina.com/
 * Requires at least: 3.0.0
 * Tested up to:      4.4.2
 *
 * @package Theme_Customisations
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

define('LEGAL_URL2',plugins_url( '/custom/', __FILE__ ));
define('LEGAL_DIR2',plugin_dir_path( __FILE__ ). 'custom/');


/**
 * Main Theme_Customisations Class
 *
 * @class Theme_Customisations
 * @version	1.0.0
 * @since 1.0.0
 * @package	Theme_Customisations
 */
final class Theme_Customisations2 {

	/**
	 * Set up the plugin
	 */
	public function __construct() {
		add_action( 'init', array( $this, 'theme_customisations_setup' ), -1 );
		require_once( 'custom/functions.php' );
		require_once( 'custom/optin-form.php' );
		require_once( 'custom/optin-form-report.php' );
		require_once( 'custom/optin-form-settings.php' );
		require_once( 'custom/eventexpresso/registration_details.php' );
	}

	/**
	 * Setup all the things
	 */
	public function theme_customisations_setup() {
		add_action( 'wp_enqueue_scripts', array( $this, 'theme_customisations_css2' ), 999 );
		add_action( 'wp_enqueue_scripts', array( $this, 'theme_customisations_js2' ) );
		add_filter( 'template_include',   array( $this, 'theme_customisations_template' ), 99 );
		add_filter( 'wc_get_template',    array( $this, 'theme_customisations_wc_get_template2' ), 11, 5 );
	}

	/**
	 * Enqueue the CSS
	 *
	 * @return void
	 */
	public function theme_customisations_css2() {
		wp_enqueue_style( 'custom2-css', plugins_url( '/custom/style.css', __FILE__ ), '', filemtime(untrailingslashit( plugin_dir_path( __FILE__ ) ) . '/custom/style.css') );
	}

	/**
	 * Enqueue the Javascript
	 *
	 * @return void
	 */
	public function theme_customisations_js2() {
		$dataoptin = get_option('optin_settings_data');
		wp_enqueue_script( 'cookie-js', plugins_url( '/custom/jquery.cookie.min.js', __FILE__ ), array( 'jquery' ), filemtime(untrailingslashit( plugin_dir_path( __FILE__ ) ) . '/custom/jquery.cookie.min.js') );
		wp_enqueue_script( 'custom2-js', plugins_url( '/custom/custom.js', __FILE__ ), array( 'jquery' ), filemtime(untrailingslashit( plugin_dir_path( __FILE__ ) ) . '/custom/custom.js') );
		wp_localize_script('custom2-js','optin_data',$dataoptin);
	}
	
	/**
	 * Look in this plugin for template files first.
	 * This works for the top level templates (IE single.php, page.php etc). However, it doesn't work for
	 * template parts yet (content.php, header.php etc).
	 *
	 * Relevant trac ticket; https://core.trac.wordpress.org/ticket/13239
	 *
	 * @param  string $template template string.
	 * @return string $template new template string.
	 */
	public function theme_customisations_template( $template ) {
		if(is_singular('sfwd-courses')){
			$template = untrailingslashit( plugin_dir_path( __FILE__ ) ) . '/custom/learndash/single-course.php';
		}
		if ( file_exists( untrailingslashit( plugin_dir_path( __FILE__ ) ) . '/custom/templates/woocommerce/' . basename( $template ) ) ) {
			$template = untrailingslashit( plugin_dir_path( __FILE__ ) ) . '/custom/templates/woocommerce/' . basename( $template );			
		}
		return $template;
	}

	/**
	 * Look in this plugin for WooCommerce template overrides.
	 *
	 * For example, if you want to override woocommerce/templates/cart/cart.php, you
	 * can place the modified template in <plugindir>/custom/templates/woocommerce/cart/cart.php
	 *
	 * @param string $located is the currently located template, if any was found so far.
	 * @param string $template_name is the name of the template (ex: cart/cart.php).
	 * @return string $located is the newly located template if one was found, otherwise
	 *                         it is the previously found template.
	 */
	public function theme_customisations_wc_get_template2( $located, $template_name, $args, $template_path, $default_path ) {
		$plugin_template_path = untrailingslashit( plugin_dir_path( __FILE__ ) ) . '/custom/templates/woocommerce/' . $template_name;

		if ( file_exists( $plugin_template_path ) ) {
			$located = $plugin_template_path;
		}		

		return $located;
	}
} // End Class

/**
 * The 'main' function
 *
 * @return void
 */
function theme_customisations_main2() {
	new Theme_Customisations2();
}

/**
 * Initialise the plugin
 */
add_action( 'plugins_loaded', 'theme_customisations_main2' );
