<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package LegalHeat
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'legal-heat' ); ?></a>
	<div class="menu-overlay"></div> 
	<header id="masthead" class="site-header">
		<div class="container">
			<div class="row">
				<div class="col-3">
					<div class="site-branding">
						<?php
						the_custom_logo();
						if ( is_front_page() && is_home() ) :
							?>
							<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
							<?php
						else :
							?>
							<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
							<?php
						endif;
						$legal_heat_description = get_bloginfo( 'description', 'display' );
						if ( $legal_heat_description || is_customize_preview() ) :
							?>
							<p class="site-description"><?php echo $legal_heat_description; /* WPCS: xss ok. */ ?></p>
						<?php endif; ?>
					</div><!-- .site-branding -->
				</div>
				
				<div class="col-9">
					<div class="accout-header clearfix">
						<div class="account-header-right">
							<div class="logintext-wrap">
								<?php if ( is_user_logged_in() ) { ?>
								 	<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('My Account','legal-heat'); ?>"><?php _e('My Account','legal-heat'); ?></a>
								 <?php } 
								 else { ?>
								 	<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Login / Register','legal-heat'); ?>"><?php _e('Login / Register','legal-heat'); ?></a>
								 <?php } ?>
							</div>
							<div class="minicart-wrap">
								<?php if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
							 
								    $count = WC()->cart->cart_contents_count;
								    ?><a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php 
								    if ( $count > 0 ) {
								        ?>
								        <span class="cart-contents-count"><?php echo esc_html( $count ); ?></span>
								        <?php
								    }
								        ?></a>
								 
								<?php } ?>
							</div>
							<div class="search-wrap">
								<span class="search-toggle"><i class="icon-search"></i></span>
								<div class="search-form">
									<form method="get" class="input-group" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
										<input type="text" class="form-control" name="s" id="s" placeholder="<?php esc_attr_e( 'Search Products', 'legal-heat' ); ?>" />
										<div class="input-group-append">
											<input type="submit" class="btn submit" name="submit" id="searchsubmit" value="<?php esc_attr_e( 'Search', 'legal-heat' ); ?>" />
										</div>
									</form>
								</div>
							</div>
							<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><span></span><span></span><span></span></button>
						</div>
					</div>
					<nav id="site-navigation" class="main-navigation">
						<div class="sidebar-nav-header clearfix">
							<div class="minicart-wrap float-left">
								<?php if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
							 
								    $count = WC()->cart->cart_contents_count;
								    ?><a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php 
								    if ( $count > 0 ) {
								        ?>
								        <span class="cart-contents-count"><?php echo esc_html( $count ); ?></span>
								        <?php
								    }
								        ?></a>
								 
								<?php } ?>
							</div>
							
							<div class="logintext-wrap float-right">
								<?php if ( is_user_logged_in() ) { ?>
								 	<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('My Account','legal-heat'); ?>"><?php _e('My Account','legal-heat'); ?></a>
								 <?php } 
								 else { ?>
								 	<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Login / Register','legal-heat'); ?>"><?php _e('Login / Register','legal-heat'); ?></a>
								 <?php } ?>
							</div>
						</div>

						<?php
							wp_nav_menu( array(
								'theme_location' => 'menu-1',
								'menu_id'        => 'primary-menu',
							) );
						?>

						<div class="sidebar-nav-footer">
							<?php
								wp_nav_menu( array(
									'menu' => 'Footer Menu',
									'menu_class' => 'sidebar-bottom-menu'
								) );
							?>
						</div>
					</nav><!-- #site-navigation -->
				</div>
			</div>
		</div>
	</header><!-- #masthead -->

	<?php 
		if ( is_singular('sfwd-courses') ) {
			$productid = get_the_ID();
			$metadata = get_post_meta($productid,'wpcf-class-type',true);
			if($metadata == 'local-class' || $metadata == 'online-class'):
			$image = '';
			if(types_render_field('background-image')){
				$image = types_render_field('background-image');
			}else{
				$image = wp_get_attachment_url( get_post_thumbnail_id());
			}
			endif;
			?>
			<div class="page-header product_single_header" style="background-image: url(<?php echo $image; ?>);">
				<div class="container">
					<div class="row">
						<div class="col-lg-8 col-md-12 col-sm-12">
							<?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>

							<?php if (types_render_field('header-title')) {?>	
								<h1><?php echo types_render_field("header-title");?></h1>
							<?php }else {
								?>
									<h1><?php the_title();?></h1>
								<?php
							}?>
							<?php if (types_render_field('header-description')) {?>
								<h4><?php echo types_render_field("header-description");?></h4>
							<?php } else {echo '';} ?>
						</div>
					</div>
				</div>
			</div>

		<?php
		}else if ( is_singular('espresso_events') ) {
		$productid = get_the_ID();
		$image = '';
		if(types_render_field('background-image')){
			$image = types_render_field('background-image');
		}else{
			$image = wp_get_attachment_url( get_post_thumbnail_id());
		}
		?>
		<div class="page-header product_single_header" style="background-image: url(<?php echo $image; ?>);">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-md-12 col-sm-12">
						<?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>

						<?php if (types_render_field('header-title')) {?>	
							<h1><?php echo types_render_field("header-title");?></h1>
						<?php }else {
							?>
								<h1><?php the_title();?></h1>
							<?php
						}?>
						<?php if (types_render_field('header-description')) {?>
							<h4><?php echo types_render_field("header-description");?></h4>
						<?php } else {echo '';} ?>
					</div>
				</div>
			</div>
		</div>
		<?php
		} elseif ( is_single() || is_archive() ){

			$page_id = "39";

			if(is_tax('partner')){
			$partner_term=get_queried_object();
		
			$partner_logo_url=get_term_meta($partner_term->term_id,'wpcf-partner-image',true);
			}

			if (has_post_thumbnail($page_id) ):
		    	$image = wp_get_attachment_image_src( get_post_thumbnail_id($page_id), 'single-post-thumbnail' );
		    endif;	
			?>
			<div class="page-header" style="background-image: url(<?php echo $image_URI = $image[0]; ?>)">
					<div class="container">
						<?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>
						
						<img src="<?php echo  $partner_logo_url; ?>"  class="partner-logo"/>

						<?php if (types_render_field('header-title')) {?>	
							<h1><?php echo types_render_field("header-title"); ?></h1>
						<?php }else {
							?>
								<h1><?php the_title();?></h1>
							<?php
						}?>

						<?php if (types_render_field('header-description')) {?>
							<h4><?php echo types_render_field("header-description");?></h4>
						<?php } else {echo '';} ?>

						<?php 
						$post_id = get_the_ID();
						echo $value = get_field( 'breadcrumb_text_', $post_id ); ?>
					</div>
				</div>
			<?php
		} elseif ( is_page() ){
			if (!is_front_page()) {
				?>
				<div class="page-header" style="background-image: url(<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>)">
					<div class="container">
						<?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>
						
						<?php if (types_render_field('header-title')) {?>	
							<h1><?php echo types_render_field("header-title");?></h1>
						<?php }else {
							?>
								<h1><?php the_title();?></h1>
							<?php
						}?>

						<?php if (types_render_field('header-description')) {?>
							<h4><?php echo types_render_field("header-description");?></h4>
						<?php } else {echo '';} ?>

						<?php 
						$post_id = get_the_ID();
						//echo $value = get_field( 'breadcrumb_text_', $post_id ); ?>
					</div>
				</div>
			<?php
			}
		} elseif ( is_home() ){
				?>
				<div class="page-header" style="background-image: url(<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>)">
					<div class="container">
						<?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>
						
						<?php if (types_render_field('header-title')) {?>	
							<h1><?php echo types_render_field("header-title");?></h1>
						<?php }else {
							?>
								<h1><?php the_title();?></h1>
							<?php
						}?>

						<?php if (types_render_field('header-description')) {?>
							<h4><?php echo types_render_field("header-description");?></h4>
						<?php } else {echo '';} ?>

						<?php 
						$post_id = get_the_ID();
						echo $value = get_field( 'breadcrumb_text_', $post_id ); ?>
					</div>
				</div>
			<?php
		}
	?>

	<div id="content" class="site-content">
