<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package LegalHeat
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function legal_heat_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	if ( types_render_field('body-color') ) {
		global $post;	
		$bodycolor = get_post_meta($post->ID,'wpcf-body-color',true);
		$classes[] = $bodycolor;
	}

	return $classes;
}
add_filter( 'body_class', 'legal_heat_body_classes',99,2 );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function legal_heat_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'legal_heat_pingback_header' );
