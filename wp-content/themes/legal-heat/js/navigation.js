/**
 * File navigation.js.
 *
 * Handles toggling the navigation menu for small screens and enables TAB key
 * navigation support for dropdown menus.
 */
( function($) {
	var $toggleButton = $('.menu-toggle'),
    	$menuWrap = $('.main-navigation'),
    	$menuOverlay = $('.menu-overlay');

    	$( "<span class='sidebar-menu-arrow'></span>" ).insertBefore( ".sub-menu" );
    	$( ".sub-menu" ).hide();

	// Hamburger button
	$toggleButton.on('click', function() {
		$(this).toggleClass('button-open');
		$menuWrap.toggleClass('menu-show');
		$menuOverlay.toggleClass('overlay-open');
	});
	$menuOverlay.on('click', function() {
		$(this).removeClass('overlay-open');
		$menuWrap.toggleClass('menu-show');
		$('.sub-menu').hide(300);
	});
	$('.sidebar-menu-arrow').click(function() {
		$(this).next().slideToggle(300);
	});
	// End


	// Search Toggle
	$('.search-toggle').on("click", function (event) {
	    event.stopPropagation();
	    $('.search-form').toggle();
	});
	 $(".search-form").on("click", function (event) {
        event.stopPropagation();
    });

	$(document).on("click", function (event) {
	    $('.search-form').hide();
	});
	// End


})(jQuery);
