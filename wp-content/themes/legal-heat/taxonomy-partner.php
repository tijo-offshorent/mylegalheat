<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package LegalHeat
 */

get_header();
?>
<div class="container">		
	<div class="row">
	<div id="primary" class="col-12 col-sm-8 content-area">
		<main id="main" class="site-main">

		<div class="course_section">	
		<?php
			echo do_shortcode('[course_list_by_partner]'); 
		?>	
		</div>
		</main><!-- #main -->
	</div><!-- #primary -->
	<?php get_sidebar(); ?>
	</div>
</div>
<?php
//get_sidebar();
get_footer();